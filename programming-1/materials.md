# Лекция №16. 20 декабря
## Метапрограммирование
### std::static_assert

* `static_assert` предназначен для compile-time проверок. Аналог обычного assert
* <details>

  <summary>Пример кода</summary>

    ```c++
    #include <string>
    #include <sstream>
    #include <iostream>
    #include <type_traits>

    template <class T>
    void Swap(T& a, T& b) noexcept
    {
        static_assert(std::is_copy_constructible_v<T>,
                    "Swap requires copying");
        static_assert(std::is_nothrow_copy_constructible_v<T> &&
                    std::is_nothrow_copy_assignable_v<T>,
                    "Swap requires nothrow copy/assign");
        auto c = b;
        b = a;
        a = c;
    }

    struct NonCopyable {
        NonCopyable() {};
        NonCopyable(const NonCopyable&) = delete;
    };

    struct ThrowOnCopy {
        ThrowOnCopy() {};
        ThrowOnCopy(const ThrowOnCopy&) {
            throw;
        };
    };

    struct AllOk {
        AllOk() {};
        AllOk(const AllOk&) noexcept {
        };
    };

    int main() {
        //NonCopyable a, b;
        //ThrowOnCopy a, b;
        AllOk a, b;
        Swap(a, b);

        return 0;
    };
    ```
  </details>

### std::enable_if
`enable_if` позволяет проверять условие во время компиляции и возвращает указанный тип, если условие выполняется.
Если условие не выполняется, то подстановка типа проваливается и текущий шаблон не вычисляется (по принципе SFINAE)
* <details>

  <summary>Пример кода</summary>

    ```c++
    #include <string>
    #include <iostream>

    using namespace std;

    // enable_if_t<...> -- это просто `typename enable_if<...>::value`

    template<size_t CoolValue>
    enable_if_t<(CoolValue > 10), string> Foo() {
        return "Cool function for the big value\n";
    }

    template<size_t CoolValue>
    enable_if_t<(CoolValue <= 10), string> Foo() {
        return "Cool function for the small value\n";
    }

    int main() {
        cout << Foo<5>();
        cout << Foo<15>();
        return 0;
    };
    ```
  </details>

### std::conditional
Если в обычном коде для ветвления логики мы можем использовать `if`, то в шаблонах вместо этого можно использовать
`std::conditional`. С помощью этого шаблона можно во время компиляции выбирать тип, который будет подставлен
в итоговое выражение. А если учесть, что многие шаблоны, используемые при метапрограммировании, представляют собой
шаблоны типов, то фактически с помощью `std::conditional` можно управлять логикой формирования кода.
Более-менее простой и понятный пример, как с помощью `std::conditional` выбрать наименьший тип для хранения положительного
целого числа

* <details>

  <summary>Пример кода</summary>

    ```c++
    #include <string>
    #include <sstream>
    #include <iostream>
    #include <type_traits>

    // С помощью цепочки условий (по аналогии с тернарным оператором) выбираем тип, подходящий под заданное количество байт
    template <std::uint8_t T_numBytes>
    using UintSelector =
        typename std::conditional<T_numBytes == 1, std::uint8_t,
            typename std::conditional<T_numBytes == 2, std::uint16_t,
                typename std::conditional<T_numBytes == 3 || T_numBytes == 4, std::uint32_t,
                    std::uint64_t
                >::type
            >::type
        >::type;

    // По аналогии с тем, как выше считали факториал, вычисляем количество полезных бит в числе
    template<std::uint64_t n>
    struct BitsCounter {
        enum {value = BitsCounter<n / 2>::value + 1};
    };

    template<>
    struct BitsCounter<1> {
        enum {value = 1};
    };

    template<>
    struct BitsCounter<0> {
        enum {value = 1};
    };

    // Конвертируем биты в байты
    template<std::uint64_t n>
    struct BytesCounter {
        enum {value = (BitsCounter<n>::value + 7) / 8};  // хак с +7, чтобы предотвратить округление вниз
    };

    template<std::uint64_t n>
    UintSelector<BytesCounter<n>::value> AutoSizedInteger() {
        return n;
    }

    int main() {
        // тип переменных автоматически подстраивается под количество байт, необходимых для хранения чисел
        auto oneByte = AutoSizedInteger<150>();
        auto twoBytes = AutoSizedInteger<65000>();
        auto fourBytes = AutoSizedInteger<100500>();
        auto eightBytes = AutoSizedInteger<1000000000000000>();

        std::cout << "oneByte = " << oneByte << std::endl;
        std::cout << "sizeof(oneByte) = " << sizeof(oneByte) << std::endl;

        std::cout << "twoBytes = " << twoBytes << std::endl;
        std::cout << "sizeof(oneBytes) = " << sizeof(twoBytes) << std::endl;

        std::cout << "fourBytes = " << fourBytes << std::endl;
        std::cout << "sizeof(fourBytes) = " << sizeof(fourBytes) << std::endl;

        std::cout << "eightBytes = " << eightBytes << std::endl;
        std::cout << "sizeof(oneBytes) = " << sizeof(eightBytes) << std::endl;
    };
    ```
  </details>

### std::decay
Выполняет следующие операции над переданным типом:
- Если тип является типом массива или ссылкой на тип массива, то `decay` вернет тип указателя на элемент массива.
Например, из типа `const char[5]` сделает тип `const char*`
- Если тип является функцией или ссылкой на функцию, то `decay` вернет указатель на функцию
- Иначе удалит с типа ссылку, а затем cv-модификаторы. Например, из `const int` сделает `int`, а из `const string&` сделает `string`

Зачем это все нужно. Часто в шаблонах в качестве параметров используются универсальные ссылки. При использовании
универсальных ссылок выведенный тип часто содержит `const` или ссылку. Мы можем захотеть сравнивать выведенный тип с другим
с помощью `std::conditional`, либо сделать специализацию шаблона для конкретных типов, но лишние данные о типе помешают нам его
правильно сравнить. В таком случае `decay` поможет избавиться от лишних данных и привести тип к определенному виду.

* <details>

  <summary>Пример кода</summary>

    ```c++
    // При использовании && с параметром шаблона возникает "универсальная ссылка",
    // которая при компиляции может быть преврещане в lvalue или rvalue ссылку в зависимости от переданного значения
    template<class T>
    void func(T&& param) {
        if (std::is_same<T, int>::value)
            std::cout << "param is an int\n";
        else
            std::cout << "param is not an int\n";
    }

    int main() {
        int three = 3;
        // мы передали переменную, универсальная ссылка T&& раскрылась как int&, проверка is_same<int&, int> не проходит
        func(three);  // выведет "param is not an int"
    }
    ```
  </details>

При использовании `std::decay` все работает. Конкретно в данном случае можно было использовать также `remove_const_t<remove_reference_t<T>>`
* <details>

  <summary>Пример кода</summary>
    ```c++
    template<class T>
    void func(T&& param) {
        if (std::is_same<typename std::decay<T>::type, int>::value)
            std::cout << "param is an int\n";
        else 
            std::cout << "param is not an int\n";
    }
    ```
  </details>

### constexpr if

Также была добавлена конструкция `if constexpr`, которая
позволяет компилировать ту или иную ветку кода в зависимости от условия, но при этом обходиться без SFINAE и шаблонов.

* <details>

  <summary>Пример с `constexpr if`</summary>

    ```c++
    template<int i>
    auto f() {
        if constexpr(i==0)
            return 10;
        else
            return std::string("hello");
    }

    int main() {
    std::cout<<f<0>(); // 10
    std::cout<<f<1>(); // hello
    }
    ```

  </details>

Без `if constexpr` мы бы не смогли сделать перегрузку функции f, отличающуюся только типом возвращаемого значения,
так как компилятор при вызове бы не смог понять по переданным аргументам, какой из вариантов реализации надо использовать.
Эту задачу можно было бы решить с помощью `std::conditional`, но это было бы более громоздко, чем с `if constexpr`.

Код во всех ветках кода под `if constexpr` должен быть валидным с точки зрения синтаксиса языка, вне зависимости от того,
какую из веток выберет компилятор в соответствии с условием. Но при этом мы можем работать с одной и той
же переменной как со значением разных типов в разных ветках, так как по сути при выборе какой-то ветки происходит
генерация новой функции по шаблону.

### std::tuple
`std::tuple` представляет из себя кортеж -- коллекция из элементов разного типа с фиксированным во время компиляции размером.
Является более общим вариантом пары `std::pair`, в отличие от которой количество элементов у кортежа может быть любым.

Кортеж можно использовать вместо массивов при работе с шаблонами. Можно итерироваться по кортежу с помощью variadic templates,
можно склеивать два кортежа в один с помощью `std::tuple_cat`. Например, посчитаем кортеж с числами Фибоначчи на
этапе компиляции

* <details>

  <summary>Пример кода с использованием `tuple`</summary>

    ```c++
    #include <string>
    #include <iostream>
    #include <type_traits>
    #include <tuple>

    using namespace std;

    template<size_t n>
    struct Fib {
        static constexpr auto value = tuple_cat(Fib<n - 1>::value, make_tuple(get<n - 2>(Fib<n - 1>::value) + get<n - 3>(Fib<n - 1>::value)));
    };

    template<>
    struct Fib<1> {
        static constexpr auto value = make_tuple<size_t>(1);
    };

    template<>
    struct Fib<2> {
        static constexpr auto value = make_tuple<size_t, size_t>(1, 1);
    };

    int main() {
        auto fibonacci = Fib<10>::value;
        cout << "Precalculated " << tuple_size<decltype(fibonacci)>() << " Fibonacci numbers" << endl;
        cout << get<0>(fibonacci) << " ";
        cout << get<1>(fibonacci) << " ";
        cout << get<2>(fibonacci) << " ";
        cout << get<3>(fibonacci) << " ";
        cout << get<4>(fibonacci) << " ";
        cout << get<5>(fibonacci) << " ";
        cout << get<6>(fibonacci) << " ";
        cout << get<7>(fibonacci) << " ";
        cout << get<8>(fibonacci) << " ";
        cout << get<9>(fibonacci) << " ";
        return 0;
    };
    ```

  </details>

### variadic templates
**Variadic template** -- шаблон с переменным числом параметров. 
* <details>

  <summary>Пример кода</summary>

    ```c++
    template<typename... Args>
    std::tuple<Args...> Pack(Args&&... args) {
        return std::make_tuple(std::forward<Args>(args)...);
    }
    ```

  </details>

При компиляции этого шаблона количество аргументов `args` будет зависеть от того, сколько их будет передано в том месте,
откуда вызывается шаблонная функция `Pack`. Конструкция `(forward<Args>(args)...)` раскроется как `(forward<Arg1Type>(arg1), forward<Arg1Type>(arg2), ..., forward<ArgNType>(argN))`
в зависимости от количества аргументов.

Также можно, добавив сюда `std::integer_sequence`, реализовать обратное действие -- разбить кортеж на отдельные параметры

* <details>

  <summary>Пример кода</summary>

    ```c++
    #include <iostream>
    #include <string>
    #include <tuple>
    #include <utility>
    #include <functional>

    using namespace std;

    template<typename... Args>
    tuple<Args...> Pack(Args&&... args) {
        return make_tuple(forward<Args>(args)...);
    }

    // Второй аргумент в этой функции сам по себе не нужен, он используется только для вывода variadic параметра Indices
    template<typename F, typename... Args, std::size_t... Indices>
    auto UnpackImpl(F f, const tuple<Args...>& t, index_sequence<Indices...>) {
        return f(get<Indices>(t)...);
        // тут строка раскрывается как f(get<0>(t), get<1>(t), ... и т.д. для каждого числа из списка Indices)
    }

    template<typename F, typename... Args>
    auto Unpack(F f, const tuple<Args...>& t) {
        return UnpackImpl(f, t, index_sequence_for<Args...>{});
    }

    template<typename F, typename... Args>
    auto Call(F f, Args&&... args) {
        return f(forward<Args>(args)...);
    }

    auto Foo(int a, string b, double c) {
        return b + to_string(a + c);
    }

    int main() {
        auto tup = Pack(1, string("hello"), 1.56);

        cout << get<0>(tup) << ", " << get<1>(tup) << ", " << get<2>(tup) << endl;

        cout << Call(Foo, 10, "without tuple ", 0.5) << endl;
        cout << Unpack(Foo, tup) << endl;

        return 0;
    }
    ```
  </details>

Также можно пробежать по значениям из variadic параметра шаблона с помощью рекурсии

* <details>

  <summary>Пример кода</summary>

    ```c++
    #include <iostream>

    using namespace std;

    template<typename Arg>
    void Print(Arg&& arg) {
        cout << arg << endl;
    }

    template<typename Arg0, typename ...Args>
    void Print(Arg0&& arg0, Args&&... args) {
        cout << arg0 << " ";
        Print(args...);
    }

    int main() {
        Print(1, "hello", 42, 5.5);
        Print(1);
        // Print();  так не будет работать, нет подходящей реализации для вызова

        return 0;
    }
    ```

  </details>

# Семинар №15. 17 декабря
## Контрольная работа
* Напишите иерархию классов IShape2D, Circle, Rectangle, Square с минимальным объемом кода и максимальной расширяемостью
## Материалы
  [Решение и детали условия](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-15.cpp)

# Лекция №15. 13 декабря
## Метапрограммирование

### Что это такое
**Метапрограммирование** — вид программирования, связанный с созданием программ, которые порождают другие программы как результат своей работы.

В контексте языка C++ под метапрограммированием понимается генерация кода во время компиляции с помощью программ, состоящих из шаблонов.
То есть в C++ шаблоны можно использовать не только для создания обобщенных алгоритмов или структур, которые могут работать с разными типами данных,
но и для генерации различного кода в зависимости от каких-то исходных данных, известных на этапе компиляции.

В общем случае использование метапрограммирования считается крайней мерой. Если можно обойтись без него, то лучше
обходиться без него.

### Зачем нужно генерировать код на этапе компиляции?
* **Эффективность программы в рантайме.**
Если какой-то код можно выполнить один раз во время компиляции, то во время выполнения уже скомпилированной программы не
надо будет тратить время на повторные вычисления, а можно будет пользоваться готовым результатом, который компилятор подставит в код.
Пример с вычислением факториала в compile-time. Тут интересно обратить внимание, что в качестве параметра шаблона можно
принимать не только типы данных, но и какие-то константы


* <details>
  <summary>Пример вычисления факториала</summary>

    ```c++
    template<unsigned int n>
    struct Factorial {
        enum { value = n * Factorial<n-1>::value};
    };

    template<>
    struct Factorial<0> {
        enum {value = 1};
    };

    int main() {
        // При компиляции вычисленные значения будут подставлены в код, поэтому при запуске программы время
        // не будет затрачиваться на вычисление
        std::cout << Factorial<5>::value << std::endl;
        std::cout << Factorial<10>::value << std::endl;
    }
    ```
</details>


### **SFINAE** - substitution failure is not an error.
Бывает так, что для написания обобщенного кода недостаточно сделать просто подстановку произвольного типа данных
в качестве параметра шаблона, так как в зависимости от особенностей разных типов данных код для работы с ними может
отличаться. В этом случае также приходится прибегать к метапрограммированию, чтобы генерировать разные реализации шаблона
в зависимости от особенностей типов данных или каких-то еще параметров.
Например, мы хотим написать функцию, которая превращает переданное значение в строку.
При этом, если нам дали значение какого-то типа, у которого есть метод `ToString()`, то мы хотим использовать его,
а если нет, то `std::to_string`

* <details>

  <summary>Пример программы</summary>

    ```c++
    #include <string>
    #include <sstream>
    #include <iostream>

    using namespace std;

    /*
    * Такая запись объявления функции со стрелкой после скобок позволяет указывать
    * тип возвращаемого значения после списка аргументов. Далее с помощью decltype мы просим
    * компилятор самостоятельно вывести тип из выражения obj.ToString().
    * Если вывести тип не получилось (например, нет метода ToString в классе T, или тип T вообще не класс), то
    * компилятор просто пропустит этот шаблон и пойдет компилировать код дальше. Такое поведение называется 
    * SFINAE -- substitution failure is not an error
    */
    template<typename T>
    auto Serialize(const T& obj) -> decltype(obj.ToString()) {
        return obj.ToString();
    }

    template<typename T>
    auto Serialize(const T& obj) -> decltype(std::to_string(obj)) {
        return to_string(obj);
    }

    class Foo {
    public:
        string ToString() const {
            return "I am Foo";
        }
    };

    class Bar {
    public:
        string ToString() const {
            return "I am Bar";
        }
    };

    int main() {
        cout << Serialize(Foo()) << endl; // сработает подстановка в первый шаблон
        cout << Serialize(Bar()) << endl; // сработает подстановка в первый шаблон
        cout << Serialize(55) << endl; // сработает подстановка во второй шаблон
        cout << Serialize(1.15f) << endl; // сработает подстановка во второй шаблон
    };
    ```

    </details>

### **decltype** и **declval**. Если хочется обойтись без `auto Serialize() -> ...`, то можно воспользоваться `declval`
* decltype возвращает тип переданного выражения
* declval генерирует фейковое "значение" указанного типа, которое можно использовать в шаблонах для вывода типов. Для случаев нетривиального конструктора
* <details>

  <summary>Пример кода</summary>

    ```c++
    /*
    * declval генерирует фейковое "значение" указанного типа, которое можно использовать в шаблонах для вывода типов
    */
    template<typename T>
    decltype(declval<T>().ToString()) Serialize(const T& obj) {
        return obj.ToString();
    }

    template<typename T>
    decltype(std::to_string(declval<T>())) Serialize(const T& obj) {
        return to_string(obj);
    }
    ```
</details>

* Применение decltype и declval.
* <details>

  <summary>Пишем проверку наличия метода у объекта</summary>

    ```c++
    template<typename T> struct HasFoo{
    private:
        static int detect(...);
        template<typename U> static decltype(std::declval<U>().foo(42)) detect(const U&);
    public:
        static constexpr bool value = std::is_same<void, decltype(detect(std::declval<T>()))>::value;
    };
    ```

  </details>

### constexpr
Конструкции с шаблонами выглядят очень сложно, их трудно читать и отлаживать. Чтобы сделать метапрограммирование
на C++ проще, в язык было добавлено ключевое слово `constexpr`. С его помощью можно обозначить переменные и функции,
значение которых можно вычислить на этапе компиляции. Например, вычислить факториал на этапе компиляции можно и без шаблонов

* <details>

  <summary>Код факториала</summary>
    
    ```c++
    #include <string>
    #include <iostream>
    #include <chrono>

    using namespace std;

    // объявили функцию как constexpr, поэтому она может быть вычислена во время компиляции
    // но ее также можно вызывать и во время выполнения программы как любую другую функцию
    constexpr uint64_t factorial(int n) {
        uint64_t result = 1;
        for (int i = 2; i < n; ++i) {
            result *= i;
        }
        return result;
    }

    template<unsigned int n>
    struct Factorial {
        enum { value = n * Factorial<n-1>::value};
    };

    template<>
    struct Factorial<0> {
        enum {value = 1};
    };

    int main() {
        {
            auto start = std::chrono::high_resolution_clock::now();

            constexpr auto fact10000 = factorial(1000);

            auto finish = std::chrono::high_resolution_clock::now();
            std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() << "ns\n";
        }

        {
            auto start = std::chrono::high_resolution_clock::now();

            auto fact10000 = Factorial<1000>::value;

            auto finish = std::chrono::high_resolution_clock::now();
            std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() << "ns\n";
        }

        {
            auto start = std::chrono::high_resolution_clock::now();

            // если не объявляем переменную как константу, то компилятор не будет вычислять значение в момент компиляции
            auto fact10000 = factorial(1000);

            auto finish = std::chrono::high_resolution_clock::now();
            std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(finish-start).count() << "ns\n";
        }
        return 0;
    };
    ```

  </details>
  
Если в выводе время в случае с шаблоном и с constexpr отличается, то можно перезапустить пример несколько раз.
Разница во времени будет являться следствием погрешности измерений, а не реальной разницей в бинарном коде.

# Семинар №14. B-114 10 декабря
* Напоминание сети Mephist. Класс TUser. Метод TUser::Follow. Общее хранилище объектов TMephistNetwork.
* Класс TGroup. Метод AddMember.
* Интерфейс ISubject, общий для TGroup и TUser.
* Каждый интерфейс должен содержать виртуальный деструктор.
* Почему не бывает виртуальных конструкторов?
* Почему нет неявного каста из std::unordered_set<TUser*> к std::unordered_set<ISubject*>? Коротко про расположение базового класса в памяти. Указатель на базовый класс и класс наследник могут отличаться при множественном наследовании.
* Чем отличается private от public наследования?
* Интерфейс ISubjectIterator и метод ISubject::GetMembersIterator.
* Реализация TEmptySubjectIterator.
* Реализация TUnorderedSetSubjectIterator.
* Обобщение до TContainerSubjectIterator. Вызов CreateContainerSubjectIterator.
* Обобщение до метода ISubject::GetFollowersIterator.
## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-14-B-114.cpp)

# Семинар №14. B-118 10 декабря
* Пишем сеть Mephist.
* Класс TUser.
* IEvent + 2 события, реализующих интерфейс

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-1/code/lesson14B118)
* [Картинка с семинара] (https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lesson14B118/%D0%9A%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B0_%D1%81_%D1%81%D0%B5%D0%BC%D0%B8%D0%BD%D0%B0%D1%80%D0%B0.pdf)
* Во время написания вы увидели несколько явных косяков архитектуры нашей задумки
* [Диаграмма классов доработанная] (https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lesson14B118/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%B0%D1%80%D1%85%D0%B8%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0.pdf) 

# Лекция №14. 06 декабря
* Обработка ошибок в С (errno)
* Исключения в C++:
  * Копирование исключения в throw и catch
  * Исключения в конструкторе / деструкторе
  * Иерархия наследования исключений
  * Особенности catch (ловля конкретного типа исключения / всех исключений)
  * Операторы кидающие исключения: new, dynamic_cast
  * noexcept методы
* Сигналы, обработка сигналов
## Материалы
  [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-14.cpp)

# Семинар №13. 03 декабря
## Контрольная работа
* Напишите IntrusivePtr
## Материалы
  [Решение](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-13.cpp)

# Лекция №13. 28 ноября
* Абстрактный класс и чисто-виртуальная функция, override, дефолтная реализация, интерфейс
* Копирование полиморфного объекта. virtual TBase* Clone() с наследником TDerived* Clone() override
* Обязательный виртуальный деструктор. Почему не бывает виртуального конструктора?
* Множественное наследование
* Virtual table
## Материалы
  [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-13)

# Семинар №12. В-114. 26 ноября
* Социальная сеть:
  * Пользователи сети (TUser)
  * Умение подписываться на других пользователей
  * Поиск связи между двумя пользователями (с использованием динамического и статического полиморфизма)

# Семинар №12. В-118. 26 ноября
* Социальная сеть:
  * Пользователи сети (TUser)
  * Интерфейс для обхода соседей
## Материалы
* [Код B-114](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-12-B-114.cpp)
* [Код B-118](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-12-B-118.cpp)

# Лекция №12. 21 ноября
* Наследование:
  * Наследование полей
  * Наследование методов. Перекрытие
  * Явный вызов конструктора, методов и полей родительского класса
  * Порядок вызова конструкторов и деструкторов в иерархии
  * Наследование vs композиция
* Инкапсуляция:
  * Public, private, protected, примеры
  * Смена модификатора доступа в наследнике
* Полиморфизм:
  * Статический и динамический полиморфизм
## Материалы
  [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-12)

# Семинар №11. B-114. 19 ноября
* Напишите класс-функтор, который соответствует лямбде ```[&a, b, c = std::move(c)] (int d)```
* Реализуйте класс ```TSignal``` с методами ```Subscribe(callback)``` и ```Fire()```
* Реализуйте ```WrapIntoLogging```: функция принимает ```callback``` и возвращает ```callback```, перед запуском которого всегда производится запись в ```std::cout```
* Реализуйте ```WrapIntoTiming```: функция принимает ```callback```, замеряет время его работы и выводит его после исполнения
* Реализуйте ```WrapIntoMapping```: функция принимает ```callback```, меняющий один элемент контейнера, возвращает callback, меняющий каждый элемент контейнера таким же образом
* ```std::bind```
* Реализуйте ```BindFirst```: принимает функцию двух аргументов, возвращает функцию, первый аргумент которой подменен на фиксированное значение
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-11-B-114.cpp)

# Семинар №11. B-118. 19 ноября
* Напишите сортировку пузырьком, которая использует только auto и decltype, и не использует названия типов переменных
* Передайте лямбду в функцию через шаблоны
* Напишите свой finally, который в самом конце скоупа выполнит функцию, переданную в конструктор finally
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-11-B-118.cpp)

# Лекция №11. 15 ноября
## Auto
- auto x = 0, особенно хороша для сложных возвращаемых типов
- Проверка выведенных типов через static_assert(std::is_same<decltype(x), TSomeType>::value)
  - decltype(x) возвращает тип x
  - decltype(expr): T&, если expr соответствует l-value of type T, возвращает T в противном случае, когда r-value
  - decltype((x)) вернет decltype(x)&
  - Не вычисляет выражение в рантайме, а в compilation time выводит тип
- const int x = 0; auto y = x не перенесет const на y, нужно явно указать const auto y = x
- int m = 0; int& i = m; auto j = i выдаст int j, для переноса & надо явно написать auto& j = i
- auto& захватывает и const квалификатор
- auto&& работает как универсальная ссылка, сложная концепция, можно про нее не говорить
- for (const auto& x : a)
- for (auto it = a.begin(); it != a.end(); ++it)
- decltype на возвращаемое значение, когда оно неоднозначно выводится:
  - template <class T1, class T2> auto Function(T1 t1, T2 t2) { if (t1 < t2) { return t1 + 1; } else { return t2 + 1} } -> decltype(t1 + 1)
- std::vector<decltype(f())> results

## Lambda
- Мотивация, пример lambda, передача в std::sort
- Какая возможная реализация силами компилятора: уникальный класс со стейтом и вызовом TClass::Call
- Упрощенный синтаксис без (), если нет параметров
- Явное указание возвращаемого значения: где необходимо, когда можно обойтись без
- auto аргументы lambda и возможная реализация через шаблонный класс-функтор
- Передача аргумента по значению, по ссылке, по [arg = std::move(arg)]
- Изменение параметра - mutable lambda
- Как принимать lambda в своей функции - шаблонный параметр TFunction
- Передача lambda может быть тяжелой из-за стейта

## Structured bindings
- std::pair<int, double> p; auto [x, y] = p
- Аналогично со struct TStruct
- Аналогично с std::tuple
- for (const auto& [x, y] : vector of pairs)
- for (const auto& [key, value] : map)

## std::function
- std::function, чтобы принимать lambda без шаблонного параметра
- Чем плохо передавать шаблонный параметр?
  - Раздувается код, растет число сгенерированных классов, дольше компиляция
  - Тяжелее пользоваться классами с шаблонами: приходится везде указывать тип шаблонных аргументов: детали реализации выносятся наружу
  - std::function более тяжела, чем lambda, из-за виртуальной функции, динамической аллокации
  - std::function явно указывает сигнатуру

# Семинар №10. B-114. 12 ноября
## Динамическое управление памятью. Умные указатели. RAII
* Использование и мотивация std::shared_ptr и std::weak_ptr на примере TProfiler, TSensor
* Реализация TSharedPtr
* Реализация TWeakPtr

# Семинар №10. B-118. 12 ноября
## Move-семантика. Начало. 
* Если объект куда-то записывается, он должен стать "пустышкой".
* Если приняли переменную как r-value, дальше все равно передаем через move


## Материалы
* [Код B-114](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-10-B-114.cpp)
* [Код B-118](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-10-B-118.cpp)
* [Дополнительные материалы](https://en.cppreference.com/w/cpp/language/copy_elision) про copy-elision. [Статья](https://habr.com/ru/company/vk/blog/666330/) с пояснениями

# Лекция №10. 8 ноября
## Динамическое управление памятью. Умные указатели. RAII
* Категории выражений: l-value, r-value [reference](https://en.cppreference.com/w/cpp/language/reference)
* Конструктор/оператор копирования, перемещения
* Умные указатели:
  * как с помощью RAII решить проблему явного вызова delete
  * проблемы с владением ресурсом
  * [std::unique_ptr](https://en.cppreference.com/w/cpp/memory/unique_ptr) - единоличное владение ресурсом
    - отсутсвие конструктора/оператора копирования
  * [std::shared_ptr](https://en.cppreference.com/w/cpp/memory/shared_ptr) - раздельное владение ресурсом
    - счетчик ссылок
## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-10.cpp)

# Семинар №9. 5 ноября
## Самостоятельная работа
* Необходимо написать структуру, которая принимает в конструкторе некоторое число в виде непосредственно числа или строки, и хранит в себе информацию про тип значения
* По итогу должен работать `main` из кода задания
* Написанный код должен соблюдать указания из комментариев

<details>
  <summary>Код задания (под катом)</summary>

  ```cpp
  int main() {
    vector<TNumber> numbers;
    //содержит строку или число, лексикографическое сравнение
    numbers.emplace_back(TNumber{20});
    numbers.emplace_back(TNumber{10});
    numbers.emplace_back(TNumber{"10"});
    numbers.emplace_back(TNumber{9});
    numbers.emplace_back(TNumber{"10"});
    numbers.emplace_back(TNumber{"11"});
    numbers.emplace_back(TNumber{11});
    numbers.emplace_back(TNumber{"1"});
    numbers.emplace_back(TNumber{"30"});
    //оставить уникальные, std::sort + остальное со своей реализацией
    KeepUnique(numbers);
    assert(numbers.size() == 6);
    //вывод элемента - строчка (type, value)
    cout << "first " << numbers[0] << endl;
    cout << "all " << numbers << endl;
    //своя реализация
    cout << accumulate(numbers.begin(), numbers.end(), TNumber{""}) << endl;
    cout << "all " << numbers << endl;

    return 0;
  }
  ```

</details>

## Вариант рабочего кода
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-9.cpp)

# Лекция №9. 1 ноября
## Динамическое управление памятью. Умные указатели. RAII (часть I)
* <details>
    <summary>Длительность хранения объекта (storage duration)</summary>
    Storage duration определяет, какие правила применяются к созданию и уничтожению объектов.

    Storage duration бывает:
    * **automatic** — память для объекта выделяется в тот момент, когда поток выполнения заходит в зону видимости (scope), в котором переменная объявлена, и освобождается, когда поток выходит из scope;
    * **static** — память выделяется, когда программа начинает работу, и освобождается, когда программа завершает работу;
    * **dynamic** — выделение памяти контролируется с помощью вызовов new и delete.
    * **thread (thread-local)** — похоже на static storage duration, но применимо к потоку выполнения;


    ```c++
    int globalVar = 10;  // static storage duration

    int Foo(int a) {
        static int s = 15;  // static storage duration
        s += a;
        return s;
    }

    int Bar(int a) {
        int b = a + 100;  // automatic storage duration
        {
            int c = (b + a) * 3;  // automatic storage duration
            if (c % 2) {
                return c;
            }
        }
        // тут переменной c уже нет, так как мы вышли за зону видимости (scope)
        return b;
    }

    int main() {
        // переменная 'a' имеет automatic storage duration
        // но при этом объект типа int, адрес которого хранится в переменной 'a',
        // имеет dynamic storage duration
        int* a = new int();
    
        *a = 100500;
        cout << Foo(a) << endl;
        cout << Bar(a) << endl;
        delete a;
    
        return 0;
    }
    ```
</details>

* <details>
    <summary>new/delete</summary>
    Чтобы выделить память для объекта с dynamic storage duration, сущуствует оператор `new`.

    Чтобы освободить память, которая была выделена с помощью оператора `new`, существует оператор `delete`.

    ```c++
    int* CreateDynamicArray(int size) {
        // если при вызове new указать число в квадратных скобках,
        // то выделится такое количество памяти,
        // которое вмещает требуемое количество объектов данного типа
        return new int[size];
    }

    void DeleteDynamicArray(int* arr) {
        // если выделяли память через new с квадратными скобками, то и delete должен быть со скобками
        delete[] arr;
    }

    void DoSomethingWithMemoryLeak() {
        int* arr = CreateDynamicArray(1000);
        arr[0] = 1;
        arr[1] = 100500;
        // что-то делаем с массивом
    
        // если в этой функции мы не вызовем нигде DeleteDynamicArray,
        // то память не освободится
    }

    int main() {
        for (int i = 0; i < 1000000000; ++i) {
            DoSomethingWithMemoryLeak();
            // Количество памяти, потребляемое нашей программой,
            // увеличивается с каждой итерацией
        }
    }
    ```

    * Если оператор `new` используется с примитивными типами данных (`int`, `char`, `bool` и т.д., не являющиеся структурами и классами), то происходит просто выделение памяти.
    * Если оператор `new` используется со сложными типами, то после выделения памяти происходит также вызов конструктора с передачей в него аргументов.

    ```c++
    int* pA = new int[10]; 
    // выделили память под 10 целых чисел и ничего больше не делаем

    std::vector<int>* pB = new vector<int>(100);
    // выделили память под 1 вектор, а затем создали объект вектора,
    // передав в конструктор число 10
    ```

    * Соответственно оператор `delete` вызывает деструктор, если используется со сложными типами
</details>

* use after free
    * Если мы освободили память с помощью оператора `delete`, то её больше нельзя использовать. Операционная система может отдать этот участок памяти в пользование другой программе. Если обратиться к участку памяти, который используется другой программой, то операционная система может аварийно завершить нашу программу, послав сигнал SIGSEGV (Segmentation Fault).
    * При этом не обязательно при обращении к освобожденному участку памяти будет ошибка, иногда такое обращение может пройти безболезненно. Из-за этого обнаружить такую ошибку, просто запустив программу для отладки, бывает сложно. Для упрощения отладки таких проблем существует утилита `address sanitizer` (сокращенно `ASAN`), который собирает и запускает программу особым образом, чтобы неправильные обращения к адресам памяти сразу обнаруживались.

* RAII (Resource Acquisition Is Initialization)
    * Выше в примере про `new` и `delete` было показано, как может быть создана ситуация с утечкой памяти, когда выделение и освобождение памяти никак не связаны друг с другом. Никто не гарантирует, что после вызова оператора `new` и получения указателя на выделенную область памяти где-то в коде программы в дальнейшем будет вызван оператор `delete`.
    * Кроме выделенной памяти могут быть и другие ресурсы, которые программа может запрашивать у операционной системы, а затем должна освобождать.
Например, открытые для чтения или записи файлы в файловой системе, сокеты для отправки и получения данных.

    * Чтобы упростить управление такими ресурсами и уменьшить вероятность ошибки, существует подход **RAII**, который заключается в том, что при создании какого-то объекта мы в конструкторе получаем ресурсы, а в деструкторе их освобождаем. В комбинации с automatic storage duration это дает гарантированное автоматическое освобождение ресурсов в тот момент, как только выполнение кода выйдет из области видимости объекта.

    * Примером такого подхода могут служить контейнеры из STL (например, `vector`), которые динамически выделяют память для хранения объектов, а затем гарантированно её освобождают, как только объект контейнера будет уничтожен.

    * <details>
        <summary>Попробуем на примере рассмотреть, как работает этот подход</summary>

        ```c++
        class DynamicArray {
        public:
            DynamicArray(int size): Data_(new int[size]) {}
            ~DynamicArray() {delete[] Data_;}
    
            int& operator [](size_t i) {return Data_[i];}
            const int& operator [](size_t i) const {return Data_[i];}
        private:
            int* Data_;
        };

        void Foo() {
            // переменная arr имеет automatic storage duration
            // поэтому объект будет уничтожен при покидании области видимости
            // при уничтожении объекта будет вызван деструктор ~DynamicArray
            DynamicArray arr(1000);
            arr[0] = 1;
            arr[1] = 100500;
            // что-то делаем с массивом
      
            // в этой функции мы не должны беспокоиться об освобождении памяти,
            // так как C++ вызовет деструктор объекта arr автоматически,
            // как только завершится выполнение кода в функции Foo
        }

        int main() {
            for (int i = 0; i < 1000000000; ++i) {
                Foo();
                // Количество памяти, потребляемое нашей программой,
                // не увеличивается с каждой итерацией
            }
        }
        ```
      </details>

    * Также этот подход может быть использован не только для выделения и освобождения ресурсов, получаемых от операционной системы, а для выполнение любых действий, которые должны произойти в начале и в конце времени жизни объекта. Например, можно сделать таймер, который измеряет время от инициализации самого себя до конца времени своей жизни https://github.com/alexriegler/RAII-Stopwatch/blob/main/include/RAII-Stopwatch/RAII-Stopwatch.h. Если поместить такой таймер в scope какой-то функции, создав объект в самом начале, то можно замерить время её работы https://github.com/alexriegler/RAII-Stopwatch/blob/main/example/example.cpp.

* <details>
    <summary>placement new</summary>

    * Есть возможность вызвать оператор `new` таким образом, чтобы он не запрашивал выделение новой оперативной памяти у операционной системы, а использовал заранее выделенную облсть памяти. Такой вызов оператора называется `placement new`.

    ```c++
    char* buf  = new char[sizeof(string)]; // выделяем область памяти
    
    // placement new использует уже выделенную область памяти,
    // чтобы проинициализировать там объект типа string
    string* p = new (buf) string("hi");

    // обычный new выделяет новую область памяти и
    // инициализирует там объект типа string
    string* q = new string("hi");
    ```

    * Смысл использования `placement new` заключается в экономии времени на запрос выделения памяти в операционной системе. Можно заранее выделить большой кусок памяти, а затем инициализировать в нем разные объекты. 
    * При использовании `placement new` нельзя использовать `delete` для каждого объекта, который мы разместили в заранее выделенном буфере, так как нам не нужно возвращать память операционной системе. Вместо этого нужно будет вызвать `delete` единожды для всего буфера. Подробнее о том, почему не существует `placement delete`, можно почитать на сайте создателя языка C++ Бьерна Страуструпа https://www.stroustrup.com/bs_faq2.html#placement-delete.
  </details>

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-9.cpp) с лекции без изменений
* [Статья](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D0%B5%D0%BA_%D0%B2%D1%8B%D0%B7%D0%BE%D0%B2%D0%BE%D0%B2) про стек вызовов
* [Статья](https://habr.com/ru/company/smart_soft/blog/185226/) про память процесса

# Семинар №8. 29 октября
* Напоминание std::remove_if, std::remove
* Реализация RemoveIf, Remove
* std::nth_element, std::accumulate
* std::optional на примере FindByCondition
* Почему размер std::optional<int> составляет 8 байт, а не 5-6? Выравнивание, alignof
* std::variant, std::visit
* [Код B-114](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-8-B-114.cpp)

# Лекция №8. 25 октября
## STL Алгоритмы: sort, lower bound, upper bound, find, remove if, merge. Простые структуры: pair, optional
### Общий подход к алгоритмам STL - через итераторы
В библиотеке STL в заголовочном файле `algorithm` реализованы различные алгоритмы для работы с последовательностями элементов.
Общая черта реализаций этих алгоритмов заключается в том, что они не заточены под какие-то конкретные типы контейнеров, а умеют работать с итераторами, с помощью которых можно пробежать по данной последовательности.
В разных алгоритмах требуются итераторы с различными свойствами.
Где-то достаточно итератора, который умеет только перемещаться к следующему элементу  -- `InputIterator` (дает возможность пробежать по элементам один раз), `ForwardIterator` (гарантирует, что можно будет пробежать по элементам много раз) или `OutputIterator` (дает возможность не только читать данные, но и записывать)
Где-то нужен итератор, котоырй умеет перемещаться к следующему и к предыдущему элементу -- `BidirectionalIterator`
Где-то нужен итератор, который умеет перемещаться к любому элементу в последовательности за константное время -- `RandomAccessIterator`

### Преимущества: универсальность, гибкость, Недостатки: много писать, редко нужна такая гибкость
Такой подход к организации библиотеки `algorithm` позволяет использовать реализации алгоритмов не только с определенными контейнерами из стандартной библиотеки.
Эти алгоритмы можно адаптировать к собственным структурам данных, либо запускать поверх генерирующихся "на лету" данных, если обернуть взаимодействие со своей структурой данных в итератор с определенными свойствами.
В некоторых ситуациях такой подход застявляет писать больше кода, хотя требуется сделать какое-то простое действие над стандартным контейнером (например, отсортировать вектор `std::sort(vec.begin(), vec.end(), std::greater)`)

В стандартной библиотеке представлено множество различных функций. Рассмотрим некоторые примеры:
* <details>
    <summary>std::sort</summary>
    Одно из возможных объявлений

    ```cpp
    template< class RandomIt, class Compare >
    constexpr void sort( RandomIt first, RandomIt last, Compare comp );
    ```

    Сортирует элементы в диапазоне от `first` до `last`, используя компаратор `comp`.
    Если не передать компаратор, то используется `std::less`.
    Порядок между элементами, которые равны с точки зрения компаратора, может в результате оказаться быть любым.

    Асимптотика `O(N·log(N))`, где `N = std::distance(first, last)`
  </details>

* <details>
    <summary>std::stable_sort</summary>
    Одно из возможных объявлений

    ```cpp
    template< class RandomIt, class Compare >
    void stable_sort( RandomIt first, RandomIt last, Compare comp );
    ```

    Сортирует элементы в диапазоне от `first` до `last`, используя компаратор `comp`.
    Если не передать компаратор, то используется `std::less`.
    Порядок между элементами, которые равны с точки зрения компаратора, сохраняется таким же, который был до сортировки.

    Асимптотика:
    - Если доступна дополнительная память, то `O(N·log(N))`, где `N = std::distance(first, last)`
    - Иначе `O(N·log(N)2)`
  </details>

* <details>
    <summary>std::next_permutation</summary>

    Одно из возможных объявлений
    ```c++
    template< class BidirIt, class Compare >
    bool next_permutation( BidirIt first, BidirIt last, Compare comp );
    ```

    Совершает перестановку элементов `[first, last)`, делая лексикографически следующую (с точки зрения данного компаратора или оператора `operator <`) перестановку.
    Если лексикографически следующая перестановка возможна, то функция возвращает `true`, иначе функция возвращает `false` и переставляет элементы таким образом, что они составляли лексикографически наименьшую перестановку.

    Асимптотика:
    Маскимально `N/2` обменов, где `N = std::distance(first, last)`.
    В среднем по всем перестановкам для одной последовательности элементов получается около 3 сравнений и 1.5 обмена для генерации одной перестановки.
  </details>

* <details>
    <summary>std::prev_permutation</summary>
    Одно из возможных объявлений
    ```c++
    template< class BidirIt, class Compare >
    bool prev_permutation( BidirIt first, BidirIt last, Compare comp );
    ```

    Совершает перестановку элементов `[first, last)`, делая лексикографически предыдущую (с точки зрения данного компаратора или оператора `operator <`) перестановку.
    Если лексикографически предыдущая перестановка возможна, то функция возвращает `true`, иначе функция возвращает `false` и переставляет элементы таким образом, что они составляли лексикографически наибольшую перестановку.

    Асимптотика:
    Маскимально `N/2` обменов, где `N = std::distance(first, last)`.
    В среднем по всем перестановкам для одной последовательности элементов получается около 3 сравнений и 1.5 обмена для генерации одной перестановки.
  </details>

* <details>
    <summary>std::erase, std::remove_if</summary>

    Возможные объявления
    ```cpp
    template< class ForwardIt, class T >
    ForwardIt remove( ForwardIt first, ForwardIt last, const T& value );
    ```

    ```cpp
    template< class ForwardIt, class UnaryPredicate >
    ForwardIt remove_if( ForwardIt first, ForwardIt last, UnaryPredicate p );
    ```

    Перемещяет в конец данного диапазона элементы, равные `value` или те элементы, для которых предикат `p` является истинным.
    Относительный порядок оставшихся в поледовательности элементов сохраняется.
    Возвращает указатель на первый элемент после оставшихся.

    Хорошо комбинируется с методом `erase` многих контейнеров. Например
    ```cpp
    struct TIsEven {
      bool operator ()(int it) {
        return it % 2 == 0;
      }
    };

    std::vector<int> vec = {1, 2, 3, 4, 5, 6, 7};
    vec.erase(std::remove_if(vec.befin(), vec.end(), TIsEven()), vec.end());
    // vec == {2, 4, 6}
    ```

    Асимптотика:
    `O(N)`, где `N = std::distance(first, last)`
  </details>

* <details>
    <summary>sort + unique + erase</summary>

    Простой способ оставить только уникальные элементы:
    * отсортировать элементы функцией `sort`
    * удалить все одинаковые элементы, идущие подряд, с помощью функции `unique`
    * удалить всё лишнее вызовом метода `erase` у контейнера
    ```cpp
    vector<int> vec = {9, 4, 4, 1, 0, 7, 5};
    sort(vec.begin(), vec.end());
    vec.erase(unique(vec.begin(), vec.end()), vec.end());
    // vec == {0, 1, 4, 5, 7, 9}
    ```
  </details>

### Структура данных std::pair (кортеж из двух элементов)
Пара, кортеж из двух элементов, тип каждого элемента задается в параметрах шаблона.

```cpp
std::pair<int, float> p;

// можно инициализировать по-разному
p = {1, 2.5f};
p = std::make_pair(2, 6.7f);

// доступ к элементам
std::cout << p.first << std::endl;
std::cout << p.last << std::endl;
```

"Из коробки" определен оператор `operator <`, который работает примерно так
```cpp
template<typename T1, typename T2>
bool operator <(const pair<T1, T2>& p1, const pair<T1, T2>& p2) {
    return p1.first < p2.first || (p1.first == p2.first && p1.second < p2.second);
}
```

Также сразу определены операторы `operator >` и `operator ==`.

## Материалы
* Причёсанный [код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-8.cpp) с лекции

# Семинар №7. 22 октября
* Использование stl контейнеров 
* Реализация string_view

## Материалы
* [Код 1](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-7-B-114.cpp)
* [Код 2](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-7.cpp)

# Лекция №7. 18 октября
* C-строки
    * По факту это массив типа `char` (последовательность байтов), но от обычного массива отличается наличием особого элемента в конце — нулевого байта `\0`
    * Можно создать переменную типа `char[]` и инициализировать ее как обычный массив через `{'s', 't', 'r', 'o', 'k', 'a', '\0'}`, а можно как строковую константу `"stroka"`, если задавать как константу, то нулевой байт компилятор подставляет автоматически
    * Также можно создать массив динамически любым способом (`new`, `malloc`, `std::array`, `std::vector`), и интерпретировать указатель на него как строку, если в последнем элементе лежит `\0`
    * <details>
        <summary>Есть стандартная библиотека для работы с c-строками `string.h`</summary>
          Полезные функции (запоминать сигнатуру не нужно, просто знать, что есть такая библиотека и что она в целом умеет)
        
          | Функция | Описание |
          | --- | --- |
          | char *strcat(char *s1, char *s2) | присоединяет s2 к s1, возвращает s1 |
          | char *strncat(char *s1, char *s2, int n) | присоединяет не более n символов s2 к s1, завершает строку символом '\0', возвращает s1 |
          | char *strсpy(char *s1, char *s2) | копирует строку s2 в строку s1, включая '\0', возвращает s1 |
          | char *strncpy(char *s1, char *s2, int n) | копирует не более n символов строки s2 в строку s1, возвращает s1; |
          | int strcmp(char *s1, char *s2) | сравнивает s1 и s2, возвращает значение 0, если строки эквивалентны |
          | int strncmp(char *s1, char *s2, int n) | сравнивает не более n символов строк s1 и s2, возвращает значение 0, если начальные n символов строк эквивалентны |
          | int strlen(char *s) | возвращает количество символов в строке s |
          | char *strset(char *s, char c) | заполняет строку s символами, код которых равен значению c, возвращает указатель на строку s |
          | char *strnset(char *s, char c, int n) | заменяет первые n символов строки s символами, код которых равен c, возвращает указатель на строку s |

        </details>


* `std::string`  — класс строки в c++ из стандартной библиотеки. Хедер `string` (не путать с `string.h`)
    * Внутри себя содержит указатель на c-строку. Его можно получить методом `c_str()`
    * Объект типа `std::string` владеет этим указателем, поэтому вручную следить за выделением и освобождением памяти не нужно
    * Память для хранения строки в `std::string` выделяется динамически, что позволяет делать операции со строкой, изменяющие её длину. Например `push_back`, `pop_back`, `replace`, `insert`, `erase`, `append`, `operator +=`
    * Итерироваться по `std::string` можно так же, как и по `std:vector`. Нулевой байт, находящийся в конце строки, не учитывается в методах `size` и `length`, а также не входит в диапазон от `begin` до `end`
    * В отличие от c-строк объекты типа `std::string` можно сравнивать через операторы `==, <, >`, и т.д., так как сравниваться будут не указатели, а содержимое строк
    * Есть разные удобные методы для поиска в строке

* `std::string_view` — обертка над c-строкой, которая не владеет содержимым
    * Нельзя делать модифицирующие операции над строкой, указатель на которую лежит внутри
    * Так как строка внутри неизменна, то длину можно посчитать один раз и хранить её явно внутри `std::string_view`, поэтому метод `length` и `size` работают быстро
    * Разницы между `begin, end` и `cbegin, cend` по сути нет, так как данные внутри константные всегда
    * Можно сместить указатель на начало строки или на конце строки внутри `string_view` с помощью методов `remove_prefix` и `remove_suffix`, при этом с данными в памяти ничего не произойдет и строка останется неизменной, просто конкретный объект `string_view` будет ссылаться на часть строки
    * Зачем нужен `string_view` — если в какой-то функции нужно получить на вход константную строку, но не хочется забирать себе владение этой строкой. Отличие от передачи `const std::string&` в том, что при использовании `string_view` можно будет передать в функцию c-строку или строковый литерал без инициализации объекта `std::string`, поэтому не будет лишнего копирования данных

* Потоки ввода-вывода
    
    ![Диаграмма классов](https://upload.cppreference.com/mwiki/images/0/06/std-io-complete-inheritance.svg)
    
    * Базовые классы потоков:
        * В базовых классах `ios_base, basic_ios<CharT, Traits>` содержится общая функциональность, которая нужна для различных типов потоков ввода-вывода (мы их сейчас не рассматриваем)
        * `basic_ostream<CharT, Traits>` шаблон базового класса для потоков вывода. В этом классе переопределяется `operator <<`, который используется для записи данных в поток. Для потока типа `char` используется специализация `std::ostream`.
        * `basic_istream<CharT, Traits>` шаблон базового класса для потоков ввода. В этом классе переопределяется `operator >>`, который используется для чтения данных из потока. Для потока типа `char` используется специализация `std::istream`.
        * `basic_iostream` наследуется сразу от `basic_istream` и `basic_ostream`, поэтому содержит переопределенные операторы `operator <<` и `operator >>`, и поддерживает как операции ввода, так и вывода.
    * Классы, упомянутые выше, используются как базовые классы для своих реализаций потоков, либо для использования полиморфизма в каком-то общем коде.
    * <details>
        <summary>Про операторы ввода/вывода и их перегрузку</summary>
        В потоки можно писать и читать не только строки, но и данные других типов. Для этого нужно, чтобы была определена соответствующая перегрузка `operator >>` или `operator >>`. Например, если у нас есть какой-то свой класс `class Foo`, то можно перегрузить оператор вывода в поток, который будет доставать , и затем объекты этого типа можно будет писать в потоки.

        ```cpp
        class Foo {
        public:
          std::string to_string() const;
        };
            
        std::ostream& operator << (std::ostream& stream, const Foo& foo) {
          return stream << foo.to_string();  // пусть в классе Foo есть метод to_string()
        }
            
        // или например
    
        class Foo {
          friend std::ostream& operator <<(std::ostream& stream, const Foo& foo);
        public:
          Foo(int a): a(a) {}
        private:
          int a;
        };
            
        std::ostream& operator <<(std::ostream& stream, const Foo& foo) {

        }
        ```
      </details>

  * Более конкретные классы потоков из стандартной библиотеки:
    * `std::stringstream` — поток ввода-вывода. В него можно сначала записать данные с помощью `operator <<`, а затем прочитать их же с помощью `operator >>`. Типичное применение — сборка строки из данных разного типа, поддерживающих запись в поток.
    * <details>
        <summary>Пример кода</summary>
      
  
        ```cpp
        std::stringstream ss;
        ss << 5 << " + " << 5 << " = " << 10;
        std::string s;
        ss >> s;
        // теперь в строке s лежит "5 + 5 = 10"
    
        // также можно было получить строку по-другому
        std::string s1 = ss.str();
    
        // или string_view
        std::string_view sv = ss.view();
        ```
      </details>

    * `std::ifstream`, `std::ofstream` , `std::fstream` — поток ввода из файла, поток вывода в файл, поток ввода-вывода в файл соответственно. При создании экземпляра класса указывается путь к файлу. Записывая или читая данные через эти потоки мы работаем с файлом в файловой системе.
    * Глобальные потоки в стандартной библиотеке. В файле `iostream` объявлены переменные `cin`, `cout`, `cerr`, которые являются стандартными потоками ввода, вывода и вывода ошибок. При запуске программы операционная система предоставляет три стандартных потока, работать с которыми можно при помощи этих объектов.
        * Поток `cin` имеет тип `std::istream`, `cout` и `cerr` имеют тип `std::ostream`
        * Стандартный поток ввода обычно получает данные, которые пользователь вводит с клавиатуры в терминале, либо в него направляют данные, полученные из стандартного потока вывода другой программы, либо данные из файла
        * Стандартный поток вывода обычно выводит данные в терминал, либо в файл, либо на ввод другой программы
        * Стандартный поток вывода ошибок работает аналогично
    * Метод [flush](https://en.cppreference.com/w/cpp/io/basic_ostream/flush) записывает все буферизированные данные в выходной поток. Работает медленно, но нужно для синхронизации программ, общающихся через файл
        * <details>
            <summary>Как общаются две программы общаться через стандарный поток входа/выхода в `unix`</summary>

            ```bash
            # пример из unix
    
            # программа tee считывает данные из стандартного потока ввода и
            # выводит их же на стандартный поток вывода
    
            # программа echo получет строку в виде аргумента на старте и
            # выводит её на стандартный поток вывода
    
            # программа получает ввод с клавиатуры
            tee
            # ввели "lol"
            # на экране получили "lol"
    
            # перенаправляем вывод одной программы на ввод другой через пайп
            echo lol | tee
            # на экране получили "lol"
    
            # перенаправляем вывод одной программы на ввод другой через переопределение потока ввода
            tee <<(echo lol)
            # на экране получили "lol"
    
            # перенаправляем вывод программы в файл
            echo lol > /tmp/lol.txt
            # в файл /tmp/lol.txt записалась строка "lol"
    
            # перенаправляем содержимое файла на ввод программе
            tee < /tmp/lol.txt
            # на экране получили "lol"
            ```
          </details>
        * Можно написать свой аналог `tee`

## Материалы
* Причёсанный [код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-7.cpp) с лекции

# Семинар №6. 15 октября
## Самостоятельная работа
* Необходимо написать итератор для шаблонного класса `TZipper`, который внутри содержит 2 вектора
* По итогу должен работать `range based for loop` для класса `TZipper`

<details>
  <summary>Код задания (под катом)</summary>

  ```cpp
  template <typename F, typename S>
  struct TPair {
      F First;
      S Second;
  };

  template <typename F, typename S>
  class TZipIterator {
      //TODO
      TPair operator *();
  };

  template <typename F, typename S>
  class TZipper {
  public:
      TZipper(const std::vector<F>& first, const std::vector<S>& second);
      //TODO

  private:
      const std::vector<F>& First_;
      const std::vector<S>& Second_;
  };

  int main() {
      vector<int> a = {1, 2, 3};
      vector<string> b = {"abc", "def", "zxc"};
      TZipper z(a, b);

      for (TPait<int, string> it : z) {
          cout << it.First << " " << it.Second << endl;
      }
      //1 abc
      //2 def
      //3 zxc
  }
  ```

</details>

## Вариант рабочего кода
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-6.cpp)

# Лекция №6. 11 октября

## Что такое STL
**Библиотека стандартных шаблонов (STL) (англ. Standard Template Library)** — набор согласованных обобщённых алгоритмов, контейнеров, средств доступа к их содержимому и различных вспомогательных функций в C++.

В библиотеке выделяют пять основных компонентов:

* **Контейнер (англ. container)** — хранение набора объектов в памяти.
* **Итератор (англ. iterator)** — обеспечение средств доступа к содержимому контейнера.
* **Алгоритм (англ. algorithm)** — определение вычислительной процедуры.
* **Адаптер (англ. adaptor)** — адаптация компонентов для обеспечения различного интерфейса.
* **Функциональный объект (англ. functor)** — сокрытие функции в объекте для использования другими компонентами.

## Контейнеры в STL

* [Основная](https://en.cppreference.com/w/cpp/container) страница, посвящённая библиотеке контейнеров

| Контейнер|	Описание|
|----------|------------|
|Последовательные контейнеры|
|`vector`| C-подобный динамический массив произвольного доступа с автоматическим изменением размера при добавлении/удалении элемента. Доступ по индексу за `O(1)`. Добавление-удаление элемента в конец `vector` занимает амортизированное `O(1)` время, та же операция в начале или середине vector — `O(n)`. Стандартная быстрая сортировка за `O(n*log(n))`. Поиск элемента перебором занимает `O(n)`. Существует специализация шаблона `vector` для типа `bool`, которая требует меньше памяти за счёт хранения элементов в виде битов, однако она не поддерживает всех возможностей работы с итераторами.|
|`list`| Двусвязный список, элементы которого хранятся в произвольных кусках памяти, в отличие от контейнера `vector`, где элементы хранятся в непрерывной области памяти. Поиск перебором медленнее, чем у вектора из-за большего времени доступа к элементу. Доступ по индексу за `O(n)`. В любом месте контейнера вставка и удаление производятся очень быстро — за `O(1)`.|
|`forward_list`| Односвязный список. Устройство аналогично двусвязному, но с отличием - каждый элемент хранит связь только в одну сторону|
|`deque`| Двухсторонняя очередь. Контейнер похож на `vector`, но с возможностью быстрой вставки и удаления элементов на обоих концах за `O(1)`. Реализован в виде двусвязанного списка линейных массивов. С другой стороны, в отличие от `vector`, двухсторонняя очередь не гарантирует расположение всех своих элементов в непрерывном участке памяти, что делает невозможным безопасное использование арифметики указателей для доступа к элементам контейнера.|
|`array`| C-подобный статический массив произвольного доступа. Контейнер похож на `vector`, но без возможности изменения количества элементов в процессе выполнения программы. Реализован в виде обертки с удобными методами над обычным C-массивом|
|Ассоциативные контейнеры|
|`set`| Упорядоченное множество уникальных элементов. При вставке/удалении элементов множества итераторы, указывающие на элементы этого множества, не становятся недействительными. Обеспечивает стандартные операции над множествами типа объединения, пересечения, вычитания. Тип элементов множества должен реализовывать оператор сравнения `operator<`, или требуется предоставить функцию-компаратор. Реализован на основе самобалансирующего дерева двоичного поиска.|
|`multiset`| То же, что и `set`, но позволяет хранить повторяющиеся элементы.|
|`map`| Упорядоченный ассоциативный массив пар элементов, состоящих из ключей и соответствующих им значений. Ключи должны быть уникальны. Порядок следования элементов определяется ключами. При этом тип ключа должен реализовывать оператор сравнения `operator<`, либо требуется предоставить функцию-компаратор.|
|`multimap`| То же, что и `map`, но позволяет хранить несколько одинаковых ключей.|
|`unordered_map`| Неупорядоченный ассоциативный массив пар элементов, состоящих из ключей и соответствующих им значений. Ключи должны быть уникальны. Для типа, используемого в качестве ключа, должна быть определена операция `std::hash`, либо задана кастомная функция для вычисления хеша. Реализован на основе хеш-таблицы с бакетами.
|`unordered_set`| Неупорядоченное множество элементов. Значения должны быть уникальны. Для типа, используемого в качестве значений, должна быть определена операция `std::hash`, либо задана кастомная функция для вычисления хеша. Реализован на основе хеш-таблицы с бакетами.
|Контейнеры-адаптеры|
|`stack`| Стек — контейнер, в котором добавление и удаление элементов осуществляется с одного конца.|
|`queue`| Очередь — контейнер, с одного конца которого можно добавлять элементы, а с другого — вынимать.|
|`priority_queue`| Очередь с приоритетом, организованная так, что самый большой элемент всегда стоит на первом месте.|

## Особенности контейнеров
* Общий интерфейс и сложность различных операций
* map - красно-чёрное дерево
* unordered_map - хеш-таблица с бакетами
* Метод map::lower_bound, map::upper_bound (за logN)
* Функции lower_bound, upper_bound за N для map
* Написание своего [хеша](https://en.cppreference.com/w/cpp/utility/hash)

## Материалы
* [Таблица](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/containers-complexity.md)

# Семинар №5. 8 октября
## Написание своего итератора
* Написали свой класс `map<int, map<int, int>>`
* Написали итератор по нему для вызова `range based for loop` поверх этого класса
* Итерирование в порядке "возрастание первого ключа и возрастание второго ключа"
* Итерирование в порядке "возрастание первого ключа и убывание второго ключа"
## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-5-B-114.cpp)
* [Код v2](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-5-B-118.cpp)

# Лекция №5. 4 октября
## Итераторы
* Итератор - объект, идентифицирующий элемент коллекции данных (последовательности)
* Для чего нужны
* Где используются итераторы
* Рассмотреть примеры из документации:
  * [`std::vector`](https://en.cppreference.com/w/cpp/container/vector).
  * [`std::sort`](https://en.cppreference.com/w/cpp/algorithm/sort).
  * [`Range-based for loop`](https://en.cppreference.com/w/cpp/language/range-for).

## Классификация
|                        |read, increment by 1| multiple passes increment| decrement| random access| contiguous storage| write, increment by 1|
|------------------------|--------------------|--------------------------|----------|--------------|-------------------|----------------------|
| `InputIterator`        | +                  | -                        | -        | -            | -                 | -                    |
| `ForwardIterator`      | +                  | +                        | -        | -            | -                 | -                    |
| `BidirectionalIterator`| +                  | +                        | +        | -            | -                 | -                    |
| `RandomAccessIterator` | +                  | +                        | +        | +            | -                 | -                    |
| `ContiguousIterator`   | +                  | +                        | +        | +            | +                 | -                    |
| `OutputIterator`       | -                  | -                        | -        | -            | -                 | +                    |
* mutable iterators
* Аналогичная [таблица](https://en.cppreference.com/w/cpp/iterator) на cppreference.

## Общие функции
* [`std::advance`](https://en.cppreference.com/w/cpp/iterator/advance). Меняет итератор, без копирования
* [`std::next`](https://en.cppreference.com/w/cpp/iterator/next). Копирует итератор и возвращает копию
* [`std::prev`](https://en.cppreference.com/w/cpp/iterator/prev). Только для biderectional
* [`std::distance`](https://en.cppreference.com/w/cpp/iterator/distance)

## Примеры и применение
* На странице контейнера указано, какими свойствами обладает его итератор
* `ForwardIterator`
  * [`std::forward_list`](https://en.cppreference.com/w/cpp/container/forward_list)
* `BiderectionalIterator`
  * [`std::list`](https://en.cppreference.com/w/cpp/container/list)
* `RandomAccessIterator`
  * [`std::dequeue`](https://en.cppreference.com/w/cpp/container/deque)
* `ContiguousIterator`
  * [`std::vector`](https://en.cppreference.com/w/cpp/container/vector)
* `OutputIterator`
  * [`insert_iterator`](https://en.cppreference.com/w/cpp/iterator/insert_iterator)

## Adaptors
* `reverse_iterator`
  * Существует только для `BiderectionalIterator`
  * Итерирование по нему даёт обход в обратном порядке
* [`std::inserter`](https://en.cppreference.com/w/cpp/iterator/inserter) ([`std::insert_iterator`](https://en.cppreference.com/w/cpp/iterator/insert_iterator))
* [`std::front_inserter`](https://en.cppreference.com/w/cpp/iterator/front_inserter)
* [`std::back_inserter`](https://en.cppreference.com/w/cpp/iterator/back_inserter)

## Инвалидация итераторов
* [`std::list`]([`std::list`](https://en.cppreference.com/w/cpp/container/list)). Инвалидация только при удалении
* [`std::vector`](https://en.cppreference.com/w/cpp/container/vector). Итераторы могут инвалидироваться

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-5.cpp)

# Семинар №4. 1 октября
## Git
* [О системе контроля версий](https://git-scm.com/book/ru/v2/Введение-О-системе-контроля-версий)
* [Справка по командам](https://git-scm.com/docs)

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-4.cpp)

# Лекция №4. 26 сентября
## Шаблонные функции
* Для чего нужны шаблоны (например: описание общих алгоритмов), пример std::max/min
* Шаблонные параметры по умолчанию
* Алгоритм выбора типа, использование *, &, const в шаблонах
* Проблемы, которые надо учитывать в шаблонах (примеры: наличие у типа конструкторов, копирования, операторы сравнения)
* Специализация шаблонов, для конкретного типа

## Шаблонные классы
* Шаблонные классы
* Шаблонные методы
* Частичная специализация классов

## Базовые STL контейнеры: vector, stack, queue
* STL контейнеры. Описание шаблонов [`std::vector<T, Allocator>`](https://en.cppreference.com/w/cpp/container/vector), [`std::stack<T, Container>`](https://en.cppreference.com/w/cpp/container/stack), [`std::queue<T, Container>`](https://en.cppreference.com/w/cpp/container/queue)
* Рассмотрение методов контейнеров, их асимптотики, устройство вектора
* Специализация [`std::vector<bool>`](https://en.cppreference.com/w/cpp/container/vector_bool)

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-4.cpp)

# Семинар №3. 24 сентября
## Конструкторы, деструкторы
* Конструктор:
  * Один, два, три аргумента
  * Дефолтный конструктор и = delete для его запрета
  * Список инициализаций полей. Зачем нужно еще и тело конструктора?
  * Порядок конструирования полей класса
  * Вызов конструктора в конструкторе
* Деструктор
* New, delete, new[], delete[] на примере простейшего TVector, мотивация определения деструктора
* Конструирование через TClass{}, TClass(), = TClass(), = TClass{}

## Операторы
* Копирование, присваивание, мотивация и разница между ними
* Operator [], возвращение ссылки, const, пример TPersonsDatabase
* Operator + на примере TBigInteger свободной функцией
* Другие операторы: *, *=, +=, *, -, unary -, [], (), ->, --obj, obj--

## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-3.cpp)

# Лекция №3. 20 сентября
## Struct
* Описание struct, примеры с данными
* Функция sizeof
* Понятие и создание объекта, как обращаться к полям (. и ->)
* Методы
* This
* Mutable поля
* Определение функции внутри класса и вне
* Inline функции
* Const поля и методы

## Class. Инкапсуляция
* Ключевое слово class
* Инкапсуляция: public, private, без protected
* Friend классы
* Указатель на метод класса. Синтаксис редкий

# Лекция №2. 13 сентября
## Указатели
* Устройство памяти, создание и уничтожение объектов
  * Устройство памяти в системе
  * Виды памяти: куча и стек
  * Оператор new
  * Оператор delete
* Указатели и их арифметика
  * Операторы new/delete
  * Указатель, владение памятью
  * Время жизни
  * Const qualifier в указателях (константный указатель + указатель на константу)
  * Возвращение из функции. Опасности из-за разрушения переменных
* С-style массив и применение арифметики указателей
  * Объявление и использование
  * Sizeof для вычесления размера массива
  * Арифметика указателей на примере обащения к массиву
  * Передача/возвращение базовых типов и c-style массива в/из функцию
* Ссылки mutable/const
  * Объявление и использование
  * Отличие от указателей
  * Передача в функцию по ссылке
  * Возвращение из функций
  * Const qualifier в ссылках
## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-2.cpp)

# Семинар №1. 10 сентября
## Базовый синтаксис языка C++
* Const qualifier
* Операторы
  * +, -, *, /
  * Битовые операции &, |, !, ^. Отличия от &&
  * ++
  * сравнения
  * ()
  * +=, <<, >>,>>=
* Условные операторы:
  * if
  * Тернарный оператор
* Циклы:
  * C предусловием: while, for
  * Циклы с постусловием
### Материалы
* [Документация](https://en.cppreference.com/w/cpp/language/operators) по операторам
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/seminar-1.cpp) с семинара

# Лекция №1. 6 сентября
## Базовый синтаксис языка C++
### План
* C++ - компилируемый, статически типизированный язык программирования
* Структура программы на примере Hello, world!
* Заголовочные файлы
* Namespace
* Кратко про cin/cout
* Объвление переменной, область видимости
* Базовые типы
  * signed/unsigned short/int/long
  * bool
  * char/wchar
  * float/double/long double
  * size_t
  * int8_t
* Приведение типов
* Объявление функции
### Материалы
* [Директория](https://gitlab.com/mephi-dap/history-and-materials/-/blob/main/programming-1/code/lecture-1) с кодом
