#include <iostream>

class IntrusivePtrCounter {
private:
    size_t counter_ = 0;

    void AddRef() { ++counter_; }
    size_t ReleaseRef() { return --counter_; }
    size_t GetRefCount() { return counter_; }

    template <class T>
    friend class IntrusivePtr;
};

template <class T>
class IntrusivePtr {
public:
    IntrusivePtr() : ptr_(nullptr) {}

    IntrusivePtr(T* ptr) : ptr_(nullptr) {
        Reset(ptr);
    }

    IntrusivePtr(const IntrusivePtr& ip) : ptr_(nullptr) {
        Reset(ip.ptr_);
    }

    IntrusivePtr(IntrusivePtr&& ip) : ptr_(nullptr) {
        Reset(ip.ptr_);
        ip.Reset();
    }

    ~IntrusivePtr() {
        Reset();
    }

    IntrusivePtr& operator=(const IntrusivePtr& rhs) {
        if (ptr_ == rhs.ptr_) {
            return *this;
        }
        Reset(rhs.ptr_);
        return *this;
    }

    IntrusivePtr& operator=(IntrusivePtr&& rhs) {
        if (ptr_ == rhs.ptr_) {
            return *this;
        }
        Reset(rhs.ptr_);
        rhs.Reset();
        return *this;
    }

    IntrusivePtr& operator=(T* ptr) {
        Reset(ptr);
        return *this;
    }

    operator bool() const {
        return ptr_ != nullptr;
    }

    T& operator*() {
        return *ptr_;
    }

    const T& operator*() const {
        return *ptr_;
    }

    T* operator->() {
        return ptr_;
    }

    const T* operator->() const {
        return ptr_;
    }

    void Reset(T* newPtr = nullptr) {
        if (ptr_ == newPtr) {
            return;
        }
        if (ptr_ && ptr_->ReleaseRef() == 0) {
            delete ptr_;
        }
        if (newPtr) {
            newPtr->AddRef();
        }
        ptr_ = newPtr;
    }

    T* Get() {
        return ptr_;
    }

    const T* Get() const {
        return ptr_;
    }

private:
    T* ptr_;
};

class Foo : public IntrusivePtrCounter {
public:
    Foo(int value): value_(value) {}
    int Bar() {
        return 100500;
    }
private:
    int value_;
    friend std::ostream& operator<<(std::ostream&, const Foo&);
};

std::ostream& operator<<(std::ostream& stream, const Foo& foo) {
    return stream << "Foo(" << foo.value_ << ')';
}

int main() {
    IntrusivePtr<Foo> ptr1;
    IntrusivePtr<Foo> ptr2;

    ptr1 = new Foo(1);
    std::cout << "*ptr1 = " << *ptr1 << std::endl;

    ptr1 = new Foo(2);
    std::cout << "*ptr1 = " << *ptr1 << std::endl;

    ptr2 = std::move(ptr1);
    std::cout << "ptr1 = " << ptr1.Get() << std::endl;
    std::cout << "*ptr2 = " << *ptr2 << std::endl;

    ptr1 = IntrusivePtr<Foo>(new Foo(11));
    std::cout << "*ptr1 = " << *ptr1 << std::endl;

    ptr1 = new Foo(12);
    std::cout << "*ptr1 = " << *ptr1 << std::endl;

    ptr1.Reset();
    std::cout << "ptr1 = " << ptr1.Get() << std::endl;

    if (!ptr1) {
        std::cout << "ptr1 is null: " << !ptr1 << std::endl;
    } else {
        return 1;
    }

    IntrusivePtr<Foo> ptr3 = new Foo(3);
    std::cout << "ptr3->Bar() = " << ptr3->Bar() << std::endl;

    const IntrusivePtr<Foo> ptr4 = new Foo(42);
    std::cout << "ptr4 is null: " << !ptr4 << std::endl;
    std::cout << "*ptr4 = " << *ptr4 << std::endl;

    return 0;
}
