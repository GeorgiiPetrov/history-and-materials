#include <iostream>
#include <list>
#include <vector>

using namespace std;

int main() {
    vector<int> b = {1, 2, 3, 4, 5};

    list<int> a;
    a.push_front(10);
    a.push_front(123);
    a.push_front(3456);
    a.push_front(346);
    a.push_front(0);

    //auto it = a.begin();
    //advance(it, 2);
    //copy(b.begin(), b.end(), inserter(a, it));
    copy(b.begin(), b.end(), back_inserter(a));
    copy(b.begin(), b.end(), front_inserter(a));

    for (int elem : a) {
        cout << elem << ' ';
    }
    cout << endl;

    //{
    //    auto it = a.end();
    //    do {
    //        it = prev(it, 1);
    //        cout << *it << ' ';
    //    } while (it != a.begin());
    //    cout << endl;
    //}

    //{
    //    for (auto it = a.rbegin(); it != a.rend(); ++it) {
    //        cout << *it << ' ';
    //    }
    //    cout << endl;
    //}

    //{
    //    for (int it : a) {
    //        cout << it << ' ';
    //    }
    //    cout << endl;
    //}

    return 0;
}
