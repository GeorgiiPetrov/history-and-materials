#include <iostream>
#include <algorithm>
#include <string.h>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

int main() {
    cout << "\n======PART 1=======\n" << endl;
    {
        char asString[100] = "c-string";
        char asArray[] = {'c', '-', 's', 't', 'r', 'i', 'n', 'g', '\0'}; // good
        char asArrayWithoutNull[] = {'c', '-', 's', 't', 'r', 'i', 'n', 'g'}; // bad
        char asArrayWithNullInMiddle[] = {'c', '\0', 's', 't', 'r', 'i', 'n', 'g'}; // correct, but strange

        cout << asString << endl; //c-string
        cout << asArray << endl; //c-string
        cout << asArrayWithoutNull << endl; //c-string<some chars> or coredump
        cout << asArrayWithNullInMiddle << endl; //c

        strcat(asString , asArray);
        strcat(asString , asArray);
        strcat(asString , asArray);
        strcat(asString , asArrayWithNullInMiddle);

        cout << asString << endl; //c-stringc-stringc-stringc-stringc
        cout << strlen(asString) << endl; //33
        cout << strcmp(asArrayWithoutNull, asArrayWithNullInMiddle) << " " << strcmp(asArrayWithoutNull, asArrayWithoutNull) << " " << strcmp(asArrayWithNullInMiddle, asArrayWithoutNull) << endl; //positive 0 negative
        cout << strncmp(asArrayWithoutNull, asArrayWithNullInMiddle, 1) << endl; //0
        cout << strncmp(asArrayWithoutNull, asArray, 4) << endl; //0
    }

    cout << "\n======PART 2=======\n" << endl;
    {
        string stdString = "std::string";
        cout << stdString << endl; //std::string
        cout << stdString.c_str() << endl; //std::string
        for (char c : stdString) {
            cout << c << ',';
        } //s,t,d,:,:,s,t,r,i,n,g,
        cout << endl;

        reverse(stdString.begin() + 2, stdString.end() - 2);
        cout << stdString << endl; //stirts::dng
        stdString  += "_suffix";
        cout << stdString  << endl; //stirts::dng_suffix

        cout << stdString.starts_with("st") << endl; // 1
        cout << stdString.starts_with("std") << endl; // 0

        stdString = "std::string";
        cout << stdString .find("st") << endl; // 0
        cout << stdString .rfind("st") << endl; // 5

        cout << "+++++" << endl;

        cout << stdString.find("rts") << endl; // some int, usually 2**64 -1
        cout << (stdString.find("rts") == string::npos) << endl; // 1
        cout << stdString.find_first_of("d:") << endl; // 2
        cout << stdString.find_first_not_of("std:") << endl; // 7
    }

    cout << "\n======PART 3=======\n" << endl;
    {
        string stdString = "stdString";
        string_view stringView(stdString);
        const string& stringRef(stdString);

        cout << stdString << " " << stringView << " " << stringRef << endl; //stdString stdString stdString
        stdString[0] = ':';
        stdString[1] = ':';
        stdString[2] = ':';
        cout << stdString << " " << stringView << " " << stringRef << endl; //:::String :::String stdString
    }

    cout << "\n======PART 4=======\n" << endl;
    {
        char cString[] = "c-string";
        string_view stringView(cString);
        const string& stringRef(cString);

        cout << cString << " " << stringView << " " << stringRef << endl; //c-string c-string c-string
        cString[0] = ':';
        cString[1] = ':';
        cout << cString << " " << stringView << " " << stringRef << endl; //::string ::string c-string

        stringView.remove_suffix(2);
        stringView.remove_prefix(1);
        cout << cString << " " << stringView << " " << stringRef << endl; //::string :stri c-string
    }

    cout << "\n======PART 4=======\n" << endl;
    {
        string buf = "________________________________________________________________________________very long string_with_data::";
        stringstream sstream(buf);
        //sstream << "asd"  <==> buf += "asd"
        //vector<int> a;
        //buf += to_string(a) <=> sstream << a
        ofstream toPrint("output.txt");
        string outputText = "some text to output";
        cout << outputText << endl;
        toPrint << outputText << endl;
        sstream << outputText << " " << outputText << " " << outputText;
        cout << sstream.str() << endl;
    }

    cout << "\n======PART 4=======\n" << endl;
    {
        string buf = "________________________________________________________________________________very long string_with_data::";
        stringstream sstream(buf, std::ios_base::app | std::ios_base::out);//(std::ios_base::ate | std::ios_base::out); // append
        //sstream << "asd"  == buf += "asd"
        //vector<int> a;
        //buf += to_string(a) <=> q << a
        ofstream out("output2.txt");
        string outputText = "some text to output";
        cout << outputText << endl;
        out << outputText << endl;
        sstream << outputText << outputText << outputText;
        cout << sstream.str() << endl;
    }

    {
        string s;
        while (cin >> s) {
            cout << "++++" << s << endl;
        }
    }

    {
        ifstream in("lecture-7.cpp");
        string s;
        while (in >> s) {
            cout << s << endl; // cout << s << '\n'; cout.flush();
        }
    }

    return 0;
}