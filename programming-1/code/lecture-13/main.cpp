#include <iostream>

struct B {
    int b;  // 4B

    virtual void f(int) {
        std::cout << "B" << std::endl;
    };
};

struct D : public B {
    void f(int) override { std::cout << "D" << std::endl; }
};

struct E : public D {
    void f(int) {}
};

struct C {
    int c;
};

struct A : public B {
    int a;
};

int main() {
    // A a;
    // const B* b = &a;
    // std::cout << b << std::endl;
    // std::cout << &a << ' ' << &a.b << ' ' << &a.a << std::endl;
    // const C* c = &a;
    // std::cout << c << std::endl;

    B b;
    std::cout << sizeof(B) << std::endl;
    std::cout << &b << std::endl;
    std::cout << &b.b << std::endl;
    D d;
    std::cout << sizeof(D) << std::endl;
    B* b2 = &d;
    b2->f(5);
}
