#include <iostream>
#include <memory>
#include <string>

class Base2 {
    std::string Name() { return "aa"; }
};

class Base {
public:
    virtual std::string Name() const = 0;
    virtual Base* Clone() = 0;

    virtual ~Base() = default;
};

class Derived : public Base {
public:
    std::string Name() const override {
        return "Derived";
    }

    Derived* Clone() override {
        return new Derived(*this);
    }
};

class Derived2 : public Base {
public:
    std::string Name() const override {
        return "Derived2";
    }

    Derived2* Clone() override {
        return new Derived2(*this);
    }
};

int main() {
    std::unique_ptr<Base> ptr = std::make_unique<Derived>();
    std::unique_ptr<Base> ptr2 = std::make_unique<Derived2>();

    std::unique_ptr<Base> copy(ptr->Clone());
    std::unique_ptr<Base> copy2(ptr2->Clone());

    std::cout << copy->Name() << std::endl;
    std::cout << copy2->Name() << std::endl;

    Derived2* d = dynamic_cast<Derived2*>(copy2.get());
    std::cout << std::boolalpha << (d == nullptr) << std::endl;
    // std::cout << d->Name() << std::endl;

    std::cout << sizeof(Base) << std::endl;
    std::cout << sizeof(Base2) << std::endl;
}
