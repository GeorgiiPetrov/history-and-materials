#include <iostream>
#include <memory>
#include <vector>
#include <string>
using namespace std;

class Animal {
public:
    Animal(string name)
        : Name_(std::move(name))
    {
        cout << "Animal" << endl;
    }

    string GetName() const {
        return Name_;
    }

    ~Animal() {
        cout << "~Animal" << endl;
    }

protected:
    string Name_;
};

class Cat : public Animal {
public:
    Cat(string name)
        : Animal(std::move(name))
    {
        cout << "Cat" << endl;
    }
};

class Dog : public Animal {
public:
    Dog(string name)
        : Animal(std::move(name))
        // , Name_("Dog name")
    {
        cout << "Dog" << endl;
    }

    string GetName() const {
        return Name_;
    }

    ~Dog() {
        cout << "~Dog" << endl;
    }
};

class Dog2 {
public:
    string GetName() const {
        return animal_.GetName();
    }

private:
    Animal animal_;
};

int main() {
    Animal animal("name");
    Cat cat("cat");
    Dog dog("dog1");
    // cout << "Public name: " << dog.Name_ << endl;
    // Dog dog = "adsf";  // Dog dog("adsf");
    animal = dog;
    // animal.Name_;
    // cout << sizeof(Dog) << ' ' << sizeof(Animal) << endl;
    cout << dog.GetName() << endl;  // Dog name
    cout << animal.GetName() << endl;  // dog1
    return 0;
}
