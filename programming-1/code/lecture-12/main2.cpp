#include <iostream>
#include <string>
#include <memory>
#include <vector>
using namespace std;

class OutputStream {
public:
    OutputStream() {
        std::cout << "OutputStream" << std::endl;
    }

    virtual void Write(string_view) const {
    }

    virtual ~OutputStream() {
        std::cout << "~OutputStream" << std::endl;
    }
};

class FileStream : public OutputStream {
public:
    void Write(string_view data) const {
        cout << "FileStream: " << data << endl;
    }

private:
    // FileDescriptor
};

class StringStream : public OutputStream {
public:
    StringStream() {
        std::cout << "StringStream" << std::endl;
    }

    virtual void Write(string_view data) const {
        cout << "StringStream: " << data << endl;
        // data_ += std::string(data);
    }

    ~StringStream() {
        std::cout << "~StringStream" << std::endl;
    }

private:
    std::string data_;
};

void LogData(const OutputStream& out, string_view data) {
    out.Write(data);
}

class BufferedStringStream : public StringStream {

};

int main() {
    // OutputStream output_stream;
    OutputStream* ptr = new BufferedStringStream();
    delete ptr;
    // std::unique_ptr<OutputStream> ptr = std::make_unique<StringStream>();
    // FileStream f;
    // StringStream s;
    // std::vector<const OutputStream*> streams{&f, &s};
    // streams.push_back(f);
    // streams.push_back(s);
    // for (const OutputStream* stream : streams) {
        // LogData(*stream, "a");
    // }
    return 0;
}
