#include <map>
#include <string>
#include <iostream>

using namespace std;

template< typename T, typename U>
struct TMap {
    map<T, map<T, U>> storage;

    class iterator {
        using OutIterator = typename map<T, map<T, U>>::iterator;
        using InIterator = typename map<T, U>::iterator;
    public:   
        iterator(OutIterator outIt_, InIterator inIt_, OutIterator endIt_)
        {
            inIt = inIt_;
            outIt = outIt_;
            outEnd = endIt_;
        }

        iterator operator ++() {
            inIt++;
            if (inIt == (*outIt).second.end()) { 
                outIt++;
                if (outIt == outEnd) {
                    InIterator tmp;
                    inIt = tmp;
                    return *this;
                }
                inIt = (*outIt).second.begin();
            }
            return *this;
        }

        iterator operator --() {
            if (inIt == (*outIt).second.begin()) {
                --outIt;
                inIt = (*outIt).second.end();
            }
            --inIt;
            return *this;
        }

        bool operator ==(iterator& other) {
            if (outIt == other.outIt && inIt == other.inIt) {
                return true;
            } else {
                return false;
            }
        }

        bool operator != (iterator& other) {
            return (outIt != other.outIt || inIt != other.inIt);
        }

        pair<T, pair<T, U>> operator *() { 
            return {outIt->first, {inIt->first, inIt->second}};
        }

        //operator ++
        //operator --
        //operator ==
        //operator !=
        //operator *

    private:
        InIterator inIt;
        OutIterator outIt;
        OutIterator outEnd;
    
    };
    iterator begin() {
        return iterator(storage.begin(), storage.begin()->second.begin(), storage.end());
    }
    iterator end() {
        typename map<T, U>::iterator it;
        return iterator(storage.end(), it, storage.end());
    }
};

int main() {
    TMap<string, int> mm;
    mm.storage["key1"]["k2"] = 3;
    mm.storage["key1"]["k3"] = 4;
    mm.storage["key2"]["k4"] = 1;
    for (auto x : mm) {
        cout << x.first << " " << x.second.first << " " << x.second.second << endl;
    }

}
