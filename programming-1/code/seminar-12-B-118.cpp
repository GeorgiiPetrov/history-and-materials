#include <iostream>
#include <stdio.h>
#include <unordered_set>
#include <vector>

using namespace std;

class TUser {
private:
    unordered_set<TUser*> folowers_;
    unordered_set<TUser*> folowees_;
public:
    const unordered_set<TUser*>& GetFolowers() const {
        return folowers_;
    }
    const unordered_set<TUser*>& GetFolowees() const {
        return folowees_;
    }
};

template<class T>
class IGhaphNode {
    public:
    virtual const unordered_set<T*>& getNeighbours() const = 0;
};

template<class T>
class MyNode : public IGhaphNode<T> {
    unordered_set<T*> neigh;
public:
    MyNode() {
        T* tmp = new T();
        neigh.insert(tmp);
    }
    const unordered_set<T*>& getNeighbours() const {
        cout << "Call it!\n";
        return neigh;  
    }
};

template<class T>
class MyNode1 : public MyNode<T> {
    unordered_set<T*> neigh;
public:
    MyNode1() {
        T* tmp = new T();
        neigh.insert(tmp);
    }
    void add(const T& user) {
        neigh.insert(&T);
    }
    const unordered_set<T*>& getNeighbours() const {
        cout << "Other!\n";
        return neigh;  
    }
};


int main() {
    vector<IGhaphNode<int>*> cash;
    
        MyNode1<int> nd1;
        auto it11 = nd1.getNeighbours();
        IGhaphNode<int>& tmp1 = nd1;
        auto it12 = tmp1.getNeighbours();
        cash.push_back(&tmp1);
    
     
        MyNode<int> nd;
        auto it = nd.getNeighbours();
        IGhaphNode<int>& tmp = nd;
        auto it1 = tmp.getNeighbours();
        cash.push_back(&tmp);
    
    for (auto& el : cash) {
        cout << "IN VECTOR" << endl;
        el->getNeighbours();
    }
}
