#include <string>
#include <iostream>
#include <vector>
#include <cassert>

int GlobalId = 0;

int GenerateId()
{
    GlobalId += 1;
    return GlobalId;
}


struct TPerson
{
    TPerson() = delete;

    TPerson(
        const std::string& firstName,
        const std::string& secondName)
        : TPerson(firstName, secondName, 45)
    {
        std::cout << "Constructing person twice" << std::endl;
    }

    TPerson(
        const std::string& firstName,
        const std::string& secondName,
        int age)
        : FirstName(firstName)
        , SecondName(secondName)
        , FullName(FirstName + " " + SecondName)
        , Age(age)
        , PassportId(GenerateId())
    {
        std::cout << "Constructing person" << std::endl;
    }

    ~TPerson()
    {
        /*
        std::cout << "Destructing person "
            << FullName << std::endl;
        */
    }

    TPerson(const TPerson& other)
        : FirstName(other.FirstName)
        , SecondName(other.SecondName)
        , FullName(other.FullName)
        , Age(other.Age)
        , PassportId(GenerateId())
    {
        std::cout << "Copying person" << std::endl;
    }

    TPerson& operator=(const TPerson& other)
    {
        std::cout << "Before: " << std::endl;
        std::cout << "FullName = " << FullName << std::endl;

        FirstName = other.FirstName;
        SecondName = other.SecondName;
        FullName = other.FullName;
        Age = other.Age;
        PassportId = GenerateId();

        return *this;
    }

    void SayHello()
    {
        std::cout << "Hello, I'm "
            << FirstName << " "
            << SecondName << std::endl;
        std::cout << "Age = " << Age << std::endl;
        std::cout << "FullName = " << FullName << std::endl;
        std::cout << "PassportId = " << PassportId << std::endl;
    }

    std::string FirstName;
    std::string SecondName;
    std::string FullName;
    int Age;
    int PassportId;
};

/*

p = new int(42)
delete p

p = new int[42]
delete[] p

*/

struct TVector
{
    TVector()
        : Data(new int[42])
        , Size(42)
    { }

    void IncreaseSize(int size)
    {
        Size += size;
        delete[] Data;
        Data = new int[Size];
    }

    int operator[](int index) const
    {
        return Data[index];
    }

    int& operator[](int index)
    {
        return Data[index];
    }

    ~TVector()
    {
        delete[] Data;
    }

    int* Data;
    int Size;
};

struct TPersonsDatabase
{
    void Add(TPerson* p)
    {
        Persons.push_back(p);
    }

    TPerson* operator[](int passportId)
    {
        for (int index = 0; index < Persons.size(); ++index) {
            if (Persons[index]->PassportId == passportId) {
                return Persons[index];
            }
        }
        return nullptr;
    }

    std::vector<TPerson*> Persons;
};

struct TBigInteger
{
    TBigInteger()
    {
        Digits.push_back(0);
    }

    TBigInteger(int num)
    {
        assert(num >= 0);
        while (num > 0) {
            int digit = num % 10;
            Digits.push_back(digit);
            num /= 10;
        }
        if (Digits.empty()) {
            Digits.push_back(0);
        }
    }

    void Print()
    {
        for (int i = Digits.size() - 1; i >= 0; --i) {
            std::cout << Digits[i];
        }
        std::cout << std::endl;
    }

    std::vector<int> Digits;
    // 3 0 1 <=> 10 3
    // 1 2 3 4 5 6 7 0 8 <=> 807654321

    // 6 1 2
    // 5 1
    // 1 3 2
};

TBigInteger operator+(const TBigInteger& left, const TBigInteger& right)
{
    TBigInteger result;
    result.Digits.clear();

    int remainder = 0;
    for (int i = 0; i < std::max(left.Digits.size(), right.Digits.size()); ++i) {
        if (i < left.Digits.size()) {
            remainder += left.Digits[i];
        }
        if (i < right.Digits.size()) {
            remainder += right.Digits[i];
        }
        result.Digits.push_back(remainder % 10);
        remainder /= 10;
    }
    if (remainder > 0) {
        result.Digits.push_back(remainder);
    }

    return result;
}

/*
TODO:
operator<<(ostream&)
operator*
operator*=
operator+=
*/

int main()
{
    TPerson p("Dima", "Latyshev");
    p.SayHello();

    TPerson p2("Vlad", "Bidzilya", 27);
    p2.SayHello();

    std::cout << std::endl;

    TVector v;
    v.Data[0] = 10;

    std::cout << v.Data[0] << std::endl;

    v.IncreaseSize(10);
    v.IncreaseSize(15);

    std::cout << v.Size << std::endl;

    TPerson p3(p2);
    p3.SayHello();

    std::cout << v[10] << std::endl;
    v[10] = 156;
    std::cout << v[10] << std::endl;

    TPersonsDatabase database;
    database.Add(&p3);

    database[3]->SayHello();

    TPerson p4 = p3;
    p4 = p;
    p4.SayHello();

    TBigInteger big(156);

    big.Print();

    TBigInteger big2 = TBigInteger(32) + big;

    big2.Print();

    for (int i = 0; i < 1000; ++i) {
        big = big + big;
    }
    big.Print();

    return 0;
}
