#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>
#include <set>

using namespace std;
using namespace std::chrono;

template <class T>
struct TGreater {
    bool operator ()(const T& lhs, const T& rhs) const {
        return lhs > rhs;
    }
};

template <class T, class U>
struct TLessBySecond {
    bool operator ()(const pair<T, U>& lhs, const pair<T, U>& rhs) const {
        return lhs.second < rhs.second ||
            (lhs.second == rhs.second && lhs.first < rhs.first);
    }
};

template <class T, class U>
ostream& operator <<(ostream& out, const pair<T, U>& it) {
    out << '(' << it.first << ' ' << it.second << ')';
    return out;
}

template <class T>
ostream& operator <<(ostream& out, const vector<T>& it) {
    for (T elem : it) {
        out << elem << ' ';
    }
    return out;
}

pair<bool, int> MyDiv(int lhs, int rhs) {
    if (rhs == 0) {
        return make_pair<bool, int>(true, 0);
    }
    return {false, lhs / rhs};
}

//std::optional<int> MyDiv2(int lhs, int rhs) {
//    if (rhs == 0) {
//        return nullopt;
//    }
//    return {false, lhs / rhs};
//}

struct TIsEven {
    bool operator ()(int it) {
        return it % 2 == 0;
    }
};

int main() {
    cout << "\n======PART 1=======\n" << endl;
    {
        vector<int> toSort = {9, 8, 4, 1, 0, 4, 5};
        cout << is_sorted(toSort.begin(), toSort.end()) << endl; // 0
        sort(toSort.begin(), toSort.end()); // std::less<int>
        cout << toSort << endl; // 0 1 4 4 5 8 9
        cout << is_sorted(toSort.begin(), toSort.end()) << endl; // 1

        sort(toSort.begin() + 3, toSort.end(), TGreater<int>());
        cout << toSort << endl; // 0 1 4 9 8 5 4
        cout << is_sorted(toSort.begin(), toSort.end()) << endl; // 0
        cout << is_sorted(toSort.begin() + 3, toSort.end(), std::greater<int>()) << endl; // 1
    }

    cout << "\n======PART 2=======\n" << endl;
    {
        vector<int> toSort = {0, 1, 4, 9, 5, 8, 4};
        next_permutation(toSort.begin(), toSort.end());
        cout << toSort << endl; // 0 1 5 4 4 8 9

        prev_permutation(toSort.begin(), toSort.end());
        cout << toSort << endl; // 0 1 4 9 8 5 4
        prev_permutation(toSort.begin(), toSort.end());
        cout << toSort << endl; // 0 1 4 9 8 4 5
    }

    cout << "\n======PART 3=======\n" << endl;
    {
        vector<int> perm = {1, 2, 3};
        do {
            cout << perm << endl;
        } while (next_permutation(perm.begin(), perm.end()));
    }

    cout << "\n======PART 3=======\n" << endl;
    {
        vector<int> toRemove = {9, 4, 4, 1, 0, 7, 5};
        auto last = remove(toRemove.begin(), toRemove.end(), 4);
        cout << toRemove << endl; // 9 1 0 7 5 , 7 5
        cout << (toRemove.end() - last) << endl;
        toRemove.erase(last, toRemove.end());
        cout << toRemove << endl; // 9 1 0 7 5
    }

    cout << "\n======PART 4=======\n" << endl;
    {
        vector<int> toRemove = {9, 4, 4, 1, 0, 7, 5};
        toRemove.erase(remove_if(toRemove.begin(), toRemove.end(), TIsEven()), toRemove.end());
        cout << toRemove << endl; // 9 1 7 5
    }

    cout << "\n======PART 5=======\n" << endl;
    {
        int lhs = 0, rhs = 1;
        cout << lhs << ' ' << rhs << endl;
        swap(lhs, rhs);
        cout << lhs << ' ' << rhs << endl;
    }

    cout << "\n======PART 6=======\n" << endl;
    {
        vector<int> toReverse = {9, 4, 4, 1, 0, 7, 5};
        cout << toReverse << endl;
        reverse(toReverse.begin(), toReverse.end());
        cout << toReverse << endl;
    }

    cout << "\n======PART 7=======\n" << endl;
    {
        vector<int> toSearch = {9, 4, 4, 1, 0, 7, 5};
        sort(toSearch.begin(), toSearch.end());
        cout << binary_search(toSearch.begin(), toSearch.end(), 7) << endl; // 1
        cout << binary_search(toSearch.begin(), toSearch.end(), 100500) << endl; // 0
        cout << binary_search(toSearch.begin(), toSearch.begin() + 4, 7) << endl; // 0

        cout << *lower_bound(toSearch.begin(), toSearch.end(), 5) << endl; // 5
        cout << *upper_bound(toSearch.begin(), toSearch.end(), 5) << endl; // 7
        // cout << *upper_bound(toSearch.begin(), toSearch.end(), 100500) << endl; // error, ub
        for (auto it = lower_bound(toSearch.begin(), toSearch.end(), 3); it < upper_bound(toSearch.begin(), toSearch.end(), 3); ++ it) {
            cout << *it << ' ';
        } // empty
        cout << endl;
        for (auto it = lower_bound(toSearch.begin(), toSearch.end(), 4); it < upper_bound(toSearch.begin(), toSearch.end(), 4); ++ it) {
            cout << *it << ' ';
        } // 4 4
        cout << endl;
        for (auto it = lower_bound(toSearch.begin(), toSearch.end(), 4);
                it < upper_bound(toSearch.begin(), toSearch.end(), 7); ++ it) {
            cout << *it << ' ';
        } // 4 4 5 7
        cout << endl;
    }

    cout << "\n======PART 8=======\n" << endl;
    {
        set<int> intMap;
        for (int i = 0; i < 10050; ++i) {
            intMap.insert(i);
        }
        {
            auto start = high_resolution_clock::now();
            for (int i = 0; i < 10050; ++i) {
                *lower_bound(intMap.begin(), intMap.end(), i);
            }
            cerr << "Total execution time : " << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << endl;
        }
        {
            auto start = high_resolution_clock::now();
            for (int i = 0; i < 10050; ++i) {
                *intMap.lower_bound(i);
            }
            cerr << "Total execution time : " << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << endl;
        }
    }

    cout << "\n======PART 9=======\n" << endl;
    {
        vector<int> toUnique = {9, 4, 4, 1, 0, 7, 5};
        sort(tounique.begin(), tounique.end());
        tounique.erase(unique(tounique.begin(), tounique.end()), tounique.end());
        cout << toUnique << endl; // 0 1 4 5 7 9
    }

    cout << "\n======PART 10=======\n" << endl;
    {
        vector<pair<int, string>> pairs = {
            {2, "second"},
            {1, "first"},
            {3, "third"},
        };
        cout << pairs << endl;
        sort(pairs.begin(), pairs.end());
        cout << pairs << endl;
    }

    cout << "\n======PART 11=======\n" << endl;
    {
        vector<pair<int, string>> pairs = {
            {1, "p"},
            {2, "o"},
            {3, "x"},
        };
        cout << pairs << endl;
        sort(pairs.begin(), pairs.end(), TLessBySecond<int, string>());
        cout << pairs << endl;
    }

    cout << "\n======PART 12=======\n" << endl;
    {
        auto res = MyDiv(10, 0);
        cout << '(' << res.first << ", " << res.second << ')' << endl;
        res = MyDiv(10, 1);
        cout << '(' << res.first << ", " << res.second << ')' << endl;
    }

    return 0;
}
