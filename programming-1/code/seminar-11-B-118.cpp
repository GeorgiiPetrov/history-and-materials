#include <iostream>
#include <algorithm>
#include <deque>
using namespace std;

template<class Tcomp>
void my_sort(vector<int>& arr, Tcomp comp) {
    auto size = arr.size();
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (comp(arr[j], arr[j + 1])) {
                // меняем элементы местами
                auto temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int g(int a, int b) {
    return a- b;
}

struct Foo {
    function<void()> ability;
    clock_t begin;
    Foo(function<void()> to_do)
        : ability(to_do), begin(clock()) { }
    ~Foo() {
        ability();
        cout << (clock() - begin) / CLOCKS_PER_SEC << "\n";
    }
};

int main() {
    { 
        int b = 2;
        clock_t time_1 = clock();
        int c = 3;
        clock_t timer = clock();
        auto tmp = 
            [timer]() {
                cout << clock()  - timer << " sdfsdfs" << endl;
            };
        tmp();
        deque<int> d;
        d.push_back(1);
        cout << "We start" << endl;
        for (long long i = 0; i < 1e6; ++i) {
           d.push_front(i);
           d.pop_back();
        }
        tmp();
    }
    return 0;
    function<int(int, int)> sum =
        [](int a, int b) {
        return a + b;
        };
    sum = g;
    cout << sum(3, 4) << endl;
    vector<int> v = {1, 12, 4, 5, 3, 2};
    int timer = 0;
    //написать функцию, которая принимает
    //вектор по ссылке и компаратор-лямбду, сортировка пузырьком
    //в лямбде захвачено по ссылке значение timer
    //посчитать сколько раз вызовется компаратор
    auto f = [&timer](int l, int r) {
            ++timer;
            return l < r;
        };
    my_sort(v, f);
    for (auto& x : v)
        cout << x << " ";
}

/*
#include <iostream>
#include <vector>

using namespace std;

 auto not_negative(decltype(1) x) {
    if (x < 0)
        return 0;
    return x;
}

template<class A, class B>
auto f(A a, B b) {
    return a < b ? a : b;
}

int main() {

    int a = 2;
    int& b = a;
    string s = "abc";
    //type of a + a
    //move(b)
    //type of s[-3]
    //
    static_assert(is_same<int&&, decltype(move(b))>::value);
    static_assert(is_same<char&, decltype(s[-3])>::value);
}
*/


