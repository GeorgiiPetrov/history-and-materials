#include <chrono>
#include <iostream>

using namespace std::chrono;
using namespace std;

int global = -1;

class TLog {
public:
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    TLog() {
        start = high_resolution_clock::now();
        cout << "Construct" << endl;
    }

    ~TLog() {
        cout << "Destruct.";
        cerr << "Total execution time : " << duration_cast<milliseconds>(high_resolution_clock::now() - start).count() << " ms" << endl;
    }
};


void foo2(int depth = 0) {
    const int SIZE = 1000;
    int a[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        a[0] = a[i] = i;
    }
    cout << depth++ << endl;
    foo2(depth);
    a[0] = a[SIZE - 1] = 1;
}

void foo() {
    TLog log = TLog();
    {
        int x = 0;
        const int SIZE = 100000000;
        int a[SIZE];
        for (int i = 0; i < SIZE; ++i) {
            a[0] = a[i] = i;
            x += i;
        }
        cout << x << endl;
    }
    static int staticInt = 10;
    string local = "local";
    cout << local << " " << staticInt++ << endl;
    string* ptr = &local;
    cout << *ptr << endl;
}

int* leak() {
    const int SIZE = 10000;
    int* x = new int[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        x[i] = i;
    }
    cout << x[0] << " " << x[SIZE] << endl;
    //delete x;
    return x;
}

//new (size_t size)
//new (char* ptr, size_t size)
void DoPlacementNew() {
    char* buf = new char[sizeof(string) * 2];

    string* a = new(buf) string("frist_string");
    string* b = new(buf + sizeof(string)) string("second_string_big_length");

    cout << *a << " " << *b << endl;

    for (size_t i = 0; i < sizeof(string) * 2; ++i) {
        cout << (int)(*(buf + i)) << " ";
    }
    cout << endl;
    for (char c : (*a)) {
        cout << (int)c << " ";
    }
    cout << endl;
    for (char c : (*b)) {
        cout << (int)c << " ";
    }
    cout << endl;
}

int main() {
    int local = -1;
    foo();
    foo();
    foo();
    foo();
    string* dynamic = new string("dynamic");
    cout << local << endl;
    cout << *dynamic << endl;

    int* deleted = leak();
    cout << *deleted << endl;
    delete deleted;

    DoPlacementNew();

    return 0;
}