#include <iostream>
#include <memory>
#include <string>
#include <unordered_set>
#include <utility>

template <typename TIterator>
class IGraphNode {
public:
    virtual std::pair<TIterator, TIterator> GetNeighbours() const = 0;
};

template <typename TIterator>
bool Dfs(
    const IGraphNode<TIterator>* node,
    const IGraphNode<TIterator>* target,
    std::unordered_set<const IGraphNode<TIterator>*>& visited
) {
    if (node == target) {
        return true;
    }
    visited.insert(node);
    std::pair<TIterator, TIterator> neighbours = node->GetNeighbours();
    for (TIterator it = neighbours.first; it != neighbours.second; ++it) {
        if (!visited.contains(*it) && Dfs(*it, target, visited)) {
            return true;
        }
    }
    return false;
}

template <typename TIterator>
bool AreConnected(const IGraphNode<TIterator>* node, const IGraphNode<TIterator>* target) {
    std::unordered_set<const IGraphNode<TIterator>*> visited;
    return Dfs(node, target, visited);
}

class TUser : public IGraphNode<std::unordered_set<const TUser*>::const_iterator> {
public:
    using TIterator = std::unordered_set<const TUser*>::const_iterator;

    explicit TUser(std::string name)
        : Name_(std::move(name))
    { }

    const std::string& GetName() const {
        return Name_;
    }

    void Follow(TUser* target) {
        if (Followings_.insert(target).second) {
            target->Followers_.insert(this);
        }
    }

    std::pair<TIterator, TIterator> GetNeighbours() const override {
        return std::make_pair(Followers_.cbegin(), Followers_.cend());
    }

private:
    std::string Name_;
    std::unordered_set<const TUser*> Followers_;
    std::unordered_set<const TUser*> Followings_;
};


int main() {
    TUser user1("1");
    TUser user2("2");

    user1.Follow(&user2);
    user2.Follow(&user1);
    std::cout << std::boolalpha << AreConnected(&user1, &user2) << std::endl;
    return 0;
}
