#include <iostream>

using namespace std;

class Foo {
public:
    int a;
    int b;

    virtual int foo() {
        return 10;
    }
};

class Bar : public Foo {
public:
    int c;
};

int func(const Foo* a) {
    Foo* b = const_cast<Foo*>(a);
    b->a = 321;
    return a->a * 2;
}

int main() {
    Foo foo;
    foo.a = 5;
    foo.b = 6;

    Bar bar;
    bar.a = 10;
    bar.b = 15;
    bar.c = 16;

    Foo* p1 = &foo;
    (*p1).a = 100;
    p1->a = 100;

    Foo* p2 = &bar;
    static_cast<Bar*>(p2)->c = 500;
    static_cast<Bar*>(p1)->c = 600;

    cout << "foo = " << foo.a << ", " << foo.b << endl;
    cout << "bar = " << bar.a << ", " << bar.b << ", " << bar.c << endl;

    Bar* p3 = dynamic_cast<Bar*>(p1);
    // if (p3 != 0) {
    //if (p3 != NULL) {
    if (p3 != nullptr){
        p3->c = 600;
        cout << "p3 is not null" << endl;
    }
    // ?? ????????
    // dynamic_cast<Bar*>(p1)->c = 600;
    cout << p3 << endl;

    int c = 10;
    cout << func(&foo) << endl;
    cout << "foo.a = " << foo.a << endl;

    Bar* p4 = reinterpret_cast<Bar*>(&foo);

    bool b1 = true;
    bool b2 = false;

    cout << "b1 and b2 = " << (b1 && b2) << endl;
    cout << "b1 or b2 = " << (b1 || b2) << endl;
    cout << "b1 xor b2 = " << (b1 ^ b2) << endl;
    cout << "not b1 = " << !b1 << endl;

    int i1 = 5; // 101
    int i2 = 3; // 011

    cout << "i1 and i2 = " << (i1 & i2) << endl;
    cout << "i1 or i2 = " << (i1 | i2) << endl;
    cout << "i1 xor i2 = " << (i1 ^ i2) << endl;
    cout << "not i1 = " << ~i1 << endl;

    // 1 -> 100
    cout << "3rd bit of i1 = " << bool(i1 & (1 << 2)) << endl;

    int i = 0;
    while (i < 10) {
        cout << "i = " << i << endl;
        i++;
    }

    do {
        cout << "i = " << i << endl;
        i++;
    } while (i < 10);

    for (
         size_t j = 1, k = 50;
         i < 15;
         j++, k -= 5, i += 2
    ) {
        cout << "i,j,k = " << i << ", " << j << ", " << k << endl;
    }

    int i3 = 0;
    if (i1 < 5) {
        i3 = i1 + i2;
    } else if (i2 > 100) {
        i3 = 677;
    } else {
        i3 = 100;
    }

    int i4 =
        i1 < 5
        ? i1 + i2
        : i2 > 100
            ? 677
            : 100;
    cout << "i4 = " << i4 << endl;

    return 0;
}
