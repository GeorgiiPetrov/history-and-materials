#include <iostream>
#include "lib.h"

using namespace std;

int main() {
    cout << "Hello world!" << endl;

    cout << "foo(5) = " << foo(5) << end;
    cout << "Foo::foo(5) = " << Foo::foo(5) << end;

    return 0;
}
