#include <iostream>
#include <memory>
#include <vector>
using namespace std;

class Speaker {
    string name;
    int id;
public:
    Speaker(const string& s) : name(s), id(0) {
        cout << get_name() << " Created!\n";
    }
    ~Speaker() {
        cout << get_name() << " Deleted!\n";
    }
    Speaker(const Speaker& other) : name(other.name), id(other.id + 1) {
        cout << get_name() << " Create by &\n";
    }
    Speaker& operator =(const Speaker& other) {
        name = other.name;
        id = other.id + 1;
        cout << get_name() << "= by &\n";
        return *this;
    }
    Speaker(Speaker&& other) : name(""), id(other.id + 1) {
        swap(name, other.name);
        cout << get_name() << " Create by &&\n";
    }
    Speaker& operator =(Speaker&& other) {
        name = "";
        swap(other.name, name);
        id = other.id + 1;
        cout << get_name() << "= by &&\n";
        return *this;
    }
    string get_name() {
        return name + to_string(id);
    }
};

class Dialog {
    Speaker A, B;
public:
    Dialog(Speaker&& a1, Speaker&& b1) : A(move(a1)), B(move(b1)) { 
        cout << A.get_name() << " with_name "
            << B.get_name() << " Dialog created\n";
    }
};

unique_ptr<Speaker> foo(unique_ptr<Speaker>&& p) {
    cout << p->get_name() << endl;
    return move(p);
}

int main() {
    Speaker a("a");
    Speaker b("b");
    Dialog d1(move(a), move(b));
    cout << a.get_name() << " " << b.get_name() << endl;
    //теперь a и b - пустышки. 
    //чтобы их не испортить,
    //нужно сделать 2ой конструктор диалога,
    //который принимает Speaker(создаст копию)
    
    //part2: about unique_ptr
    auto p = make_unique<Speaker>("b");
    cout << p->get_name() << '\n';
    auto q = foo(move(p));
    //cout << p->get_name() << '\n'; - Error
    p = move(q);
    cout << p->get_name() << '\n';

    //part3: about shared_ptr
    vector<shared_ptr<Speaker>> v;
    v.push_back(make_shared<Speaker>("a"));
    for (int i = 0; i < 10; ++i) {
        shared_ptr<Speaker> it = v[i];
        v.push_back(it);
    }
    while (v.size()) {
        v.pop_back();
        cout << v.size() << endl;
    }
}
