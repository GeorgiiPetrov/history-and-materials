#include <iostream>
#include <vector>

using namespace std;

template <typename F, typename S>
struct TPair {
    F First;
    S Second;
};

template <typename F, typename S>
class TZipIterator {
public:
    using TFirstIterator = typename vector<F>::const_iterator;
    using TSecondIterator = typename vector<S>::const_iterator;

public:
    TZipIterator(TFirstIterator first, TSecondIterator second)
        : FirstIterator_(first)
        , SecondIterator_(second)
    {
    }

    TPair<F, S> operator*() const {
        return {*FirstIterator_, *SecondIterator_};
    }

    TZipIterator<F, S>& operator++() {
        ++FirstIterator_;
        ++SecondIterator_;
        return *this;
    }

    TZipIterator<F, S> operator++(int) {
        TZipIterator<F, S> copy(*this);
        operator++();
        return copy;
    }

    bool operator==(const TZipIterator<F, S>& rhs) const {
        return FirstIterator_ == rhs.FirstIterator_ &&
            SecondIterator_ == rhs.SecondIterator_;
    }

    bool operator!=(const TZipIterator<F, S>& rhs) const {
        return !(*this == rhs);
    }

private:
    TFirstIterator FirstIterator_;
    TSecondIterator SecondIterator_;
};

template <typename F, typename S>
class TZipper {
public:
    TZipper(const std::vector<F>& first, const std::vector<S>& second)
        : First_(first)
        , Second_(second)
    { }

    TZipIterator<F, S> begin() const {
        return TZipIterator<F, S>(First_.begin(), Second_.begin());
    }

    TZipIterator<F, S> end() const {
        return TZipIterator<F, S>(First_.end(), Second_.end());
    }

private:
    const std::vector<F>& First_;
    const std::vector<S>& Second_;
};

int main() {
    vector<int> a = {1, 2, 3};
    vector<string> b = {"abc", "def", "zxc"};
    TZipper<int, string> z(a, b);

    for (TPair<int, string> it : z) {
        cout << it.First << " " << it.Second << endl;
    }
    //1 abc
    //2 def
    //3 zxc
}