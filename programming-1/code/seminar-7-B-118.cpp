#include <bits/stdc++.h>

using namespace std;

namespace about_stl {

namespace about_set {

void task1() {
    vector<int> v = {1, 3, 2, 4, 2};
    set<int> s;
    for (auto& x : v)
        s.insert(x);
    v.clear();
    for (set<int>::iterator it = s.begin(); it != s.end(); ++it){
        v.push_back(*it);        
    }
    for (auto& x : s)
        v.push_back(x);
    for (auto& x : v)
        cout << x << " ";
    cout << endl;
}

void task2() {
    vector<int> v = {1, 100, 3, 4, 50};
    set<int> s;
    for (auto& x : v)
        s.insert(x);
    //min
    {
        auto it = s.begin();
        cout << *it << " - min\n";
    }
    //second min
    { 
        auto it = s.begin();
        ++it;
        cout << *it << " - sec min\n";
    }
    //max
    { 
        set<int>::reverse_iterator it = s.rbegin();
        cout << *it << " - max\n";
    }
    //second max
    {
        auto it = s.rbegin();
        ++it;
        cout << *it << " - second max\n";
    }
}

void task3() {
    vector<int> v = {1, 100, 3, 4, 50};
    set<int> s;
    for (auto& x : v)
        s.insert(x);
    if (s.find(5) == s.end())
        cout << "TRUE IF 4 is not in set\n";
    if (s.find(3) != s.end())
        cout << "TRUE IF 3 is in set\n";
}

void task4() {
    vector<int> v = {1, 100, 3, 4, 50};
    set<int> s;
    for (auto& x : v)
        s.insert(x);
    int X = 50;
    auto it = s.upper_bound(X);
    if (it == s.begin()) {
        cout << "No element <= x\n";
    } else {
        --it;
        cout << *it << "<= x\n";
    }
}

}//namespace about_set

namespace about_multiset {

void task1() {
    multiset<int> s;
    vector<int> v = {1, 100, 2, 2, 50, 50};
    for (auto& x : v)
        s.insert(x);
    //delete all elems=2
    s.erase(2);
    for (auto& x : s)
        cout << x << " ";
    cout << endl;
    //delete one of elem=50
    s.erase(s.find(50));
    for (auto& x : s)
        cout << x << " ";
    cout << endl;
}

void task2() {
    multiset<int> trash;
    vector<int> v = {3, 4, 6, 9, 3};
    for (auto x : v)
        trash.insert(x);
    auto it = trash.begin();
    while (true) {
        if (it == trash.end()) {
            cout << "P2 WIN\n";
            break;
        }
        auto P2 = trash.lower_bound((*it) * 2);
        if (P2 == trash.end()) {
            cout << "P1 WIN\n";
            break;
        }
        trash.erase(P2);
        ++it;
    }
}

}//namespace about_multiset

namespace about_map {

template<class T, class U>
void operator +=(pair<T, U> &a, const pair<T, U> &b) {
    a.first += b.first;
    a.second += b.second;
}

template<class T, class U>
ostream& operator <<(ostream& out, pair<T, U> p) {
    out << "{" << p.first << ", " << p.second << "}";
    return out;
}

pair<int, int> operator + (pair<int, int> l, pair<int, int> r) {
    return {l.first + r.first, l.second + r.second};
}

void task1() {
    //pair<int, pair<int, pair<string, char>>> p = {2, {3, {"abc", 'd'}}};
    //p += p;
    //cout << p << endl;
    string s = "ullulrd";
    pair<int, int> now = {0, 0};
    map<char, pair<int, int>> step; //unordered_map better in this case
    step['u'] = {0, 1};
    step['d'] = {0, -1};
    step['r'] = {1, 0};
    step['l'] = {-1, 0};
    for (auto x : s)
        now = now + step[x];
    cout << now << '\n';
}

}//namespace about_map

namespace about_unordered_set {

void task1() {
    //Возникают ситуации, когда числа необходимо "сжать"
    //сохранив инвариант, что если a < b, то после сжатия неравенство останется верным
    //Простыми словами - заменить число на его номер в отсортированном без повторов массиве
    //
    //Используется деревьях отрезков(и не только). Позволяет перейти от неявной структуры к явной
    unordered_map<int, int> compress;
    vector<int> to_compress = {10, 100, 321, 1010};
    set<int> s;
    for (auto& x : to_compress) {
        s.insert(x);
    }
    int timer = 0;
    for (auto& el : s) {
        compress[el] = timer++;
    }
    //compress[x] - "сжатое число"
}

}//namespace about_unordered_set

} //namespace about_stl

namespace about_strings {

void task1() {
    string s;
    cin >> s;
    s[1] = '2';
    vector<int> cnt(26);
    for (auto& x : s)
        ++cnt[x - 'a'];
    for (int i = 0; i < cnt.size(); ++i)
        cout << char('a' + i) << ' ' << cnt[i] << endl;
}

void task2() {
    string s;
    cin >> s;
    string_view kek = s;
    cout << kek << endl;
    int ans = 0;
    int k = 1;
    int start = 0;
    if (s[0] == '-') {
        k = -1;
        start = 1;
    }
    for (int i = start; i < s.size(); ++i)
        ans = ans * 10 + (s[i] - '0');
    ans *= k;
    cout << ans << '\n';
}

///////task3

struct User {
    string name;
    string surname;
    int id;
    User() : name("noname"), surname(""), id(-1) { }
};

istream& operator >> (istream& in, User& user) {
    return in >> user.name >> user.surname >> user.id;
}

ostream& operator << (ostream& out, User& user) {
    return out << "User(name = " << user.name << ", surname = " << user.surname << ", id = " << user.id << ")";
}

void task4() {
    User A, B;
    //ifstream f("out.txt");
    auto& f = cin;
    f >> A >> B;
    cout << A << endl << B << endl;
}

}//namespace about_strings

signed main() {
    string s;
    cin >> s;
    s[1] = '2';
    cout << s << endl;
    //about_strings::task4();
    return 0;
}
