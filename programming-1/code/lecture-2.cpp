#include <iostream>

using namespace std;

int* foo(int a) {
    // так делать не надо
    int b = a * 2;
    return &b;
}

int bar(
    int a,
    const int b,
    int* c,
    const int* d,
    int* const e,
    const int* const f
) {
    a = 1;
    // не можем присваивать значение в константу
    // b = 1;
    *c = 1;
    // указатель на константный int, не можем присваивать
    // *d = 1;
    // Но можем присвоить другой адрес, так как сам указатель не константный
    d = c;

    // тут константа относится к указателю, а не к данным, на которые он указывает
    *e = 1;
    // сам указатель менять не можем
    // e = c;

    // константен и указатель и данные
    // *f = 1;
    // f = c;

    int* ff = const_cast<int*>(f);
    *ff = 1;
    ff = c;

    return 0;
}

void baz(int& a, const int& b) {
    a = 555;
    // b = 100000;
}

int main() {
    int a = 50;
    int* pa = &a;

    cout << "a = " << a << "; pa = " << pa << "; *pa = " << *pa << endl;

    *pa = 150;
    cout << "a = " << a << "; pa = " << pa << "; *pa = " << *pa << endl;

    int* pb = new int();

    cout << "pb = " << pb << "; *pb = " << *pb << endl;

    *pb = 654;
    cout << "pb = " << pb << "; *pb = " << *pb << endl;

    delete pb;
    cout << "pb = " << pb << "; *pb = " << *pb << endl;

    // так лучше не делать
    *pb = 1000;
    cout << "pb = " << pb << "; *pb = " << *pb << endl;

    int arr[10] = {1, 5, 67, 654, 90};
    arr[0] = 100500;
    arr[1] = 456;

    cout << "arr[0] = " << arr[0] << "; arr[4] = " << arr[4] << "; arr[8] = " << arr[8] << endl;
    cout << "arr = " << arr << "; &arr[0] = " << &arr[0] << endl;

    // for (int i = 0; i < sizeof(arr); ++i) {
    //     cout << "arr[" << i << "] = " << arr[i] << endl;
    // }

    // for (void* pvi = arr; pvi < &arr[10]; pvi += sizeof(arr[0])) {
    //     cout << "pvi = " << pvi << endl;
    // }

    // for (int* pi = arr; pi - arr < sizeof(arr); ++pi) {
    //     cout << "pi = " << pi << "; *pi = " << *pi << endl;
    // }

    // cout << 0x80 << " " << 0b1101 << " " << 0456 << endl;

    // for (int i = 0; i < 10; ++i) {
    //     cout << "arr[" << i << "] = " << *(&arr[0] + i) << endl;
    // }

    // так делать не надо
    // int* pf = foo(10);
    // cout << "pf = " << pf << "; *pf << " << *pf << endl;

    cout << arr[0] << " " << arr[1] << " " << arr[2] << " " << arr[3] << " " << arr[4] << " " << arr[5] << endl;
    bar(arr[0], arr[1], &arr[2], &arr[3], &arr[4], &arr[5]);
    cout << arr[0] << " " << arr[1] << " " << arr[2] << " " << arr[3] << " " << arr[4] << " " << arr[5] << endl;

    int& ra = arr[0];
    cout << "arr[0] = " << arr[0] << endl;
    ra = 550;
    cout << "arr[0] = " << arr[0] << endl;

    cout << "arr[0] = " << arr[0] << endl;
    baz(arr[0], 9);
    cout << "arr[0] = " << arr[0] << endl;

    return 0;
}