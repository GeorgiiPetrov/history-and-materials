#include <iostream>
#include <map>
#include <fstream>

using namespace std;

class TDoubleMap {
    using TInnerMap = map<int, int>;
    using TOuterMap = map<int, TInnerMap>;

    using TOuterIterator = TOuterMap::iterator;
    using TInnerIterator = TInnerMap::reverse_iterator;
public:
    TOuterMap Mp;

    class TIterator {
    public:
        TOuterIterator OuterIter;
        TOuterIterator OuterEnd;
        TInnerIterator InnerIter;
        //innerEnd == outerIter->second.end()

        TIterator(TOuterIterator outerIter, TOuterIterator outerEnd, TInnerIterator innerIter)
            : OuterIter(outerIter)
            , OuterEnd(outerEnd)
            , InnerIter(innerIter)
        {
            //OuterIter = outerIter;
        }

        int operator *() {
            return InnerIter->second;
            //return make_pair<int, pair<int, int>>(OuterIter->first, make_pair<int, int>(InnerIter->first, InnerIter->second));
        }

        bool operator !=(const TIterator& it) const {
            return OuterIter != it.OuterIter || InnerIter != it.InnerIter;
        }

        //myClass.Method1()
        //       .Method2();
        //VS
        //myClass.Method1();
        //myClass.Method2();
        TIterator& operator ++() {
            ++InnerIter;
            if (InnerIter != OuterIter->second.rend()) {
                return *this;
            }

            ++OuterIter;
            while (OuterIter != OuterEnd && OuterIter->second.empty()) {
                ++OuterIter;
            }

            if (OuterIter == OuterEnd) {
                InnerIter = TInnerIterator();
                return *this;
            }

            InnerIter = OuterIter->second.rbegin();
            return *this;
        }
    };

    void Insert(int outerKey, int innerKey, int value) {
        Mp[outerKey][innerKey] = value;
    }

    void Insert(int outerKey) {
        if (Mp.find(outerKey) == Mp.end()) {
            Mp[outerKey] = map<int, int>();
        }
    }

    TIterator begin() {
        TOuterIterator outerBegin = Mp.begin();

        // типы int -> string
        // смысл key -> value
        // обращение it.first -> it.second
        //TInnerMap& innerMap = outerBegin->second;
        while (outerBegin != Mp.end() && outerBegin->second.empty()) {
            ++outerBegin;
        }

        if (outerBegin == Mp.end()) {
            return end();
        }

        return TIterator(outerBegin/* outerIter */, Mp.end()/* outerEnd */, outerBegin->second.rbegin() /* innerIter */);
    }

    TIterator end() {
        return TIterator(Mp.end()/* outerIter */, Mp.end()/* outerEnd */, TInnerIterator() /* innerIter */);
    }
};

ostream& operator <<(ostream& out, TDoubleMap& mp) {
    for (int it : mp) {
        out << it << ' ';
        //1 2 3 6 4 5
        //
        //6 3 2 1 5 4
    }
    return out;
}

int main() {
    TDoubleMap dm; // map<int, map<int, int>>
    dm.Insert(0);
    dm.Insert(1, 1, 1);
    dm.Insert(1, 2, 2);
    dm.Insert(1, 3, 3);
    dm.Insert(2);
    dm.Insert(3, 1, 4);
    dm.Insert(3, 2, 5);
    dm.Insert(1, 4, 6);

    //

    // 0 -> {}
    // 1 -> {}
    // 2 -> {}

    // 0 -> {}
    // 1 -> {1->1, 2->2, 3->3, 4->6}
    // 2 -> {}
    // 3 -> {1->4, 2->5}

    //for (TDoubleMap::TIterator it = dm.begin(); it != dm.end(); ++it) {
    //    cout << *it << ' ';
    //}

    ofstream out("asdqw");
    out << dm << endl;
    cout << dm << endl;

    return 0;
}
