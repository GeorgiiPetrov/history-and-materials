#include <iostream>
#include <vector>
using namespace std;

template<class T>
bool predicate(T x, int ost) {
    return x % 2 == ost;
}

template<class T>
typename vector<T>::iterator remove_if1(
        typename vector<T>::iterator begin,
        typename vector<T>::iterator end,
        bool (*pr)(T, int)) {
    auto help = begin;
    for (; begin != end; ++begin) {
        if (pr(*begin, 1)) {
            *help = *begin;
            help++;
        }
    }
    return help;
}

template<class T>
void DoSomething(vector<T> &v,
        typename vector<T>::iterator(*f)(typename vector<T>::iterator, typename vector<T>::iterator, bool (*n)(T, int))) {
        auto it_end = f(v.begin(), v.end(), predicate);
    for (auto it = v.begin(); it != it_end; ++it)
        cout << *it << " ";
    cout << endl;
}
int main() {
    vector<int> v = {1, 2, 3, 4, 3, 2};
    DoSomething(v, remove_if1);
    /*
    auto it_end = remove_if1<int>(v.begin(), v.end(),predicate);
    for (auto it = v.begin(); it != it_end; ++it)
        cout << *it << " ";
    cout << endl;
    //{1, 2, 3, 4, 3, 2}
    //remove(2)
    //{1, 3, 4, 3}*/
    return 0;
}
