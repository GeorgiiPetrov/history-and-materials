#include <iostream>
#include "lib.h"

using namespace std;

struct PersonalCard {
    const char* name;
    const char* last_name;
    int age;
    const int height;

    PersonalCard();
    PersonalCard(const char* name, const char* last_name, int age, int height = 180);

    void print_card() const;
    void print_card();

    const char* get_name() {
        return this->name;
    }

    void increment_age() {
        // неявно подразумевается,
        // что this имеет тип PersonalCard*
        // поэтому const PersonalCard* не подходит
        ++age;  // то же самое, что this->age
    }

};

void PersonalCard::print_card() const {
    cout << "Constant person object. "
         << "Name: " << this->name
         << ", last name: " << this->last_name
         << ", age: " << age
         << endl;
    // age = 15;
    // так нельзя, потому что this имеет тип const PersonalCard*
}

void PersonalCard::print_card() {
    cout << "Modifiable person object. "
         << "Name: " << this->name
         << ", last name: " << this->last_name
         << ", age: " << age
         << endl;
    // age = 15;
}

PersonalCard::PersonalCard() : name("unknown"), last_name("unknown"), age(-1), height(180) {
}

PersonalCard::PersonalCard(const char* name, const char* last_name, int age, int height) : height(height) {
    this->name = name;
    this->last_name = last_name;
    (*this).age = age;
}

// PersonalCard::PersonalCard(const char* name, const char* last_name, int age, int height) {
//     this->name = name;
//     this->last_name = last_name;
//     (*this).age = age;
//     не будет работать, так как this->height это константа
//     this->height = height;
// }

const char* get_name(const PersonalCard* ppc) {
    return ppc->name;  // то же самое, что (*ppc).name
}

void print_card(PersonalCard& pc) {
    cout << "Modifiable person object. "
         << "Name: " << pc.name
         << ", last name: " << pc.last_name
         << ", age: " << pc.age
         << endl;
}

int main() {
    PersonalCard person1("Ivan", "Ivanov", 25);
    const PersonalCard person2;
    PersonalCard person3("Petr", "Popov", 30, 178);

    person1.print_card();
    person2.print_card();
    person3.print_card();

    // нельзя так вызывать из-за const
    // person2.increment_age();
    // print_card(person2);
    person2.print_card();

    cout << "name of person 1 = "
         << person1.get_name() << endl;

    cout << "sizeof(const char*) = " << sizeof(const char*) << endl;
    cout << "sizeof(PersonalCard) = " << sizeof(PersonalCard) << endl;

    cout << "&person1.name = " << &person1.name << endl;
    cout << "&person1.last_name = " << &person1.last_name << endl;

    TestRectangle();

    return 0;
}
