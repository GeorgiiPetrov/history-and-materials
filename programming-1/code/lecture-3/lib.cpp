#include <iostream>
#include "lib.h"

using namespace std;

Rectangle::Rectangle() : width(0), height(0) {}

Rectangle::Rectangle(float width, float height) : width(width), height(height) {}

float Rectangle::GetArea() const {
    return width * height;
}

float Rectangle::GetWidth() const {
    return width;
}

float Rectangle::GetHeight() const {
    return height;
}

void TestRectangle() {
    Rectangle empty_rect;
    Rectangle rect1(5, 10);

    // cout << rect1.width << endl;
    cout << rect1.GetWidth() << endl;

    Rectangle rect2 = rect1;
    rect2 = empty_rect;
}
