class Rectangle {
    float width, height;

public:
    Rectangle();
    Rectangle(float width, float height);

    float GetArea() const;

    float GetWidth() const;
    float GetHeight() const;
};


void TestRectangle();
