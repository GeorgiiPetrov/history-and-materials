#include <cassert>
#include <chrono>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <thread>
#include <variant>
#include <vector>

struct TCallback
{
    int& a;
    std::vector<int> b;
    std::string e;

    int operator()(int x) const
    {
        return 42;
    }
};

class TSignal
{
public:
    // Type erasure.
    void Subscribe(std::function<void()> callback)
    {
        Callbacks_.push_back(std::move(callback));
    }

    void Fire() const
    {
        for (const auto& callback : Callbacks_) {
            callback();
        }
    }

private:
    std::vector<std::function<void()>> Callbacks_;
};

void ShowBankruptUser()
{
    std::cout << "User bankrupt" << std::endl;
}

template <class TCallback>
auto WrapIntoLogging(TCallback callback)
{
    return [callback = std::move(callback)] {
        std::cout << "Callback called" << std::endl;
        return callback();
    };
}

template <class TCallback>
auto WrapIntoTiming(TCallback callback)
{
    return [callback = std::move(callback)] {
        auto start = std::chrono::high_resolution_clock::now();
        auto result = callback();
        auto finish = std::chrono::high_resolution_clock::now();
        std::cout << "Elapsed time is " << std::chrono::duration_cast<std::chrono::milliseconds>(finish - start).count() << " ms" << std::endl;
        return result;
    };
}

template <class TCallback>
auto WrapIntoMapping(TCallback callback)
{
    return [callback = std::move(callback)] (auto& container) {
        for (auto& element : container) {
            callback(element);
        }
    };
}

template <class TCallback, class TFirstArgument>
auto BindFirst(TCallback callback, TFirstArgument argument)
{
    return [callback = std::move(callback), argument = std::move(argument)] (auto& secondArgument) {
        return callback(argument, secondArgument);
    };
}

int main()
{
    // Part 1. Lambda as a functor.
    int a;
    std::vector<int> b;
    std::string c;

    auto callback = [&a, b, e = std::move(c)] (int x) {
        (void)a;
        return 42;
    };

    TCallback callback2{a, b, std::move(c)};

    // Part 2. TSignal.
    TSignal userBankrupt;

    userBankrupt.Subscribe(ShowBankruptUser);

    std::shared_ptr<int> userBankruptCount = std::make_shared<int>(0);

    userBankrupt.Subscribe([userBankruptCount] {
        *userBankruptCount += 1;
    });

    userBankrupt.Fire();
    userBankrupt.Fire();

    std::cout << "User bankrupt count = " << *userBankruptCount << std::endl;

    userBankrupt.Subscribe([userBankruptCount] () -> int {
        *userBankruptCount += 1;
        return *userBankruptCount;
    });

    // Part 3. Function composition.
    {
        auto calculator = [] {
            std::cout << "Calculating..." << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            return 42;
        };

        calculator();

        auto brandNewCalculator = WrapIntoTiming(WrapIntoLogging(calculator));

        brandNewCalculator();

        auto modifyInt = [] (int& x) {
            x *= x;
        };
        auto modifyString = [] (std::string& s) {
            s += 'a';
        };

        auto modifyVector = WrapIntoMapping(modifyInt);
        auto modifyList = WrapIntoMapping(modifyString);

        std::vector<int> vec = {1, 2, 3};
        std::list<std::string> lst = {"aba", "caba"};

        modifyVector(vec);
        modifyVector(vec);
        modifyVector(vec);

        modifyList(lst);
        modifyList(lst);
        modifyList(lst);
        modifyList(lst);

        std::cout << vec[0] << " " << vec[1] << " " << vec[2] << std::endl;

        std::cout << *lst.begin() << std::endl;
    }

    // Part 4. Bind.
    {
        auto sumInts = [] (int a, int b, int c) {
            return a + b * b + c * c * c;
        };

        std::cout << sumInts(0, 1, 2) << std::endl;

        auto boundSumInts1 = [sumInts] (int a) {
            return sumInts(a, 10, 20);
        };
        (void) boundSumInts1;

        auto boundSumInts2 = [sumInts] (int a) {
            return sumInts(a, 3, 4);
        };
        (void) boundSumInts2;


        using std::placeholders::_1;
        using std::placeholders::_2;

        auto boundSumInts3 = std::bind(sumInts, 5, _1, 6);

        std::cout << "sum ints for a = 5, b = 10, c = 6: " << boundSumInts3(/*b*/ 10) << std::endl;
        std::cout << "sum ints for a = 5, b = 11, c = 6: " << boundSumInts3(/*b*/ 11) << std::endl;

        // Compilation error.
        // auto boundSumInts4 = std::bind(sumInts, 5, _1, 6, 10);
        // auto boundSumInts5 = std::bind(sumInts, 5, _2, 6, 10);
        // std::cout << boundSumInts4(10) << std::endl;
        // std::cout << boundSumInts5(10) << std::endl;

        auto sumStrings = [] (const std::string& a, const std::string& b) {
            return a + a + b;
        };
        auto boundSumStrings = BindFirst(sumStrings, "aba");
        std::cout << boundSumStrings("caba") << std::endl; // aba aba caba

        auto multiplyStrings = [] (int times, const std::string& s) {
            assert(times >= 0);
            std::string result;
            for (int i = 0; i < times; ++i) {
                result += s;
            }
            return result;
        };

        auto showTwoStrings = [] (const std::string& a, const std::string& b) {
            std::cout << "first string is " << a << std::endl;
            std::cout << "second string is " << b << std::endl;
        };

        auto boundMultiplyStrings = BindFirst(multiplyStrings, 3);
        std::cout << boundMultiplyStrings("aba") << std::endl; // aba aba aba
        std::cout << boundMultiplyStrings("caba") << std::endl; // caba caba caba

        BindFirst(showTwoStrings, "aba")("cde");
    }

    return 0;
}
