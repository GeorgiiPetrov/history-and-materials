#include <algorithm>
#include <iostream>
#include <numeric>
#include <optional>
#include <variant>
#include <vector>

template <class TIterator, class T>
TIterator Remove(TIterator begin, TIterator end, const T& value)
{
    TIterator result = begin;
    for (auto it = begin; it != end; ++it) {
        if ((*it) != value) {
            if (it != result) {
                *result = *it;
            }
            ++result;
        }
    }
    return result;
}

template <class TIterator, class TCondition>
TIterator RemoveIf(TIterator begin, TIterator end, const TCondition& condition)
{
    TIterator result = begin;
    for (auto it = begin; it != end; ++it) {
        if (!condition(*it)) {
            if (it != result) {
                *result = *it;
            }
            ++result;
        }
    }
    return result;
}

struct TLessThan3
{
    bool operator()(int value) const
    {
        return value < 3;
    }
};

template <class TContainer>
void Print(const TContainer& container)
{
    std::cout << container.size() << std::endl;
    for (auto it = container.begin(); it != container.end(); ++it) {
    // for (const auto& x : container) {
    // for (const typename TContainer::value_type& x : container) {
        std::cout << (*it) << " ";
    }
    std::cout << std::endl;
}

/*
template <class TContainer, class TCondition>
bool FindByCondition(const TContainer& container, const TCondition& condition, typename TContainer::value_type& result)
*/

template <class TContainer, class TCondition>
std::optional<typename TContainer::value_type> FindByCondition(const TContainer& container, const TCondition& condition)
{
    for (const auto& x : container) {
        if (condition(x)) {
            return x;
        }
    }
    return std::nullopt;
}

struct TGreaterThan10
{
    bool operator()(int value) const
    {
        return value > 10;
    }
};

struct TStudent
{
    int Age;
    int Grade;

    int Big;
    int Data;
    int Dummy;
    int BigDummy;
    int BigDataDummy;

    TStudent() = default;

    TStudent(int age, int grade)
        : Age(age)
        , Grade(grade)
    { }
};

struct TTeacher
{
    int Age;
};

struct TAgePrinter
{
    template <class TPerson>
    void operator()(const TPerson& person)
    {
        std::cout << "age = " << person.Age << std::endl;
    }
};

// Alignment.
struct TFancySizeOf0
{
    bool Flag; // Shift = 0

    // char Padding[3]

    int Data; // Shift = 4
};

struct TFancySizeOf1
{
    int Data; // Shift = 0

    bool Flag; // Shift = 4

    // char Padding[3]
};

struct TFancySizeOf2
{
    int Data;

    bool Flag1;
    bool Flag2;
    bool Flag3;

    // char Padding[1]
};

struct TFancySizeOf3
{
    int Data;

    bool Flag1;
    bool Flag2;
    bool Flag3;
    bool Flag4;
    bool Flag5;

    // char Padding[3]
};

struct TFancySizeOf4
{
    short Data;

    bool Flag1;
    bool Flag2;
    bool Flag3;
    bool Flag4;

    // No padding.
};

int main()
{
    // Part 1.
    std::vector<int> a = {1, 2, 4, 4, 4, 3};

    // auto it = std::remove(a.begin(), a.end(), 4);
    auto it = std::remove_if(a.begin(), a.end(), TLessThan3());

    Print(a);

    a.erase(it, a.end());

    Print(a);

    std::cout << "===================" << std::endl;

    // Part 2.
    std::vector<int> a2 = {1, 2, 4, 4, 4, 3};

    // auto it2 = Remove(a2.begin(), a2.end(), 4);
    auto it2 = RemoveIf(a2.begin(), a2.end(), TLessThan3());

    Print(a2);

    a2.erase(it2, a2.end());

    Print(a2);

    std::cout << "===================" << std::endl;

    // Part 3.
    std::vector<int> a3;
    for (int i = 0; i < 10; ++i) {
        a3.push_back(i);
        a3.push_back(-i);
    }

    Print(a3);

    auto it3 = a3.begin() + 10;

    nth_element(a3.begin(), it3, a3.end());

    Print(a3);

    std::cout << "===================" << std::endl;

    // Part 4.
    a3.push_back(10);

    std::cout << std::accumulate(a3.begin(), a3.end(), 5.5) << std::endl;

    std::cout << "===================" << std::endl;

    // Part 5.
    {
        std::optional<int> result = FindByCondition(a3, TLessThan3());
        std::cout << "has value = " << result.has_value() << std::endl;
        std::cout << *result << std::endl;
        std::cout << result.value() << std::endl;

        result = FindByCondition(a3, TGreaterThan10());
        std::cout << "has value = " << result.has_value() << std::endl;

        result = 10;
        std::cout << result.value() << std::endl;

        std::optional<TStudent> student;
        std::cout << "has value = " << student.has_value() << std::endl;
        // student->Age = 10; // UB!!!
        student.emplace(13, 14);
        student->Age = 10;
        (*student).Grade = 5;
        std::cout << "has value = " << student.has_value() << std::endl;

        // Compilation error.
        // std::optional<TStudent> student2(10, 20);
        std::optional<TStudent> student2(std::in_place, 10, 20);

        TStudent& s = student2.value();
        s.Age = 15;
        student2.reset();
        // s.Age = 15; // UB!!!

        TStudent student3(28, 42);
        std::optional<TStudent> student4(student3);

        std::cout << sizeof(student4) << std::endl;
    }

    std::cout << "===================" << std::endl;

    // Part 6.

    // Alignment.
    std::cout << sizeof(TFancySizeOf0{}) << std::endl;
    TFancySizeOf0 fs0;
    std::cout << &fs0.Flag << std::endl;
    std::cout << &fs0.Data << std::endl;
    std::cout << "diff = " << reinterpret_cast<size_t>(&fs0.Data) - reinterpret_cast<size_t>(&fs0.Flag) << std::endl;

    std::cout << sizeof(TFancySizeOf1{}) << std::endl;
    TFancySizeOf1 fs1;
    std::cout << &fs1.Data << std::endl;
    std::cout << &fs1.Flag << std::endl;
    std::cout << "diff = " << reinterpret_cast<size_t>(&fs1.Flag) - reinterpret_cast<size_t>(&fs1.Data) << std::endl;

    std::cout << sizeof(TFancySizeOf2{}) << std::endl;
    std::cout << sizeof(TFancySizeOf3{}) << std::endl;
    std::cout << sizeof(TFancySizeOf4{}) << std::endl;

    std::cout << "===================" << std::endl;

    // Part 7.
    {
        using TVariant = std::variant<int, TStudent, bool, double, std::vector<int>, float>;

        // TVariant v(10.0f);
        TVariant v;

        v = 10.0;
        // Error.
        // std::cout << std::get<int>(v) << std::endl;
        std::cout << std::get<double>(v) << std::endl;
        std::cout << "index = " << v.index() << std::endl;

        v = 10;
        std::cout << std::get<int>(v) << std::endl;
        std::cout << "index = " << v.index() << std::endl;

        v = TStudent(13, 15);
        std::cout << "index = " << v.index() << std::endl;

        if (v.index() == 0) {
            std::cout << "int = " << std::get<int>(v) << std::endl;
        } else if (v.index() == 1) {
            std::cout << "age = " << std::get<TStudent>(v).Age << std::endl;
        } else if (v.index() == 2) {
            std::cout << "bool = " << std::get<bool>(v) << std::endl;
        } else {
            assert(v.index() == 3);
            std::cout << "int = " << std::get<int>(v) << std::endl;
        }

        if (int* a = std::get_if<int>(&v)) {
            std::cout << "int = " << *a << std::endl;
        } else if (TStudent* s = std::get_if<TStudent>(&v)) {
            std::cout << "age = " << s->Age << std::endl;
        } else {
            assert(false);
        }

        std::cout << "size double = " << sizeof(std::variant<double>(10.0)) << std::endl;
        std::cout << "size int = " << sizeof(std::variant<int>(10)) << std::endl;
        std::cout << "size int float = " << sizeof(std::variant<int, float>(10)) << std::endl;
        std::cout << "size int float double = " << sizeof(std::variant<int, float, double>(10)) << std::endl;
        std::cout << "size int float short bool double = " << sizeof(std::variant<int, float, short, bool, double>(10)) << std::endl;
        std::cout << "size variant = " << sizeof(v) << std::endl;
        std::cout << "size vector = " << sizeof(std::vector<int>{}) << std::endl;
        std::cout << "alignof vector = " << alignof(std::vector<int>{}) << std::endl;
        std::cout << "alignof variant = " << alignof(a) << std::endl;

        /* Compilation error.
        std::variant<int, int, int> v2;
        v2 = 10;
        std::cout << "index = " << v2.index() << std::endl;
        */

        std::variant<TStudent, TTeacher> person;

        person = TStudent(13, 15);
        std::visit(TAgePrinter(), person);

        person = TTeacher{28};
        std::visit(TAgePrinter(), person);

        // Compilation error.
        // std::cout << person->Age << std::endl;
    }

    return 0;
}
