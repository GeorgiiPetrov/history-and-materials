#include <iostream>
#include <exception>
#include <vector>
#include <cerrno>
#include <string>
#include <csignal>

int Div(int x, int y, int* res) {
    if (y == 0) {
        return EINVAL;
    }
    *res = x / y;
    return 0;
}

int Div(int x, int y) {
    if (y == 0) {
        errno = EINVAL;
        return 0;
    }
    return x / y;
}

int DivException(int x, int y) {
    if (y == 0) {
        throw 1;
    }
    if (y < 0) {
        throw "negative";
    }
    if (y > 0) {
        throw 1.0;
    }
    return x / y;
}

class Exception {
public:
    explicit Exception(std::string msg)
        : msg_(std::move(msg))
    {
        // std::cout << "Construct" << std::endl;
    }

    Exception(const Exception& other)
        : msg_(other.msg_)
    {
        // std::cout << "Copy" << std::endl;
    }

    virtual ~Exception() {
        // std::cout << "Desctruct" << std::endl;
    }

    virtual std::string ErrMsg() const {
        return msg_;
    }

private:
    std::string msg_;
};

class ZeroDivision : public std::exception {
public:
    const char* what() const noexcept override {
        return "zero";
    }
};

int ThrowException() {
    int res = DivException(1, 0);
    return res + 10;
}

void ThrowClassException() {
    throw ZeroDivision();
}

class Vec {
public:
    Vec() {
        data_ = new int[10];
        // throw 1;
    }

    ~Vec() {
        // throw 2;
        delete[] data_;
    }

private:
    int* data_;
};

// RAII
void Func() {
    std::unique_ptr<int[]> data(new int[10]);
    ThrowException();
}

void Handler(int signum) {
    std::cout << "Signal" << std::endl;
}

int main() {
    /*
    int res = Div(1, 1);
    if (errno != 0) {
        perror("error");
        exit(1);
    }
    int err = Div(1, 0, &res);
    if (err == EINVAL) {
        std::cout << "Error zero division" << std::endl;
    }
    */

    /*
    int x;
    try {
        x = ThrowException();
    } catch (long long err) {
        std::cout << "exception: " << err << std::endl;
        return 0;
    } catch (const char* err) {
        std::cout << "exception: " << err << std::endl;
        return 0;
    } catch (...) {
        std::cout << "exception!" << std::endl;
        return 0;
    }
    std::cout << x << std::endl;
    */

    /*
    try {
        ThrowClassException();
    } catch (const std::exception& ex) {
        std::cout << ex.what() << std::endl;
    }
    */

    /*
    try {
        Vec vec;
    } catch (...) {
        std::cout << "exception" << std::endl;
    }

    ZeroDivision x;
    const std::exception& ex = x;
    dynamic_cast<const Vec&>(ex);
    */

    signal(SIGINT, Handler);

    for (;;) {

    }

    return 0;
}
