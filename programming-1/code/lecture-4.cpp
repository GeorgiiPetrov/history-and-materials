#include <iostream>
#include <string>
#include <array>
using namespace std;

template <typename T = int>
const T& Min(const T& x, const T& y) {
    return x < y ? x : y;
}

template <class A = std::string>
A CreateDefault() {
    A a{};
    return a;
}

struct X {
    int x;
};

// bool operator<(const X& lhs, const X& rhs) {
//     return lhs.x < rhs.x;
// }

template <>
const X& Min<X>(const X& x, const X& y) {
    return x.x * 10 > y.x - 100 ? x : y;
}

template <typename T>
const std::vector<T>& Min(const std::vector<T>& a, const std::vector<T>& b) {
    return a.size() < b.size();
}

template <typename T, size_t N = 10>
class Vector {
public:
    Vector() = default;

    const T& Get(int i) const {
        return data[i];
    }

    size_t Size() const {
        return N;
    }

private:
    T data[N];
};

template <size_t N>
class Vector<bool, N> {

};

template <typename T, size_t N>
class Vector<T*, N> {
public:
    Vector() = default;

    const T* Get(int i) const {
        return &data[i];
    }

    size_t Size() const {
        return N;
    }

private:
    T data[N];
};

int main() {
    cout << Min(X{1}, X{2}).x << endl;
    cout << Min(1, 1) << endl;
    cout << CreateDefault() << endl;

    Vector<int> v_int;
    Vector<char> v_char;
    v_char.Get(0);

    // int n;
    // cin >> n;
    // Vector<int, n> v;
    Vector<int*, 100> vec;
    std::array<int, 5> arr;

    return 0;
}
