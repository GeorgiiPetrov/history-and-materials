#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_map>
#include <utility>

std::pair<int, int>& operator+=(std::pair<int, int>& lhs, const std::pair<int, int>& rhs) {
    lhs.first += rhs.first;
    lhs.second += rhs.second;
    return lhs;
}

// out << x << y;
// out.operator<<(x).operator<<(y)
std::ostream& operator<<(std::ostream& out, std::pair<int, int> value) {
    out << '(' << value.first << ", " << value.second << ')';
    return out;
}

void func(const std::string& x) {
    (void)x;
}

// func("asabas");
// const char* ptr = "asdfasd";
//

size_t StrLen(const char* s) {
    const char* begin = s;
    while (*s++ != '\0')
        ;
    return s - begin - 1;
}

// const char* s = "asdfasdfasdfa";
// StringView sv(s, 2);

class StringView {
public:
    StringView()
        : data_(nullptr)
        , size_(0)
    { }

    StringView(const std::string& s)
        : data_(s.data())
        , size_(s.size())
    { }

    StringView(const char* s, size_t size)
        : data_(s)
        , size_(size)
    { }

    // StringView("asdfasd")
    StringView(const char* s)
        : data_(s)
        , size_(StrLen(s))
    { }

    const char* begin() const {
        return data_;
    }

    const char* end() const {
        return data_ + size_;
    }

    char operator[](size_t index) const {
        return data_[index];
        // return *(data_ + index);
    }

    size_t Size() const {
        return size_;
    }

    const char* Data() const {
        return data_;
    }

    StringView SubStr(size_t pos, size_t n) const {
        return StringView(data_ + pos, n);
    }

    StringView Head(size_t n) const {
        return SubStr(0, n);
    }

    StringView Tail(size_t n) const {
        return SubStr(size_ - n, n);
    }

    void Clear() {
        data_ = nullptr;
        size_ = 0;
    }

    const char* FindFirst(char c) const {
        for (size_t i = 0; i < size_; ++i) {
            if (data_[i] == c) {
                return data_ + i;
            }
        }
        return end();
    }

    const char* FindLast(char c) const {
        const char* res = end();
        for (size_t i = 0; i < size_; ++i) {
            if (data_[i] == c) {
                res = data_ + i;
            }
        }
        return res;
    }

    void Split(char delim, StringView* lhs, StringView* rhs) const {
        const char* pos = FindFirst(delim);
        if (pos != end()) {
            *lhs = StringView(data_, pos - data_);
            *rhs = StringView(pos + 1, size_ - lhs->Size() - 1);
        } else {
            *lhs = *this;
            rhs->Clear();
        }
    }

    // void RSplit

private:
    const char* data_;
    size_t size_;
};

int StrCmp(StringView lhs, StringView rhs) {
    const size_t len = std::min(lhs.Size(), rhs.Size());
    for (size_t i = 0; i < len; ++i) {
        if (lhs[i] == rhs[i]) {
            continue;
        }
        return lhs[i] - rhs[i];
    }
    return static_cast<int>(lhs.Size()) - static_cast<int>(rhs.Size());
}

bool operator<(StringView lhs, StringView rhs) {
    return StrCmp(lhs, rhs) < 0;
}

bool operator==(StringView lhs, StringView rhs) {
    return StrCmp(lhs, rhs) == 0;
}

// const StringView sv("asdfasdf");
// for (char c : sv)
// sv[2];

int main() {
    {
        std::string steps = "udlruudddllr";
        std::unordered_map<char, std::pair<int, int>> vectors;
        vectors['u'] = {0, 1};
        vectors['d'] = {0, -1};
        vectors['l'] = {-1, 0};
        vectors['r'] = {1, 0};

        std::pair<int, int> position(0, 0);

        int x = 10;
        (x += 10) += 4; // operator+=(operator+=(x, 10), 4);

        for (char c : steps) {
            position += vectors[c];  // operator+=(position, vectors[c]);
        }

        std::cout << position << std::endl;
        std::cout << "======================" << std::endl;
    }

    {
        func("asdfasdf");
    }

    return 0;
}
