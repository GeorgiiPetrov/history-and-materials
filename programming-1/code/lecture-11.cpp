#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

struct TStruct
{
    // ...
};

TStruct DoSomeStuff()
{
    // ...
}

auto DoSomeStuff2()
{
    return std::vector<std::map<int, std::unordered_set<std::string>>>{};
}

// Compilation error.
/*
auto DoSomeStuff3(int a)
{
    if (a < 10) {
        return 11;
    } else {
        return 15.3;
    }
}
*/

struct TStructComparator
{
    bool operator()(const TStruct& lhs, const TStruct& rhs)
    {
        return true;
    }
};
/* <=>
[] (const TStruct& lhs, const TStruct& rhs) {
    return true;
};
*/

template <class TIterator, class TComparator>
void Sort(TIterator begin, TIterator end, const TComparator& comparator)
{ }

template <class T1, class T2>
auto DoSomeStuff4(T1 t1, T2 t2) -> decltype(t1 + 1)
{
    if (t1 < t2) {
        return t1 + 1;
    } else {
        return t2 + 1;
    }
}

int main()
{
    // Part 1: auto, decltype.

    // Part 1.1: auto basics.
    auto x = 10;
    auto y = 10.0;

    auto z = DoSomeStuff2();

    {
        std::vector<int> results;

        for (auto x : results) {
        }

        std::vector<std::string> stringResults;
        for (const auto& x : stringResults) {
        }

        for (auto it = stringResults.begin(); it != stringResults.end(); ++it) {
        }
    }

    // Part 1.2: decltype.
    static_assert(std::is_same<decltype(x), int>::value);
    static_assert(std::is_same<decltype(y), double>::value);
    static_assert(std::is_same<decltype(DoSomeStuff()), TStruct>::value);

    std::vector<decltype(DoSomeStuff())> results;
    results.push_back(DoSomeStuff());

    DoSomeStuff4(10, 15.3);

    // Part 1.3: const auto.
    {
        const int x = 10;
        auto y = x;
        static_assert(std::is_same<decltype(y), int>::value);

        const auto z = x;
        static_assert(std::is_same<decltype(z), const int>::value);
    }

    // Part 1.4: auto&.
    {
        int x = 10;
        auto& y = x;
        static_assert(std::is_same<decltype(y), int&>::value);

        int a = 10;
        int& b = a;
        auto c = b;
        static_assert(std::is_same<decltype(c), int>::value);

        const int k = 10;
        auto& l = k;
        static_assert(std::is_same<decltype(l), const int&>::value);

        // auto&&
    }

    // Part 2.1: lambda basics.
    {
        std::vector<TStruct> structs;
        std::sort(structs.begin(), structs.end(), [] (const TStruct& lhs, const TStruct& rhs) {
            return true;
        });

        auto comparator = [] (const TStruct& lhs, const TStruct& rhs) {
            return true;
        };
        std::sort(structs.begin(), structs.end(), comparator);

        std::sort(structs.begin(), structs.end(), [] (const auto& lhs, const auto& rhs) {
            return true;
        });

        auto incrementOne = [] (const auto& x) {
            return x + 1;
        };
        incrementOne(10);
        incrementOne(10.0);
    }

    // Part 2.2: lambda return type.
    {
        auto f = [] {
            return 10.0;
        };

        const int constValue = [] {
            // ...
            return 10;
        }();

        auto g = [] (int x) -> double {
            if (x < 10) {
                return 11;
            } else {
                return 15.3;
            }
        };

        // Implicit cast.
        auto g2 = [] () -> short {
            return 42;
        };
    }

    // Part 2.3: lambda state.
    {
        std::vector<int> results;
        std::string name = "abacaba";

        auto pushResult = [&results] (int x) {
            results.push_back(x);
        };

        pushResult(10);
        pushResult(15);

        auto showResults = [results] {
            std::cout << "Results = [";
            for (auto x : results) {
                std::cout << x << ", ";
            }
            std::cout << "]" << std::endl;
        };

        auto showResults2 = [=, &results] {
            std::cout << "Results " << name << " = [";
            for (auto x : results) {
                std::cout << x << ", ";
            }
            std::cout << "]" << std::endl;
        };

        auto showResults3 = [&, name] {
            std::cout << "Results " << name << " = [";
            for (auto x : results) {
                std::cout << x << ", ";
            }
            std::cout << "]" << std::endl;
        };

        showResults();
        showResults2();

        pushResult(20);

        showResults();
        showResults2();

        auto showResults4 = [results = std::move(results)] {
            std::cout << "Results = [";
            for (auto x : results) {
                std::cout << x << ", ";
            }
            std::cout << "]" << std::endl;
        };

        auto pushResult2 = [results] (int x) mutable {
            results.push_back(x);
            std::cout << "Value of " << x << " pushed back" << std::endl;
        };

        pushResult2(10);

        showResults4();

        std::cout << "Results size = " << results.size() << std::endl;
        results.clear();

        auto showResults5 = [results = &results] {
            std::cout << "Results = [";
            for (auto x : *results) {
                std::cout << x << ", ";
            }
            std::cout << "]" << std::endl;
        };
    }

    // Part 2.4: lambda as an argument.
    {
        auto comparator = [] (const auto& lhs, const auto& rhs) {
            return true;
        };

        std::vector<int> results;

        Sort(results.begin(), results.end(), comparator);

        auto comparator2 = std::move(comparator);
    }

    // Part 3: structured bindings.

    {
        std::map<int, std::string> m;
        for (const auto& [intKey, stringValue] : m) {
        }

        struct TStruct2
        {
            int X;
            int Y;
            double Z;
            std::string W;
        };

        TStruct2 s{1, 2, 15.3, "abcaba"};

        auto [x, y, z, w] = s;

        std::cout << "W = " << w << std::endl;
    }

    // Part 4: std::function.

    {
        std::function<void(int)> f = [] (int x) {
            std::cout << "Hello, X = " << x << std::endl;
        };

        f(10);
        f(20);

        auto g = [] (const std::function<void(int)>& hello) {
            hello(10);
            hello(20);
        };

        g(f);

        /*
        template <class TLambda>
        void DoSomeStuff(const TLambda& lambda)
        { }

        vs

        void DoSomeStuff(const std::function<void(int)>& lambda)
        { }

        1. Explicit signature.
        2. Compilation time.
        3. Sticky template arguments.
        4. std::function expensive.
        */
    }

    return 0;
}
