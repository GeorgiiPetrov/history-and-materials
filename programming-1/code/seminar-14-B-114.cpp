#include <iostream>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

/////////////////////////////////////////////////////////////////////////////////////

struct ISubject;

struct ISubjectIterator
{
    virtual ~ISubjectIterator() = default;
    virtual const ISubject* Get() const = 0;
    virtual void Next() = 0;
    virtual bool IsEnd() const = 0;
};

using ISubjectIteratorPtr = std::unique_ptr<ISubjectIterator>;

/////////////////////////////////////////////////////////////////////////////////////

class TEmptySubjectIterator
    : public ISubjectIterator
{
public:
    const ISubject* Get() const override
    {
        return nullptr;
    }

    void Next() override
    {
        assert(false);
    }

    bool IsEnd() const override
    {
        return true;
    }
};

/////////////////////////////////////////////////////////////////////////////////////

template <class TUnderlyingIterator>
class TContainerSubjectIterator
    : public ISubjectIterator
{
public:
    TContainerSubjectIterator(TUnderlyingIterator begin, TUnderlyingIterator end)
        : End_(end)
        , Current_(begin)
    { }

    const ISubject* Get() const override
    {
        return *Current_;
    }

    void Next() override
    {
        ++Current_;
    }

    bool IsEnd() const override
    {
        return Current_ == End_;
    }

private:
    const TUnderlyingIterator End_;

    TUnderlyingIterator Current_;
};

template <class TUnderlyingIterator>
ISubjectIteratorPtr CreateContainerIterator(TUnderlyingIterator begin, TUnderlyingIterator end)
{
    return std::make_unique<TContainerSubjectIterator<TUnderlyingIterator>>(begin, end);
}

/////////////////////////////////////////////////////////////////////////////////////

struct ISubject
{
    virtual ~ISubject() = default;

    virtual std::string GetSubjectName() const = 0;

    virtual ISubjectIteratorPtr GetMembersIterator() const = 0;
    virtual ISubjectIteratorPtr GetFollowersIterator() const = 0;
};

/////////////////////////////////////////////////////////////////////////////////////

class TUser
    : public ISubject
{
public:
    explicit TUser(std::string name)
        : Name_(std::move(name))
    { }

    const std::string& GetName() const
    {
        return Name_;
    }

    void Follow(TUser* other)
    {
        Followees_.insert(other);
        other->Followers_.insert(this);
    }

    // ISubject implementation.

    std::string GetSubjectName() const override
    {
        return "user " + Name_;
    }

    ISubjectIteratorPtr GetMembersIterator() const override
    {
        return std::make_unique<TEmptySubjectIterator>();
    }

    ISubjectIteratorPtr GetFollowersIterator() const override
    {
        return CreateContainerIterator(Followers_.begin(), Followers_.end());
    }

private:
    const std::string Name_;

    std::unordered_set<TUser*> Followees_;
    std::unordered_set<TUser*> Followers_;
};

/////////////////////////////////////////////////////////////////////////////////////

class TGroup
    : public ISubject
{
public:
    explicit TGroup(std::string name)
        : Name_(std::move(name))
    { }

    void AddMember(const ISubject* subject)
    {
        Members_.insert(subject);
    }

    // ISubject implementation.

    std::string GetSubjectName() const override
    {
        return "group " + Name_;
    }

    ISubjectIteratorPtr GetMembersIterator() const override
    {
        return CreateContainerIterator(
            Members_.begin(),
            Members_.end());
    }

    ISubjectIteratorPtr GetFollowersIterator() const override
    {
        return std::make_unique<TEmptySubjectIterator>();
    }

private:
    const std::string Name_;

    std::unordered_set<const ISubject*> Members_;
};

/////////////////////////////////////////////////////////////////////////////////////

class TMephistNetwork
{
public:
    TUser* CreateUser(std::string name)
    {
        auto* user = new TUser(std::move(name));
        std::unique_ptr<ISubject> subject(user);
        Subjects_.push_back(std::move(subject));
        return user;
    }

private:
    std::vector<std::unique_ptr<ISubject>> Subjects_;
};

/////////////////////////////////////////////////////////////////////////////////////

int main()
{
    TMephistNetwork network;

    auto* dmitry = network.CreateUser("latyshev");
    auto* sasha = network.CreateUser("minakov");

    std::cout << dmitry->GetName() << std::endl;
    dmitry->Follow(sasha);

    // Virtual destructor is required.
    // Private vs public inheritance.
    // Unordered set<TUser*> -> unordered set<ISubject*> does not work: different std::hash implementation + different pointer.
    // Virtual constructor does not exist.

    return 0;
}
