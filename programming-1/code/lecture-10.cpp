#include <iostream>
#include <memory>
#include <string>

int f();

int global_x;
int& g() {
    return global_x;
}
int&& h();

class FixedVector {
public:
    FixedVector(int size)
        : size_(size)
        , buf_(new int[size])
    {
        std::cout << "Create" << std::endl;
    }

    FixedVector(const FixedVector& other)
        : size_(other.size_)
        , buf_(new int[size_])
    {
        std::cout << "Copy" << std::endl;
        for (int i = 0; i < size_; ++i) {
            buf_[i] = other.buf_[i];
        }
    }

    ~FixedVector() {
        if (buf_ != nullptr) {
            delete[] buf_;
        }
    }

    FixedVector(FixedVector&& other)
        : size_(other.size_)
        , buf_(other.buf_)
    {
        std::cout << "Move" << std::endl;
        other.buf_ = nullptr;
    }

    FixedVector& operator=(const FixedVector& other) {
        if (&other == this) {
            return *this;
        }

        size_ = other.size_;
        delete[] buf_;
        buf_ = new int[size_];
        for (int i = 0; i < size_; ++i) {
            buf_[i] = other.buf_[i];
        }
        return *this;
    }

    FixedVector& operator=(FixedVector&& other) {
        if (&other == this) {
            return *this;
        }
        size_ = other.size_;
        buf_ = other.buf_;
        other.buf_ = nullptr;
        return *this;
    }

    int Size() const {
        return size_;
    }

private:
    int size_;
    int* buf_;
};

class S {
public:
    S(const FixedVector& vec)
        : vec_(vec)
    {
        std::cout << "S(const&)" << std::endl;
    }

    S& operator=(const S& other) {
        vec_ = other.vec_;
        return *this;
    }

    S& operator=(S&& other) {
        vec_ = std::move(other.vec_);
        return *this;
    }

    S(FixedVector&& vec)
        : vec_(std::move(vec))
    {
        std::cout << "S(&&)" << std::endl;
    }

private:
    FixedVector vec_;
};

/*
void func() {
    // int* buf = new int[10];
    SmartPtr buf(new int[10]);

    // ...
    if (...) {
        // delete[] buf;
        return;
    }
    if (...) {
        // delete[] buf;
        return;
    }
    if (...) {
        // delete[] buf;
        return;
    }

    // delete[] buf;
}
*/

// std::unique_ptr
class SmartPtr {
public:
    SmartPtr(std::string* ptr)
        : ptr_(ptr)
    { }

    ~SmartPtr() {
        delete ptr_;
    }

    std::string* Get() {
        return ptr_;
    }

    SmartPtr(const SmartPtr& other) = delete;
    SmartPtr& operator=(const SmartPtr& other) = delete;

    SmartPtr(SmartPtr&& other)
        : ptr_(other.ptr_)
    {
        other.ptr_ = nullptr;
    }
    // operator=(SmartPtr&&);

private:
    std::string* ptr_;
};

int main() {
    int x;

    // l-value:
    x;
    x = 1;
    g() = 4;

    int& p = x;
    int& p2 = g();

    // r-value:
    x + 1;
    55;
    // f();
    // h();
    int&& u = 55;
    u = 10;
    std::cout << u << std::endl;

    FixedVector vec(10);
    FixedVector vec1(vec);

    std::cout << "Create S" << std::endl;
    S s(FixedVector(5));
    S s1(std::move(vec));

    s = s1;
    s = std::move(s1);
    // s1 - нельзя читать, тк было перещемение

    SmartPtr ptr(new std::string("asadf"));
    SmartPtr ptr2 = std::move(ptr);

    // std::shared_ptr

    return 0;
}
