#ifndef LESSON14_TEVENT_H
#define LESSON14_TEVENT_H

class IEvent {
    public:
        virtual void Show() const = 0;
};

#endif // LESSON14_TEVENT_H