//
// Created by grigorijivanenko on 12.12.22.
//

#include <iostream>
#include "TTextEvent.h"

void TTextEvent::Show() const {
    std::cout << "User with pointer " << Author_ << " wrote\"" << Text_ << "\"\n";
}

TTextEvent::TTextEvent(const string& text, TUser* author) : Text_(text), Author_(author) { }