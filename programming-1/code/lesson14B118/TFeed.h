//
// Created by grigorijivanenko on 12.12.22.
//

#ifndef LESSON14_TFEED_H
#define LESSON14_TFEED_H

#include "IEvent.h"
#include <vector>
#include <memory>

class TFeed {
private:
    std::vector<std::unique_ptr<IEvent>> holder;
public:
    void Show() const;
    void AddEvent(std::unique_ptr<IEvent> newEvent);
};

#endif //LESSON14_TFEED_H
