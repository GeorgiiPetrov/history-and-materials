#include "TUser.h"
#include "TGroop.h"
#include <iostream>

using namespace std;

int main() {
    TUser user1;
    TUser user2;
    int res = user1.Subscribe(&user2);
    cout << res << endl;
}
