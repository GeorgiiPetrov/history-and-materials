//
// Created by grigorijivanenko on 12.12.22.
//

#include "TFeed.h"

void TFeed::Show() const {
    for (const auto& event : holder)
        event->Show();
}

void TFeed::AddEvent(std::unique_ptr<IEvent> newEvent) {
    holder.push_back(std::move(newEvent));
}