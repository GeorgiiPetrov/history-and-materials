#ifndef LESSON14_TUSER_H
#define LESSON14_TUSER_H

#include <unordered_set>
#include "TFeed.h"

using namespace std;

class TUser {
private:
    unordered_set<TUser*> Folowers_;
    unordered_set<TUser*> Folowees_;
    TFeed Feed_;
public:
    TUser();
    int Subscribe(TUser*);
    int UnSubscribe(TUser*);
};

#endif // LESSON14_TUSER_H
