#include "TUser.h"
#include "TMemberChangeEvent.h"

TUser::TUser() { }

int TUser::Subscribe(TUser* user) {
    if (user->Folowers_.count(this))
        return 1;
    TMemberChangeEvent ev(true, this, nullptr);
    std::unique_ptr<IEvent> nw = std::make_unique<TMemberChangeEvent>(std::move(ev));
    user->Feed_.AddEvent(std::move(nw));
    user->Folowers_.insert(this);
    Folowees_.insert(user);
    return 0;
}

int TUser::UnSubscribe(TUser* user) {
    if (user->Folowers_.count(this) == 0)
        return 1;
    TMemberChangeEvent ev(false, this, nullptr);
    std::unique_ptr<IEvent> nw = std::make_unique<TMemberChangeEvent>(std::move(ev));
    user->Feed_.AddEvent(std::move(nw));
    user->Folowers_.erase(this);
    Folowees_.erase(user);
    return 0;

}
