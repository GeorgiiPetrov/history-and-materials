#include "TMemberChangeEvent.h"
#include <iostream>

TMemberChangeEvent::TMemberChangeEvent(bool ent, TUser* us, TGroop* gro)
    : Enter_(ent), Who_(us), Groop_(gro)
{ }

string TMemberChangeEvent::whatDid() const{
    if (Enter_)
        return "enter to";
    else
        return "went out from";
}

void TMemberChangeEvent::Show() const {
    if (Groop_ == nullptr)
        std::cout << Who_ << whatDid() << "you\n";
    else
        std::cout << Who_ << whatDid() << Groop_->GetDescription() << '\n';
}

