#ifndef LESSON14_TGROOP_H
#define LESSON14_TGROOP_H

#include <unordered_set>
#include "TUser.h"
#include <string>

class TGroop{
private:
    unordered_set<TUser*> members;
    unordered_set<TUser*> black_list;
    string description;
public:
    TGroop(const string&);
    int TrySubscribe(TUser* newMember);
    int UnSubscribe(TUser* member);
    void SetDescription(const string&);
    string GetDescription() const;
};

#endif // LESSON14_TGROOP_H