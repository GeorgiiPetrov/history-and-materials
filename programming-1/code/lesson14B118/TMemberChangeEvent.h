#ifndef LESSON14_TMEMBERCHANGEEVENT_H
#define LESSON14_TMEMBERCHANGEEVENT_H


#include "IEvent.h"
#include "TUser.h"
#include "TGroop.h"
#include <iostream>


class TMemberChangeEvent : public IEvent {
private:
    bool Enter_;
    TUser* Who_;
    TGroop* Groop_;
    string whatDid() const;
public:
    TMemberChangeEvent(bool, TUser*, TGroop*);
    void Show() const override;
};

#endif // LESSON14_TMEMBERCHANGEEVENT_H