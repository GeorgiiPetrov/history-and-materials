#include "TGroop.h"

TGroop::TGroop(const string& desc)
    : description(desc)
{ }

int TGroop::TrySubscribe(TUser* newMember) {
    if (members.count(newMember)) {
        return 1;
    }
    if (black_list.count(newMember)) {
        return 1;
    }
    members.insert(newMember);
    return 0;
}

int TGroop::UnSubscribe(TUser* newMember) {
    if (members.count(newMember) == 0) {
        return 1;
    }
    members.erase(newMember);
    return 0;
}

string TGroop::GetDescription() const {
    return description;
}
