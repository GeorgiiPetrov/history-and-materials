//
// Created by grigorijivanenko on 12.12.22.
//

#ifndef LESSON14_TTEXTEVENT_H
#define LESSON14_TTEXTEVENT_H

#include <string>
#include "TUser.h"

class TTextEvent : public IEvent {
private:
    string Text_;
    TUser* Author_;
public:
    TTextEvent(const string&, TUser*);
    void Show() const override;
};

#endif //LESSON14_TTEXTEVENT_H
