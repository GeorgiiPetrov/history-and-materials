#include <vector>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>

using namespace std;

class TNumber {
    friend ostream& operator <<(ostream&, const TNumber&);
private:
    string data;
    string type;

public:
    TNumber() : data(""), type("string") { } 
    TNumber(const string& s) : data(s), type("string") { } 
    TNumber(const int& num) : data(to_string(num)), type("int") { }

    bool operator <(const TNumber& other) {
        return data < other.data;    
    }
    bool operator !=(const TNumber& other) {
        return data != other.data;
    }
    TNumber operator +(const TNumber& other) {
        TNumber responce(data + "+" + other.data);
        if (other.type == "int" && type == "int") {
            responce.type = "int";
        }
        return responce;
    }
};

ostream& operator <<(ostream& out, const TNumber& thi) {
    return out << "(" << thi.data << " " << thi.type << ")";
}

template<class T>
ostream& operator <<(ostream& out, const vector<T>& thi) {
    out << "[";
    for (auto& x : thi)
        out << x << ", ";
    return out << "]";
}

template<class T>
typename vector<T>::iterator StlUnique(typename vector<T>::iterator begin, typename vector<T>::iterator end) {
    if (begin == end) {
        return begin;
    }
    typename vector<T>::iterator help = begin++;
    for (; begin != end; ++begin) {
        if (*begin != *help) {
            ++help;
            *help = *begin;
        }
    }
    ++help;
    return help;
}

template<class T>
void KeepUnique(vector<T>& data) {
    sort(data.begin(), data.end());
    auto it = StlUnique<T>(data.begin(), data.end());
    int size = it - data.begin();
    data.resize(size);
}

int main() {
    vector<TNumber> numbers;
    //содержит строку или число, лексикографическое сравнение
    numbers.emplace_back(TNumber{20});
    numbers.emplace_back(TNumber{10});
    numbers.emplace_back(TNumber{"10"});
    numbers.emplace_back(TNumber{9});
    numbers.emplace_back(TNumber{"10"});
    numbers.emplace_back(TNumber{"11"});
    numbers.emplace_back(TNumber{11});
    numbers.emplace_back(TNumber{"1"});
    numbers.emplace_back(TNumber{"30"});
    //оставить уникальные, std::sort + остальное со своей реализацией
    KeepUnique(numbers);
    assert(numbers.size() == 6);
    //вывод элемента - строчка (type, value)
    cout << "first " << numbers[0] << endl;
   cout << "all " << numbers << endl;
    //своя реализация
    cout << accumulate(numbers.begin(), numbers.end(), TNumber{""}) << endl;
    cout << "all " << numbers << endl;
    return 0;
}
