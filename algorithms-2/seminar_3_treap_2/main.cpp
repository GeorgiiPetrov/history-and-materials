#include "treap.h"

#include <cassert>
#include <iostream>

const int MOD = 1e9;

int main()
{
    int n;
    std::cin >> n;

    TTreap treap;

    int lastLowerBound = 0;
    for (int i = 0; i < n; ++i) {
        char operation;
        int key;
        std::cin >> operation >> key;
        if (operation == '+') {
            key += lastLowerBound;
            if (key < 0) {
                key += MOD;
            }
            key %= MOD;
            treap.Insert(key);
            lastLowerBound = 0;
        } else {
            assert(operation == '?');
            auto lowerBound = treap.FindLowerBound(key);
            lastLowerBound = lowerBound.value_or(-1);
            std::cout << lastLowerBound << '\n';
        }
    }

    std::cout.flush();

    return 0;
}