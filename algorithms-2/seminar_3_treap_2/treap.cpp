#include "treap.h"

#include <random>

int GeneratePriority()
{
    static std::mt19937 Generator(42);
    return std::uniform_int_distribution<int>()(Generator);
}

void TTreap::Insert(int key)
{
    auto [leftMiddle, right] = Split(Root_, key);
    auto [left, middle] = Split(leftMiddle, key - 1);
    if (middle == -1) {
        Nodes_.emplace_back();
        Nodes_.back().Key = key;
        Nodes_.back().Priority = GeneratePriority();
        Nodes_.back().Left = -1;
        Nodes_.back().Right = -1;
        middle = Nodes_.size() - 1;
    }
    Root_ = Merge(left, Merge(middle, right));
}

bool TTreap::Find(int key)
{
    auto [leftMiddle, right] = Split(Root_, key);
    auto [left, middle] = Split(leftMiddle, key - 1);
    auto result = middle != -1;
    Root_ = Merge(left, Merge(middle, right));
    return result;
}

std::optional<int> TTreap::FindLowerBound(int key)
{
    int current = Root_;
    int result = -1;
    while (current >= 0) {
        if (Nodes_[current].Key >= key) {
            result = current;
            current = Nodes_[current].Left;
        } else {
            current = Nodes_[current].Right;
        }
    }
    return result == -1 ? std::nullopt : std::optional<int>(Nodes_[result].Key);
}

std::pair<int, int> TTreap::Split(int current, int key)
{
    if (current == -1) {
        return {-1, -1};
    }
    if (Nodes_[current].Key <= key) {
        auto [rightLeft, rightRight] = Split(Nodes_[current].Right, key);
        Nodes_[current].Right = rightLeft;
        return {current, rightRight};
    } else {
        auto [leftLeft, leftRight] = Split(Nodes_[current].Left, key);
        Nodes_[current].Left = leftRight;
        return {leftLeft, current};
    }
}

int TTreap::Merge(int left, int right)
{
    if (left == -1) {
        return right;
    }
    if (right == -1) {
        return left;
    }
    if (Nodes_[left].Priority >= Nodes_[right].Priority) {
        Nodes_[left].Right = Merge(Nodes_[left].Right, right);
        return left;
    } else {
        Nodes_[right].Left = Merge(left, Nodes_[right].Left);
        return right;
    }
}