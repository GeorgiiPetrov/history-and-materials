#pragma once

#include <optional>
#include <utility>
#include <vector>

struct TTreapNode
{
    int Key;
    int Priority;
    int Left;
    int Right;
};

class TTreap
{
public:
    std::optional<int> FindLowerBound(int key) const;
    void Insert(int key);

private:
    std::vector<TTreapNode> Nodes_;

    int Root_ = -1;

    int AllocateNode(int key, int priority, int left, int right);
    int GeneratePriority();

    std::pair<int, int> Split(int current, int key, bool* found);
    int Merge(int left, int right);

    int InsertImpl(int current, int key, int priority);
};
