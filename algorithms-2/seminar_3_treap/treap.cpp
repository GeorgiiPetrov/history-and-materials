#include "treap.h"

#include <random>

std::optional<int> TTreap::FindLowerBound(int key) const
{
    int result = -1;
    int current = Root_;
    while (current >= 0) {
        if (key == Nodes_[current].Key) {
            result = current;
            break;
        } else if (key < Nodes_[current].Key) {
            result = current;
            current = Nodes_[current].Left;
        } else {
            current = Nodes_[current].Right;
        }
    }
    return result == -1 ? std::nullopt : std::optional<int>(Nodes_[result].Key);
}

void TTreap::Insert(int key)
{
    Root_ = InsertImpl(Root_, key, GeneratePriority());
}

std::pair<int, int> TTreap::Split(int current, int key, bool* found)
{
    if (current == -1) {
        return std::make_pair(-1, -1);
    }
    if (Nodes_[current].Key == key) {
        *found = true;
    }
    if (Nodes_[current].Key <= key) {
        auto [left, right] = Split(Nodes_[current].Right, key, found);
        Nodes_[current].Right = left;
        return std::make_pair(current, right);
    } else {
        auto [left, right] = Split(Nodes_[current].Left, key, found);
        Nodes_[current].Left = right;
        return std::make_pair(left, current);
    }
}

int TTreap::Merge(int left, int right)
{
    if (left == -1) {
        return right;
    }
    if (right == -1) {
        return left;
    }
    if (Nodes_[left].Priority < Nodes_[right].Priority) {
        Nodes_[left].Right = Merge(Nodes_[left].Right, right);
        return left;
    } else {
        Nodes_[right].Left = Merge(left, Nodes_[right].Left);
        return right;
    }
}

int TTreap::AllocateNode(int key, int priority, int left, int right)
{
    Nodes_.emplace_back();
    Nodes_.back().Key = key;
    Nodes_.back().Priority = priority;
    Nodes_.back().Left = left;
    Nodes_.back().Right = right;
    return Nodes_.size() - 1;
}

int TTreap::GeneratePriority()
{
    static std::mt19937 Generator(42);
    return std::uniform_int_distribution<int>()(Generator);
}

int TTreap::InsertImpl(int current, int key, int priority)
{
    if (current == -1) {
        return AllocateNode(key, priority, -1, -1);
    }
    if (priority >= Nodes_[current].Priority) {
        bool found = false;
        auto [left, right] = Split(current, key, &found);
        if (found) {
            return Merge(left, right);
        } else {
            return AllocateNode(key, priority, left, right);
        }
    } else {
        if (Nodes_[current].Key == key) {
            return current;
        } else if (Nodes_[current].Key < key) {
            Nodes_[current].Right = InsertImpl(Nodes_[current].Right, key, priority);
            return current;
        } else {
            Nodes_[current].Left = InsertImpl(Nodes_[current].Left, key, priority);
            return current;
        }
    }
}