#pragma once

#include <memory>
#include <optional>
#include <utility>
#include <vector>

struct TTreapNode
{
    int Count;
    int Priority;
    int Value;
    int Left;
    int Right;
};

struct TTreapStorage
{
    std::vector<TTreapNode> Nodes;

    int AllocateNode(int value, int left, int right);
    int GetCount(int index) const;
    int GetPosition(int index) const;
    void InitCount(int index);
};

class TTreap
{
public:
    TTreap();
    TTreap(std::shared_ptr<TTreapStorage> storage, int index);

    TTreap Cut(int l, int r);

    void Append(int value);
    // #right can be changed and must not be used after the call.
    void Append(TTreap right);

    int Get(int position);

private:
    std::shared_ptr<TTreapStorage> Storage_;

    int Index_;

    int MergeImpl(int left, int right);
    std::pair<int, int> SplitImpl(int current, int position);
};
