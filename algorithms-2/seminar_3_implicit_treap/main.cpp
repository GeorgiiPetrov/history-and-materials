#include "treap.h"

#include <cassert>
#include <iostream>

int main()
{
    int n, m;
    std::cin >> n >> m;

    TTreap treap;
    for (int i = 0; i < n; ++i) {
        treap.Append(i);
    }

    for (int i = 0; i < m; ++i) {
        int l, r;
        std::cin >> l >> r;
        --l;
        --r;

        auto middle = treap.Cut(l, r);
        middle.Append(treap);
        treap = std::move(middle);
    }

    for (int i = 0; i < n; ++i) {
        std::cout << treap.Get(i) + 1 << " ";
    }
    std::cout << std::endl;

    return 0;
}