#include <atomic>

class TASSpinLock {
public:
    void Lock() {
        while (flag_.exchange(true)) {
            // sleep / pause
        }
    }

    void Unlock() {
        flag_.store(false);
    }

private:
    std::atomic<bool> flag_{false};
};

class MutexV1 {
public:
    void Lock() {
        count_.fetch_add(1);
        while (state_.exchange(1) == 1) {
            state_.wait(1);
        }
    }

    void Unlock() {
        state_.store(0);
        if (count_.fetch_sub(1) > 1) {
            state_.notify_one();
        }
    }

private:
    std::atomic<int> state_{0};
    std::atomic<int> count_{0};
};

class MutexV2 {
public:
    void Lock() {
        for (;;) {
            int old_val = state_.load();
            const int new_val = old == 0 ? 1 : 2;
            if (state_.compare_exchange_strong(old_val, new_val)) {
                if (old == 0) {
                    return;
                }
                state_.wait(new_val);
            }
        }
    }

    void Unlock() {
        if (state_.exchange(0) == 2) {
            state_.notify_all();
        }
    }

private:
    std::atomic<int> state_{0};
};

// 31                1 | 0
//  000000000000010100 | 1
//    thread count     | is locked
//
//  Add thread: += 2
//  Lock: |= 1
//  Unlock: ^= 1
class MutexV3 {
public:
    void Lock() {
        for (int old_val = state_.load();;) {
            const int new_val = (old_val | 1) + 2;
            if (state_.compare_exchange_strong(old_val, new_val)) {
                if ((old_val & 1) == 0) {
                    return;
                }
                state_.wait(new_val);
            }
        }
    }

    void Unlock() {
        // or just if (state_.fetch_sub(3) != 3)
        state_.fetch_sub(2);
        if (state_.fetch_xor(1) != 1) {
            state_.notify_one();
        }
    }

private:
    std::atomic<int> state_{0};
};

// std::once_flag
// std::call_once(std::once_flag& fl, callable);

// Singleton<T>::Get().func();

template <typename T>
class Singleton {
public:
    static T& Get() {
        std::call_once(flag_, Singleton::Init);
        return *instance_;
    }

    static void Init() {
        instance = std::make_unique<T>();
    }

private:
    static std::once_flag flag_;
    static std::unique_ptr<T> instance_;
};

template <typename T>
class MeyersSingleton {
public:
    static T& Get() {
        static T instance_;
        return instance_;
    }
};

int main() {
    std::atomic<int> x;
    x.fetch_add(1);
    // x *= 3;
    // x = f(x);
    x.store(f(x.load()));  // atomic?

    // CAS
    //
    // bool cas(int& expected, int value) {
    //     if (*this == expected) {
    //         *this = value;
    //         return true;
    //     }
    //     expected = *this;
    //     return false;
    // }
    //
    // x.compare_exchange_strong(T& expected, T value) -> bool
    // x.exchange(T value) -> old

    // atomic f(int) -> int
    int old = x.load();
    while (!x.compare_exchange_strong(old, f(old)))
        ;

    return 0;
}