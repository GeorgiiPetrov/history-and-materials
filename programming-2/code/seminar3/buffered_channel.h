#pragma once

#include <utility>
#include <optional>
#include <queue>
#include <condition_variable>
#include <exception>
#include <mutex>

template <class T>
class BufferedChannel {
public:
    explicit BufferedChannel(int size) : maxsize_(size), closed_(false) {
    //maxsize - размер буффера
    //closed - закрыт ли поток
    }

    void Send(const T& value) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (closed_){
            throw std::runtime_error("queue is closed");
        }else{
            while (queue_.size()==maxsize_){
                full_.wait(lock);
                // тред засыпает до момента, пока его не разбудят.
                // Пока он ждет, mutex разблокируется, его может захватить кто-то другой
                // когда поток проснется, он снова захватит(или будет ждать возможности захватить) mutex
                if (closed_){
                    throw std::runtime_error("Can't Send");
                    //обрабатываем ситуацию, когда тред зашел в вайл до закрытия потока, уснул, а проснулся уже в закрытом потоке
                    //также ситуация (1) см.  recv о чем идет речь
                }
            }
            queue_.push(value);
            empty_.notify_one();
            //если очередь была пустая и некоторые потоки заснули в ожидании элемента, надо разбудить один из них
        }
    }
    std::optional<T> Recv() {
        std::unique_lock<std::mutex> lock(mutex_);
        if (!closed_){
            while (queue_.empty()){
                empty_.wait(lock);
                //Засыпаем и полагаемся, что нас разбудят при любых обстоятельствах(на это полагаемся и в send)
                //Если элемент появится - мы его считаем
                //Также надо не забыть, что мы могли уснуть. Затем поток закрыли, элементы кончились, но до сих пор нас еще не будили(1)
                if (closed_){
                    //Если уснули еще в открытом потоке, а проснулись уже в закрытом. 
                    //Возможно есть способ написать с меньшим количеством копипасты(
                    if (queue_.empty()){
                        return std::nullopt;
                    }else{
                        T value = queue_.front();
                        queue_.pop();
                        return value;
                    }
                }
            }
            T value = queue_.front();
            queue_.pop();
            full_.notify_one();
            //Место в буффере освободилось. Надо разбудить кого-то из тех, кто пытался записать в заполненный буффер
            return value;
        }else{ // когда буффер закрыт, можно и нужно дочитать элементы, которые успели попасть в буффер до закрытия
            if (queue_.empty()){
                return std::nullopt;
            }else{
                T value = queue_.front();
                queue_.pop();
                return value;
            }
        }
    }
    void Close(){
        std::unique_lock<std::mutex> lock(mutex_);
        if (closed_){
            throw std::runtime_error("already closed");
        }    
        //разбудим всех, кто чего-то ждал
        empty_.notify_all();//(Фиксим ситуацию (1) )    
        full_.notify_all();
        closed_=true;
    }
private:
    std::mutex mutex_; 
    std::queue<T> queue_; // элементы буффера
    int maxsize_; 
    std::condition_variable full_; // отвечает за треды, пытающиеся записать в переполненный буффер (ждут освобождения места в очереди)
    std::condition_variable empty_; // отвечает за треды, пытающиеся считать из пустого буффера(ждут появления чего-то в очереди)
    bool closed_;
};
