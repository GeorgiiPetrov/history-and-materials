#include <condition_variable>
#include <mutex>

// Semaphore sm; 
//
// T1:                 T2:
// sm.Enter();         sm.Enter();
// ...                 ...
// sm.Leave();         sm.Leave();

// struct condition_variable {
//     void wait(std::unique_lock&);
//     void notify_one();
//     void notify_all();
// };

///////////////////////////////////////////////////////////////////////////////////////////////////

class SemaphoreBad1 {
public:
    explicit SemaphoreBad1(int n)
        : count_(n)
    { }

    void Enter() {
        std::unique_lock guard(mutex_);
        while (count_ == 0) {  // <-- Busy wait
            guard.unlock();
            sleep(...);
            guard.lock();
        }
        --count_;
    }

    void Leave() {
        std::lock_guard guard(mutex_);
        ++count_;
    }

private:
    std::mutex mutex_;
    int count_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

class SemaphoreBad2 {
public:
    explicit SemaphoreBad2(int n)
        : count_(n)
    { }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            cv_.wait();  // <-- Deadlock
        }
        --count_;
    }

    void Leave() {
        std::lock_guard guard(mutex_);
        if (count_ == 0) {
            cv_.notify_one();
        }
        ++count_;
    }

private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

class SemaphoreBad3 {
public:
    explicit SemaphoreBad3(int n)
        : count_(n)
    { }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            guard.unlock();
                            // <-- Lost wakeup
            cv_.wait();
            guard.lock();
        }
        --count_;
    }

    void Leave() {
        std::lock_guard guard(mutex_);
        if (count_ == 0) {
            cv_.notify_one();
        }
        ++count_;
    }

private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

class SemaphoreBad4 {
public:
    explicit SemaphoreBad4(int n)
        : count_(n)
    { }

    void Enter() {
        std::unique_lock guard(mutex_);
        if (count_ == 0) {
            cv_.wait(guard);
        }                     // <-- count_ == 0
        --count_;
    }

    void Leave() {
        std::lock_guard guard(mutex_);
        if (count_ == 0) {    // <-- Lost notify
            cv_.notify_one();
        }
        ++count_;
    }

private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

class SemaphoreGood {
public:
    explicit SemaphoreGood(int n)
        : count_(n)
    { }

    void Enter() {
        std::unique_lock guard(mutex_);
        // while (!(count_ > 0)) {
        //     cv_.wait(guard);
        // }
        cv_.wait(guard, [this] { return count_ > 0; });
        --count_;
    }

    void Leave() {
        std::lock_guard guard(mutex_);
        cv_.notify_one();
        ++count_;
    }

private:
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////

class FixedQueue {
public:
    explicit FixedQueue(int n)
        : max_size_(n)
    { }

    void Push(int x) {
        std::unique_lock lock(mutex_);
        while (queue_.size() == max_size_) {
            full_.wait(lock);
        }
        empty_.notify_one();
        queue_.push(x);
    }

    int Pop() {
        std::unique_lock lock(mutex_);
        while (queue_.empty()) {
            empty_.wait(lock);
        }
        const int result = queue_.front();
        queue_.pop();
        full_.notify_one();
        return result;
    }

private:
    std::mutex mutex_;
    std::queue<int> queue_;
    std::condition_variable full_;
    std::condition_variable empty_;
    int max_size_;
};