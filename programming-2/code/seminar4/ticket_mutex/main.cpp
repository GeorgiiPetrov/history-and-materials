#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

using namespace std::chrono_literals;

class TTicketMutex
{
public:
    void lock()
    {
        auto ticket = NextTicket_.fetch_add(1);
        while (ticket != CurrentTicket_.load()) {
            std::this_thread::yield();
        }
    }

    void unlock()
    {
        CurrentTicket_.fetch_add(1);
    }

private:
    std::atomic<size_t> CurrentTicket_ = 0;
    std::atomic<size_t> NextTicket_ = 0;
};

int main()
{
    // Use std::mutex instead to break the lock order.
    TTicketMutex mutex;

    std::vector<std::thread> threads;
    for (int i = 0; i < 100; ++i) {
        threads.emplace_back([&, i] {
            mutex.lock();
            std::cout << "Acquired for " + std::to_string(i) << std::endl;
            std::this_thread::sleep_for(10ms);
            mutex.unlock();
        });
        std::this_thread::sleep_for(1ms);
    }

    for (auto& t : threads) {
        t.join();
    }

    return 0;
}