#include <atomic>
#include <iostream>
#include <thread>

class TSharedMutex
{
public:
    void LockShared()
    {
        std::unique_lock<std::mutex> guard(Mutex_);
        if (ExclusiveCount_ != 0) {
            Available_.wait(guard, [&] {
                return ExclusiveCount_ == 0;
            });
        }
        SharedCount_ += 1;
    }

    void UnlockShared()
    {
        std::unique_lock<std::mutex> guard(Mutex_);
        SharedCount_ -= 1;
        if (SharedCount_ == 0) {
            Available_.notify_all();
        }
    }

    void Lock()
    {
        std::unique_lock<std::mutex> guard(Mutex_);
        if (SharedCount_ + ExclusiveCount_ != 0) {
            Available_.wait(guard, [&] {
                return SharedCount_ + ExclusiveCount_ == 0;
            });
        }
        ExclusiveCount_ += 1;
    }

    void Unlock()
    {
        std::unique_lock<std::mutex> guard(Mutex_);
        ExclusiveCount_ -= 1;
        Available_.notify_all();
    }

private:
    std::mutex Mutex_;
    std::condition_variable Available_;
    int ExclusiveCount_ = 0;
    int SharedCount_ = 0;
};

int main()
{
    TSharedMutex mutex;

    mutex.LockShared();
    mutex.LockShared();
    std::cout << "Acquired shared lock twice" << std::endl;
    mutex.UnlockShared();
    mutex.UnlockShared();

    mutex.Lock();
    std::cout << "Acquired exclusive lock" << std::endl;
    mutex.Lock();
    std::cout << "Deadlock happened!!!" << std::endl;
    mutex.Unlock();

    return 0;
}