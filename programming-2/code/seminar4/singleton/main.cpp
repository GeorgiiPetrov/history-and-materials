#include <atomic>
#include <iostream>
#include <mutex>
#include <vector>

template <class T>
class TSingleton
{
public:
    static T* Get()
    {
        // Fast path.
        auto* object = Object_.load();
        if (object) {
            return object;
        }

        // Slow path.
        std::unique_lock<std::mutex> lock(Mutex_);
        // Double check.
        object = Object_.load();
        if (object) {
            return object;
        }

        object = new T();
        Object_.store(object);

        return object;
    }

private:
    static std::atomic<T*> Object_;
    static std::mutex Mutex_;
};

template <class T>
std::atomic<T*> TSingleton<T>::Object_ = nullptr;

template <class T>
std::mutex TSingleton<T>::Mutex_;

struct TGlobalConfig
{
    std::vector<int> Elements;
};

int main()
{
    TSingleton<TGlobalConfig>::Get()->Elements.push_back(1);

    std::cout << TSingleton<TGlobalConfig>::Get()->Elements.size() << std::endl;

    return 0;
}