#include <atomic>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>
using namespace std;

int main(int argc, const char** argv)
{
    assert(argc > 0);

    const int padding = atoi(argv[1]);

    const int THREAD_COUNT = 100;
    const int COUNT = 1e9;
    const int PART_SIZE = COUNT / THREAD_COUNT;

    vector<int> values(COUNT);
    for (int i = 0; i < COUNT; ++i) {
        values[i] = i;
    }

    // THREAD COUNT = 3
    //
    // result with padding = 1
    //          0 0 0
    //          1 0 5
    //
    // result with padding = 7
    //          0 x x x x x x 0 x x x x x x 0 x x x x x x
    //          1 x x x x x x 0 x x x x x x 5 x x x x x x

    vector<atomic<long long>> result(padding * THREAD_COUNT);
    vector<thread> threads;
    for (int i = 0; i < THREAD_COUNT; ++i) {
        threads.emplace_back([=, &result, &values] {
            for (int j = PART_SIZE * i; j < COUNT && j < PART_SIZE * (i + 1); ++j) {
                result[padding * i] += values[j];
            }
        });
    }

    for (auto& t : threads) {
        t.join();
    }

    long long total = 0;

    for (const auto& value : result) {
        total += value;
    }
    cout << total << endl;

    return 0;
}
