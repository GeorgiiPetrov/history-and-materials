#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <vector>

struct TNode
{
    int Key;
};

struct TBucket
{
    mutable std::mutex Mutex;
    std::vector<TNode> Nodes;
};

class THashSet
{
public:
    THashSet()
        : Buckets_(42)
    { }

    bool Contains(int key) const
    {
        std::shared_lock rehashGuard(RehashMutex_);
        std::lock_guard guard(Buckets_[CalculateBucketIndex(key)].Mutex);
        return ContainsImpl(key);
    }

    bool Insert(int key)
    {
        std::shared_lock rehashGuard(RehashMutex_);
        auto bucketIndex = CalculateBucketIndex(key);
        std::lock_guard guard(Buckets_[bucketIndex].Mutex);
        if (ContainsImpl(key)) {
            return false;
        }
        Buckets_[bucketIndex].Nodes.push_back(TNode{key});
        // TODO: Invoke rehashing.
        return true;
    }

    bool Remove(int key)
    {
        std::shared_lock rehashGuard(RehashMutex_);
        auto bucketIndex = CalculateBucketIndex(key);
        std::lock_guard guard(Buckets_[bucketIndex].Mutex);
        auto& bucket = Buckets_[bucketIndex].Nodes;
        auto it = std::remove_if(
            bucket.begin(),
            bucket.end(),
            [key] (const auto& node) {
                return node.Key == key;
            });
        if (it == bucket.end()) {
            return false;
        } else {
            bucket.erase(it, bucket.end());
            return true;
        }
    }

private:
    const std::hash<int> Hasher_;

    mutable std::shared_mutex RehashMutex_;

    std::vector<TBucket> Buckets_;

    int CalculateBucketIndex(int key) const
    {
        return Hasher_(key) % Buckets_.size();
    }

    bool ContainsImpl(int key) const
    {
        auto bucketIndex = CalculateBucketIndex(key);
        for (const auto& node : Buckets_[bucketIndex].Nodes) {
            if (node.Key == key) {
                return true;
            }
        }
        return false;
    }

    void Rehash()
    {
        std::unique_lock guard(RehashMutex_);
        // TODO: Rehashing.
    }
};

int main()
{
    THashSet set;

    set.Insert(1);
    std::cout << "Contains 1: " << set.Contains(1) << std::endl;
    std::cout << "Contains 2: " << set.Contains(2) << std::endl;
    std::cout << "Contains 3: " << set.Contains(3) << std::endl;

    set.Insert(2);
    std::cout << "Contains 1: " << set.Contains(1) << std::endl;
    std::cout << "Contains 2: " << set.Contains(2) << std::endl;
    std::cout << "Contains 3: " << set.Contains(3) << std::endl;

    set.Remove(1);
    std::cout << "Contains 1: " << set.Contains(1) << std::endl;
    std::cout << "Contains 2: " << set.Contains(2) << std::endl;
    std::cout << "Contains 3: " << set.Contains(3) << std::endl;

    return 0;
}