# Семинар №3. 04 марта
* Напоминание мотивации использования singleton:
  * Логгер/трейсер для удобства вызовов LOG, TRACE.
  * Простая провязка частей программы: не пробрасывать в каждый модуль дополнительный аргумент TGlobalInfo.
  * Уникальные сущности как TFiberScheduler или TThreadManager: в программе ожидается один управлятор всеми тредами/корутинами.
  * Невозможно запустить две копии модулей в одном процессе с разными настройками singleton объектов. Как следствие, тяжелее писать тесты.
  * Тяжелее читать: непонятно, от чего зависит модуль.
* Реализация singleton через double-checked locking: std::atomic<T*> и std::mutex. Double check часто применяется в общем виде.
* Деаллокация singleton: используется std::atexit или аналог. Очень сложная тема из-за зависимостей между статическими объектами: например, можно ли использовать logger в деаллокации thread manager? Зависимость может быть в том числе от стейта стандартной библиотеки.
* Ticket mutex: std::atomic<size_t> ticket, через fetch add получаем свой номер в очереди, дожидаемся себя, fairness, почему не std::atomic<int>?
* Shared mutex: тривиальная реализация через int exclusive/shared count, mutex и condition variable.

## Материалы
* [Singleton](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-2/code/seminar4/singleton/main.cpp)
* [Ticket mutex](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-2/code/seminar4/ticket_mutex/main.cpp)
* [Shared mutex](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-2/code/seminar4/shared_mutex/main.cpp)

# Лекция №4. 01 марта
* Atomic
  * Compare and Swap (CAS)
* SpinLock
  * Крутится в цикле в отличие от mutex, который засыпает, до тех пор пока не разбудят
  * Вариации spinlock: TasSpinLock, TicketSpinLock
* Mutex
  * Для реализации нужны две операции Wait и Wake. В Linux есть системный вызов futex. В с++ можно воспользоваться std::atomic<int>::wait и std::atomic<int>::notify_*
  * Реализация mutex, через один std::atomic<int>, без лишнего вызова notify_one, в случае когда нет ждущих потоков
* Singleton
  * std::call_once
  * Реализация через std::call_once
  * Meyer's singleton
## Материалы
* [Код](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-2/code/lecture4/lecture4.cpp)

# Лекция №3. 22 февраля
* Deadlock. Как проверить, что в настоящий момент в hash set одновременно находятся ключи k1, k2, ..., km?
  * Подряд несколько независимых contains допускают гонку: contains(k1) возвращает true, удаляется k1, contains(k2) возвращает true
  * Нужно залочить все ключи, а затем провести проверку
  * Если делать в лоб, то два вызова multicontains(k1, k2) и multicontains(k2, k1) вызовут deadlock
  * Боремся с deadlock через сортировку ключей. Важно сортировать не ключи, а hash-и ключей.
* Semaphore
  * Пропускает ограниченное количество процессов в критическую секцию, остальные зависают
* Condition variable
  * wait
  * notify_one / notify_all
* FixedQueue - очередь ограниченного размера с ожиданием в push/pop при условии заполненности/пустоты на основе std::mutex и std::condition_variable
## Материалы
  * [Код](https://gitlab.com/mephi-dap/history-and-materials/-/tree/main/programming-2/code/lecture3/semaphore.cpp)

# Семинар №1. 18 февраля
* False sharing:
  * Запуск с padding = 1 работает в два раза медленнее, чем с padding = 10000
  * Если отказаться от atomic, код будет корректным, работать в разы быстрее, причем одинаково хорошо с любым padding
* Concurrent hash set:
  * std::vector<std::vector<int>> buckets, тривиальные реализации contains, insert
  * Добавляем один глобальный mutex. Что такое recursive_mutex для вызова Contains из-под Insert. Почему лучше ContainsImpl
  * Меняем глобальный mutex на один mutex на каждый bucket
  * Для rehash нужен дополнительный глобальный shared_mutex. Почему не подойдет mutex? Почему хуже лочить все mutexes внутри buckets?

# Лекция №2. 15 февраля
## Компиляция с++ проектов
* Этапы:
  * Препроцессинг
  * Компиляция
  * Линковка
* AST - abstract syntax tree
* Объявления (declaration) и определения (definition)
* Проблема циклических инклюдов
* pragma once
* ODR - one definition rule
* forward declaration

# Лекция №1. 11 февраля
## Базовая многопоточность: std::thread, std::mutex, std::shared_mutex, std::atomic

* Мотивация:
  * Скорость одного ядра процессора перестала расти в 2005, растет число ядер и немножко оптимизируется исполнение инструкций на цикл. Современные хосты по 256 ядер
  * Конкурентность исполнения: ваша любимая игра и принимает сигналы от мышки, и рисует картинку
* std::thread:
  * Пример SayHello
  * Нет конструктора копирования, есть move-конструктор, аналогично с assignment
  * Terminate, если не сделать join. Что такое join и зачем?
  * Потоки разделяют общее адресное пространство - куча, глобальные переменные, функции. Но имеют свои стеки. Ссылку на стек нельзя возвращать
  * Исключение из треда приводит к terminate. Аналогично исключению из main потока в однопоточной программе
  * std::this_thread::get_id, sleep_for
  * Смешение выводов в iostream из нескольких потоков. Race. Почему не перемешиваются символы, а только токены?
* Пример:
  * Параллелим суммирование чисел массива
  * Вектор тредов. Цикл по тредам
  * Что если поджойним не после создания всех тредов, а на каждой итерации после создания очередного?
  * Передача аргументов в callback
  * Если в потоке с номером i считать сумму по индексам j % thread count == i, получается в два раза хуже, чем если считать сумму в непрерывном блоке размера n / thread count. Кэши процессора и cache friendliness
* std::mutex:
  * std::mutex - mutual exclusion. На примере конкурентного доступа к iostream. Критическая секция между lock/unlock
  * std::lock_guard вместо явных lock/unlock
  * std::unique_lock умеет lock/unlock после конструирования
  * std::shared_mutex для read/write lock/unlock. shared_lock и unique_lock в паре объясняют, почему unique_lock называется unique
  * TAverageCalculator. Data race - один тред пишет, второй либо пишет, либо читает. Является UB и race в общем смысле. Фикс через shared_mutex
* std::atomic:
  * Эффективнее mutex, лежит в основе всех примитивов синхронизации, базовый блок
  * На уровне инструкций процессора. Блокировки не логические, а физические в памяти/процессоре на короткий интервал
  * В десятки раз медленнее обычных инструкций, только для базовых типов int, float, double, etc
  * Для сложных типов менее эффективен, сводится к std::mutex под капотом. std::atomic от std::vector не позволит сделать push_back без data race. Почему?
  * Реализация TAverageCalculator через std::atomic. Отсутствие data race, но наличие race - GetAverage конкурентно с Update
