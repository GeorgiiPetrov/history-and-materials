#include <iostream>
#include <vector>

using namespace std;

const int INF = 1e+6;

int main() {
    vector<int> p(3);
    int n;
    cin >> n;
    for (int i = 0; i < 3; ++i) {
        cin >> p[i];
    }

    vector<int> dp(n + 1, INF);

    dp[0] = 0;

    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (i - p[j] >= 0 && dp[i - p[j]] != INF) {
                if (dp[i] == INF) {
                    dp[i] = dp[i - p[j]] + 1;
                } else {
                    dp[i] = max(dp[i], dp[i - p[j]] + 1);
                }
            }
        }
    }

    cout << dp[n] << endl;

    return 0;
}