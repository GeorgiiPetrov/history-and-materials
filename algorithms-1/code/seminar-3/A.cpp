#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

const int INF = 1e+6;

int main() {
    ifstream cin("input.txt");
    ofstream cout("output.txt");

    int n;
    cin >> n;
    vector<int> t(n);
    for (int i = 0; i < n; ++i) {
        cin >> t[i];
    }

    vector<int> oldDp(2, INF);
    vector<int> newDp(2, INF);

    oldDp[0] = (t[0] >= 0);
    oldDp[1] = INF;

    for (int i = 1; i < n; ++i) {
        newDp[0] = oldDp[0] + (t[i] >= 0);
        newDp[1] = min(oldDp[0], oldDp[1]) + (t[i] <= 0);
        swap(oldDp, newDp);
    }

    cout << oldDp[1] << endl;

    return 0;
}