#include <iostream>

class TList {
public:
    class TNode {
    public:
        int Val;
        TNode* Next = nullptr;
        TNode* Prev = nullptr;
    };

    TNode* Head = nullptr;
    TNode* Tail = nullptr;

    void PushBack(int val) {
        TNode* it = new TNode;
        it->Val = val;

        if (Head == nullptr) {
            Head = it;
            Tail = it;
        } else {
            Tail->Next = it;
            it->Prev = Tail;
            Tail = it;
        }
    }

    bool Find(int val) {
        TNode* it = Head;
        while (it != nullptr) {
            if (it->Val == val) {
                return true;
            }
            it = it->Next;
        }
        return false;
    }

    void Erase(int val) {
        TNode* it = Head;
        while (it != nullptr) {
            if (it->Val == val) {
                break;
            }
            it = it->Next;
        }
        if (it == nullptr) {
            return;
        }

        TNode* left = it->Prev;
        if (left != nullptr) {
            left->Next = nullptr;
        }
        //TList = Head...left
        TNode* right = it->Next;
        if (right != nullptr) {
            right->Prev = nullptr;
        }
        // TList = right...Tail
        delete it;
        // TList = Head...left, it, right...Tail
        // TList = Head...left, right...Tail
        if (left != nullptr) {
            left->Next = right;
        }
        if (right != nullptr) {
            right->Prev = left;
        }
    }

    void PopFront() {
        if (Head == nullptr) {
            return;
        }
        TNode* right = Head->Next;
        if (right != nullptr) {
            right->Prev = nullptr;
        }
        delete Head;
        Head = right;
    }
};

std::ostream& operator <<(std::ostream& out, TList list) {
    TList::TNode* it = list.Head;
    while (it != nullptr) {
        out << it->Val << ' ';
        it = it->Next;
    }
    return out;
}

class TQueue {
public:
    TList List;
    void Push(int val) {
        List.PushBack(val);
    }
    void Pop() {
        List.PopFront(val);
    }
    //int Front();
};

int main() {
    TList list;
    list.PushBack(0);
    list.PushBack(7);
    list.PushBack(8);
    list.PushBack(4);
    std::cout << list.Find(10) << std::endl; // 0
    std::cout << list.Find(8) << std::endl; // 1
    std::cout << list << std::endl;
    list.Erase(7);
    list.Erase(10);
    std::cout << list << std::endl;
    list.PopFront();
    std::cout << list << std::endl;

    return 0;
}