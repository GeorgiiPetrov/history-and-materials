#include <vector>
#include <iostream>
using namespace std;

class TSegmentTree
{
public:
    explicit TSegmentTree(int n)
        : N_(n)
        , Counts_(4 * N_, 0)
    { }

    void Update(int pos, int value)
    {
        Update(/*v*/ 1, /*tl*/ 0, /*tr*/ N_ - 1, pos, value);
    }

    int GetKthZero(int l, int r, int k) const
    {
        return GetKthZero(1, 0, N_ - 1, l, r, k).Position;
    }

private:
    const int N_;

    vector<int> Counts_;

    struct TGetKthZeroResult
    {
        int Count;
        int Position;
    };

    void Update(int v, int tl, int tr, int pos, int value)
    {
        if (tl == tr) {
            Counts_[v] = (value == 0);
        } else {
            int tm = (tl + tr) >> 1;
            if (pos <= tm) {
                Update(2 * v, tl, tm, pos, value);
            } else {
                Update(2 * v + 1, tm + 1, tr, pos, value);
            }
            Counts_[v] = Counts_[2 * v] + Counts_[2 * v + 1];
        }
    }

    TGetKthZeroResult GetKthZero(int v, int tl, int tr, int l, int r, int k) const
    {
        assert(k >= 1);
        if (Counts_[v] < k) {
            return TGetKthZeroResult{Counts_[v], -1};
        }
        if (tl == tr) {
            return TGetKthZeroResult{Counts_[v], tl};
        }
        int tm = (tl + tr) >> 1;
        if (l == tl && r == tr) {
            if (Counts_[2 * v] >= k) {
                return GetKthZero(2 * v, tl, tm, l, tm, k);
            } else {
                auto result = GetKthZero(2 * v + 1, tm + 1, tr, tm + 1, r, k - Counts_[2 * v]);
                result.Count += Counts_[2 * v];
                assert(result.Count == k);
                return result;
            }
        }
        if (r <= tm) {
            return GetKthZero(2 * v, tl, tm, l, r, k);
        }
        if (l > tm) {
            return GetKthZero(2 * v + 1, tm + 1, tr, l, r, k);
        }
        {
            auto leftResult = GetKthZero(2 * v, tl, tm, l, tm, k);
            if (leftResult.Count >= k) {
                return leftResult;
            }
            auto rightResult = GetKthZero(2 * v + 1, tm + 1, tr, tm + 1, r, k - leftResult.Count);
            return TGetKthZeroResult{
                leftResult.Count + rightResult.Count,
                rightResult.Position
            };
        }
    }
};

int main()
{
    TSegmentTree st(5);

    st.Update(3, 0);
    st.Update(2, 0);

    cout << "[1..3] 1st zero: " << st.GetKthZero(1, 3, 1) << endl;
    cout << "[1..3] 2nd zero: " << st.GetKthZero(1, 3, 2) << endl;
    cout << "[1..3] 3rd zero: " << st.GetKthZero(1, 3, 3) << endl;

    st.Update(3, 1);
    cout << "Removed zero at 3 position" << endl;

    cout << "[1..3] 1st zero: " << st.GetKthZero(1, 3, 1) << endl;
    cout << "[1..3] 2nd zero: " << st.GetKthZero(1, 3, 2) << endl;
    cout << "[1..3] 3rd zero: " << st.GetKthZero(1, 3, 3) << endl;

    return 0;
}
