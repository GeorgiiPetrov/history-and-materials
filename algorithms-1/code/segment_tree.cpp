#include <vector>
using namespace std;

// For simplicity.
const int INF = 1e9;

class TSegmentTree
{
public:
    explicit TSegmentTree(int n)
        : N_(n)
        , Mins_(4 * n, -INF)
    { }

    void Update(int pos, int value)
    {
    }

    int GetMin(int l, int r) const
    {
        return GetMin(/*rootIndex*/ 1, /*tl*/ 0, /*tr*/ N_ - 1, l, r);
    }

private:
    const int N_;

    /*
    struct TNode
    {
        // int Left; == 2 * index
        // int Right; == 2 * index + 1
        // int TreeLeft; redundant, available during recursion
        // int TreeRight; redundant, available during recursion
        int Minimum;
    };

    vector<TNode> Nodes_;
    */

    vector<int> Mins_;

    void Update(int index, int tl, int tr, int pos, int value)
    {

    }

    int GetMin(int index, int tl, int tr, int l, int r) const
    {
        if (l == tl && r == tr) {
            return Mins_[index];
        } else {
            const int leftIndex = 2 * index;
            const int rightIndex = 2 * index + 1;
            const int tm = (tl + tr) >> 1; // treeMiddle
            // [tl..tr] = [tl..tm] + [tm + 1..tr]
            if (r <= tm) { // [l..r] in [tl..tm]
                return GetMin(leftIndex, tl, tm, l, r);
            } else if (l > tm) { // [l..r] in [tm + 1..tr]
                return GetMin(rightIndex, tm + 1, tr, l, r);
            } else {
                // [l..tm]     in [tl..tm]
                //             +
                // [tm + 1..r] in [tm + 1..tr]
                return min(
                    GetMin(leftIndex, tl, tm, l, tm),
                    GetMin(rightIndex, tm + 1, tr, tm + 1, r));
            }
        }
    }
};

int main()
{

    return 0;
}
