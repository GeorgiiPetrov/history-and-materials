# 10 декабря, atcoder abc281
* [Контест](https://atcoder.jp/contests/abc281)
* [Разбор](https://atcoder.jp/contests/abc281/editorial)

# 3 декабря, atcoder abc280
* [Контест](https://atcoder.jp/contests/abc280)
* [Разбор](https://atcoder.jp/contests/abc280/editorial)

# 26 ноября, atcoder abc279
* [Контест](https://atcoder.jp/contests/abc279)
* [Разбор](https://atcoder.jp/contests/abc279/editorial)

# 19 ноября, atcoder abc278
* [Контест](https://atcoder.jp/contests/abc278)
* [Разбор](https://atcoder.jp/contests/abc278/editorial)

# 12 ноября, atcoder abc277
* [Контест](https://atcoder.jp/contests/abc277)
* [Разбор](https://atcoder.jp/contests/abc277/editorial)

# 5 ноября, atcoder abc276
* [Контест](https://atcoder.jp/contests/abc276)
* [Разбор](https://atcoder.jp/contests/abc276/editorial)

# 29 октября, atcoder abc275
* [Контест](https://atcoder.jp/contests/abc275)
* [Разбор](https://atcoder.jp/contests/abc275/editorial)

# 15 октября, atcoder abc273
* [Контест](https://atcoder.jp/contests/abc273)
* [Разбор](https://atcoder.jp/contests/abc273/editorial)

# 8 октября, atcoder abc272
* [Контест](https://atcoder.jp/contests/abc272)
* [Разбор](https://atcoder.jp/contests/abc272/editorial)

# 1 октября, atcoder abc271
* [Контест](https://atcoder.jp/contests/abc271)
* [Разбор](https://atcoder.jp/contests/abc271/editorial)

# 24 сентября, atcoder abc270
* [Контест](https://atcoder.jp/contests/abc270)
* [Разбор](https://atcoder.jp/contests/abc270/editorial)

# 17 сентября, codeforces №713 div3
* [Контест](https://codeforces.com/group/lTkkazGqHJ/contest/398970)
* [Разбор](https://codeforces.com/blog/entry/89535)

# 10 сентября, atcoder abc268
* [Контест](https://atcoder.jp/contests/abc268)
* [Разбор](https://atcoder.jp/contests/abc268/editorial)
